/label ~"release post" ~"release post item" ~"Technical Writing"

The Release Post Manager will use this template to create separate performance improvement and bug fixes MRs for the release post blog.

## Key dates & Review

- [ ] By the 10th, `@Release Post Manager` informs EMs/PMs to draft/submit bugs or performance improvements via this MR per [release post MR task list item](https://gitlab.com/gitlab-com/www-gitlab-com/-/blob/master/.gitlab/merge_request_templates/Release-Post.md#release-post-item-creation-reminder-release_post_manager)
- [ ] By the 15th, `@Release Post Manager` assigns MR to TW lead for review and applies label `in review`
- [ ] By the 16th: `@TW Lead` reviews, applies the `ready` label and assigns  to `@Release Post Manager`
- [ ] By the 17th: `@Release Post Manager` merges the MR, prior to final content assembly
