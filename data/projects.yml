gitlab:
  name: GitLab
  path: gitlab-org/gitlab
  link: https://gitlab.com/gitlab-org/gitlab
  mirrors:
    - https://dev.gitlab.org/gitlab/gitlabhq
  description: |
    This is the canonical development repository for GitLab. Open source code
    from this repository gets mirrored to GitLab-FOSS project.

    This project is the source of the gitlab-ee distributions like
    omnibus-gitlab packages, docker images, AWS AMIs etc.

    GitLab EE requires a license key to be used, without which only the CE
    functionality will be available.

gitlab-foss:
  name: GitLab FOSS
  path: gitlab-org/gitlab-foss
  link: https://gitlab.com/gitlab-org/gitlab-foss
  mirrors:
    - https://github.com/gitlabhq/gitlabhq
    - https://dev.gitlab.org/gitlab/gitlabhq
  description: |
    This is a mirror of GitLab codebase with all the proprietary code removed.

    Development happens in the canonical repository at GitLab.
    This repository is updated periodically with changes from there.

    This project is the source of the gitlab-ce distributions like
    omnibus-gitlab packages, docker images, AWS AMIs etc.

gitlab-elasticsearch-indexer:
  name: GitLab Elasticsearch Indexer
  path: gitlab-org/gitlab-elasticsearch-indexer
  link: https://gitlab.com/gitlab-org/gitlab-elasticsearch-indexer
  description: |
    An Elasticsearch indexer for Git repositories. Used by GitLab EE to
    implement Global Code Search.

gitlab-shell:
  name: GitLab Shell
  path: gitlab-org/gitlab-shell
  link: https://gitlab.com/gitlab-org/gitlab-shell
  mirrors:
    - https://dev.gitlab.org/gitlab/gitlab-shell
    - https://github.com/gitlabhq/gitlab-shell
  description: |
    GitLab Shell handles Git commands for GitLab. It's an essential part of GitLab.

gitlab-workhorse:
  name: GitLab Workhorse
  path: gitlab-org/gitlab-workhorse
  link: https://gitlab.com/gitlab-org/gitlab-workhorse
  description: |
    Gitlab-workhorse is a smart reverse proxy for GitLab. It handles "large" HTTP
    requests such as file downloads, file uploads, Git push/pull and Git archive
    downloads.

omnibus-gitlab:
  name: Omnibus GitLab
  path: gitlab-org/omnibus-gitlab
  link: https://gitlab.com/gitlab-org/omnibus-gitlab
  mirrors:
  - https://dev.gitlab.org/gitlab/omnibus-gitlab
  - https://github.com/gitlabhq/omnibus-gitlab
  description: |
    Omnibus GitLab creates the packages for GitLab.

gitlab-orchestrator:
  name: GitLab Orchestrator
  path: gitlab-org/gitlab-orchestrator
  link: https://gitlab.com/gitlab-org/gitlab-orchestrator
  description: |
    GitLab Orchestrator automates the installation of GitLab multi-node deployments,
    High Availability deployments, and GitLab Geo deployments.

cookbook-omnibus-gitlab:
  name: Cookbook Omnibus GitLab
  path: gitlab-org/cookbook-omnibus-gitlab
  link: https://gitlab.com/gitlab-org/cookbook-omnibus-gitlab
  description: |
    Chef Cookbooks for deploying omnibus-gitlab package

distribution:
  name: Distribution team issue tracker
  path: gitlab-org/distribution/team-tasks
  link: https://gitlab.com/gitlab-org/distribution/team-tasks
  description: |
    Used to track Distribution team work.

gitlab-com-infrastructure:
  name: GitLab.com - infrastructure Terraform files
  path: gitlab-com/gitlab-com-infrastructure
  link: https://gitlab.com/gitlab-com/gitlab-com-infrastructure
  description: |
    Terraform - configuration and provisioning files for virtual machine nodes on production and staging

gitlab-com-runbooks:
  name: Gitlab.com - "on call" runbooks
  path: gitlab-com/runbooks
  link: https://gitlab.com/gitlab-com/runbooks
  description: |
    Describes system components, triage procedures, and commands to use in scenarios commonly faced
    by GitLab.com production support engineers

infrastructure:
  name: GitLab.com - infrastructure issue tracker
  path: gitlab-com/infrastructure
  link: https://gitlab.com/gitlab-com/infrastructure
  description: |
    Used to track the infrastructure work of GitLab.com itself

gitlab-cookbooks:
  name: GitLab.com - infrastructure node provisioning by role
  path: gitlab-cookbooks
  link: https://gitlab.com/gitlab-cookbooks
  description: |
    This is a group with a project - cookbooks - for each provisioned role in the GitLab.com cluster.
    These cooksbooks are applied after the virtual machine node is provisioned by Terraform project.

gitlab-cog:
  name: GitLab.com COGS
  path: gitlab-cog
  link: https://gitlab.com/gitlab-cog
  description: |
    This is a group which contains a separate project for each COG used by the GitLab.com infrastructure.

    A COG is an integration between the role of a node (or cluster) and a Chat application such as Slack.  The COG allows
    the infrastructure node (or cluster) to post status and alerts into Chat Channels and also allows for commands
    to be issued in the Chat Channel that control the behavior of the infrastructure node/cluster.

gitlab-development-kit:
  name: GitLab Development Kit
  path: gitlab-org/gitlab-development-kit
  link: https://gitlab.com/gitlab-org/gitlab-development-kit
  description: |
    GitLab Development Kit (GDK) provides a collection of scripts and other
    resources to install and manage a GitLab installation for development
    purposes. The source code of GitLab is spread over multiple repositories
    and it requires Ruby, Go, Postgres/MySQL, Redis and more to run.

    GDK helps you install and configure all these different components,
    and start/stop them when you work on GitLab.

gitaly:
  name: Gitaly
  path: gitlab-org/gitaly
  link: https://gitlab.com/gitlab-org/gitaly
  description: |
    Git RPC service for handling all the git calls made by GitLab.

gitlab-qa:
  name: GitLab QA
  path: gitlab-org/gitlab-qa
  link: https://gitlab.com/gitlab-org/gitlab-qa
  description: |
    End-to-end, black-box, entirely click-driven integration tests for GitLab.

gitlab-roulette:
  name: GitLab Roulette
  path: gitlab-org/gitlab-roulette
  link: https://gitlab.com/gitlab-org/gitlab-roulette
  description: |
    This project regularly publish a JSON that contains available maintainers
    for gitlab-org projects: https://gitlab-org.gitlab.io/gitlab-roulette/roulette.json

gitlab-triage:
  name: GitLab Triage
  path: gitlab-org/gitlab-triage
  link: https://gitlab.com/gitlab-org/gitlab-triage
  description: |
    This gem aims to enable project managers and maintainers to automatically
    triage Issues and Merge Requests in GitLab projects based on defined
    policies.

triage-ops:
  name: GitLab triage operations
  path: gitlab-org/quality/triage-ops
  link: https://gitlab.com/gitlab-org/quality/triage-ops
  description: |
    Triage operations for GitLab Issues and Merge Requests

triage-serverless:
  name: GitLab triage serverless
  path: gitlab-org/quality/triage-serverless
  link: https://gitlab.com/gitlab-org/quality/triage-serverless
  description: |
    Triage operations for GitLab Issues and Merge Requests using GitLab serverless

gitlab-styles:
  name: GitLab Styles
  path: gitlab-org/gitlab-styles
  link: https://gitlab.com/gitlab-org/gitlab-styles
  description: |
    This gem centralizes some shared GitLab's styles config (only RuboCop
    for now), as well as custom RuboCop cops.

www-gitlab-com:
  name: www-gitlab-com
  path: gitlab-com/www-gitlab-com
  link: https://gitlab.com/gitlab-com/www-gitlab-com
  description: GitLab Inc. Marketing, Blog and Handbook websites available on about.gitlab.com

marketo-tools:
  name: Marketo Tools
  path: gitlab-com/marketo-tools
  link: https://gitlab.com/gitlab-com/marketo-tools
  description: Internal Marketo Tools

githost:
  name: GitHost.io
  path: gitlab-com/githost
  link: https://gitlab.com/gitlab-com/githost
  description: Hosted version of GitLab

gitlab-pages:
  name: GitLab Pages
  path: gitlab-org/gitlab-pages
  link: https://gitlab.com/gitlab-org/gitlab-pages
  description: |
    GitLab Pages daemon used to serve static websites for GitLab users

go-mimedb:
  name: Go MimeDB
  path: gitlab-org/go-mimedb
  link: https://gitlab.com/gitlab-org/go-mimedb
  description: |
    Go package that generates additional MIME types that can be loaded into memory by a web server

gitlab-runner:
  name: GitLab Runner
  path: gitlab-org/gitlab-runner
  link: https://gitlab.com/gitlab-org/gitlab-runner
  description: GitLab CI/CD Runner

autoscaler:
  name: Autoscaler Custom Executor driver for GitLab Runner
  path: gitlab-org/ci-cd/custom-executor-drivers/autoscaler
  link: https://gitlab.com/gitlab-org/ci-cd/custom-executor-drivers/autoscaler
  description: |
    A driver for GitLab Runner's Custom Executor, that implements autoscaling algorithm

license-app:
  name: LicenseDot
  path: gitlab-org/license-gitlab-com
  link: https://gitlab.com/gitlab-org/license-gitlab-com
  description: Internal GitLab License App

customers-app:
  name: CustomersDot (Subscription Portal)
  path: gitlab-org/customers-gitlab-com
  link: https://gitlab.com/gitlab-org/customers-gitlab-com
  description: Internal GitLab CustomersDot

gitlab-license:
  name: GitLab License
  path: gitlab/gitlab-license
  link: https://dev.gitlab.org/gitlab/gitlab-license
  description: Internal GitLab License Distribution App

university:
  name: GitLab University
  path: gitlab-org/university
  link: https://gitlab.com/gitlab-org/university
  description: Internal GitLab University

version-gitlab-com:
  name: version.gitlab.com
  path: gitlab-services/version-gitlab-com
  link: https://gitlab.com/gitlab-services/version-gitlab-com
  description: Internal GitLab Version App

gitlab-contributors:
  name: GitLab Contributors
  path: gitlab-com/gitlab-contributors
  link: https://gitlab.com/gitlab-com/gitlab-contributors
  description: Application behind contributors.gitlab.com

pages-gitlab-io:
  name: pages.gitlab.io
  path: pages/pages.gitlab.io
  link: https://gitlab.com/pages/pages.gitlab.io
  description: GitLab Pages landing page

gitlab-monitor:
  name: GitLab Monitor
  path: gitlab-org/gitlab-monitor
  link: https://gitlab.com/gitlab-org/gitlab-monitor
  description: Tooling used to monitor GitLab.com

gitlab-docs:
  name: GitLab Docs
  path: gitlab-org/gitlab-docs
  link: https://gitlab.com/gitlab-org/gitlab-docs
  description: Project behind docs.GitLab.com

gitlab_kramdown:
  name: GitLab Kramdown
  path: gitlab-org/gitlab_kramdown
  link: https://gitlab.com/gitlab-org/gitlab_kramdown
  description: GitLab Flavored Markdown extensions to Kramdown

gitlab-markup:
  name: GitLab Markup
  path: gitlab-org/gitlab-markup
  link: https://gitlab.com/gitlab-org/gitlab-markup
  description: Markup render for non Markdown content

release-tools:
  name: GitLab Release Tools
  path: gitlab-org/release-tools
  link: https://gitlab.com/gitlab-org/release-tools
  description: Instructions and tools for releasing GitLab

takeoff:
  name: takeoff
  path: gitlab-org/takeoff
  link: https://gitlab.com/gitlab-org/takeoff
  description: Tooling used to deploy GitLab.com to any environment

spam-master:
  name: Spam-Master
  path: gitlab-com/spam-master
  link: https://gitlab.com/gitlab-com/spam-master
  description: |
    Collection of spam fighting API scripts for GitLab instances

rubocop-gitlab-security:
  name: RuboCop-GitLab-Security
  path: gitlab-org/rubocop-gitlab-security
  link: https://gitlab.com/gitlab-org/rubocop-gitlab-security
  description: |
    GitLab RuboCop gem for static analysis.

gitlab-design:
  name: GitLab Design
  path: gitlab-org/gitlab-design
  link: https://gitlab.com/gitlab-org/gitlab-design
  description:
    GitLab Design is used to jumpstart design work through the use of our design library.
    It is intended to enable frequent, stable, and consistent contributions while making
    GitLab's design open and transparent. This project helps facilitate design handoffs and
    design–development communication.

charts-gitlab-io:
  name: GitLab Helm Repository
  path: charts/charts.gitlab.io
  link: https://gitlab.com/charts/charts.gitlab.io
  description: |
    GitLab's official All-in-one Helm charts.

gitlab-chart:
  name: GitLab Cloud Native Helm Chart
  path: charts/gitlab
  link: https://gitlab.com/gitlab-org/charts/gitlab
  description: |
    GitLab's official Cloud Native Helm chart.

auto-deploy-image:
  name: Auto Deploy Docker Image
  path: cluster-integration/auto-deploy-image
  link: https://gitlab.com/gitlab-org/cluster-integration/auto-deploy-image/
  description: |
    The image in which Auto DevOps deployment jobs run, responsible for
    complex deployment logic that can't be handled by the Auto Deploy App
    helm chart

auto-build-image:
  name: Auto Build Docker Image
  path: cluster-integration/auto-build-image
  link: https://gitlab.com/gitlab-org/cluster-integration/auto-build-image/
  description: |
    The image in which Auto DevOps build jobs run. Handles various logic around
    building and tagging Docker images.

cluster-applications:
  name: Managed Cluster Applications Docker Image
  path: cluster-integration/cluster-applications
  link: https://gitlab.com/gitlab-org/cluster-integration/cluster-applications
  description: |
    The image in which Managed Cluster Applications configuration jobs run.

gitlab-kubernetes-agent:
  name: GitLab Kubernetes Agent
  path: cluster-integration/gitlab-agent
  link: https://gitlab.com/gitlab-org/cluster-integration/gitlab-agent
  description: |
    GitLab Kubernetes Agent is an active in-cluster component for solving any GitLab<->Kubernetes integration tasks.

kubernetes-gitlab-demo:
  name: Kubernetes GitLab Demo
  path: gitlab-org/kubernetes-gitlab-demo
  link: https://gitlab.com/gitlab-org/kubernetes-gitlab-demo
  description: |
    Deprecated GitLab Idea to Production Kubernetes demo project

group-conversations:
  name: Group Conversations
  path: gitlab-org/group-conversations
  link: https://gitlab.com/gitlab-org/group-conversations
  description: |
    Presentations from (some of) the teams at GitLab, to update the rest of the
    world on what they've been working on.

design.gitlab.com:
  name: Pajamas Design System
  path: gitlab-org/gitlab-services/design.gitlab.com
  link: https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com
  description: |
    GitLab's open source Design System. Contains brand and product design
    guidelines and UI components for all things GitLab.

gitlab-svgs:
  name: GitLab SVGs
  path: gitlab-org/gitlab-svgs
  link: https://gitlab.com/gitlab-org/gitlab-svgs
  description: |
    Our SVG Assets and the corresponding pipelines to create all of our SVG sprites
    for our icons. Also automatic optimisation of SVG based illustrations.

gitlab-restore:
  name: Backup/restore procedures
  path: gitlab-restore
  link: https://gitlab.com/gitlab-restore
  description: |
    Automated backup and restore procedures for GitLab.com data

tanukidesk:
  name: Tanukidesk
  path: gitlab-com/marketing/community-relations/community-advocacy/tanukidesk
  link: https://gitlab.com/gitlab-com/marketing/community-relations/community-advocacy/tanukidesk
  description: |
    Bidirectional communication between Zendesk and Disqus / HackerNews.

remote-only:
  name: RemoteOnly.org
  path: gitlab-com/www-remoteonly-org
  link: https://gitlab.com/gitlab-com/www-remoteonly-org/
  description: |
    This is the source for the https://www.remoteonly.org site.

grape-path-helpers:
  name: grape-path-helpers
  path: gitlab-org/grape-path-helpers
  link: https://gitlab.com/gitlab-org/grape-path-helpers
  description: |
    Provides named route helpers for Grape APIs, similar to Rails' route helpers.
    Forked/renamed from https://github.com/reprah/grape-route-helpers

gitlab-ui:
  name: gitlab-ui
  path: gitlab-org/gitlab-ui
  link: https://gitlab.com/gitlab-org/gitlab-ui
  description: |
    UI component library written in Vue.js.
    Use https://gitlab-org.gitlab.io/gitlab-ui/ to see all the components.
    Every frontend engineer at GitLab is a reviewer of this project.
    However, the reviewers below are part of the gitlab-ui working group,
    and it's recommended to ping one of them in a non-trivial MR.

gitlab-eslint-config:
  name: gitlab-eslint-config
  path: gitlab-org/gitlab-eslint-config
  link: https://gitlab.com/gitlab-org/gitlab-eslint-config
  description: |
    GitLab's eslint configuration

prometheus-client-mmap:
  name: prometheus-client-mmap
  path: gitlab-org/prometheus-client-mmap
  link: https://gitlab.com/gitlab-org/prometheus-client-mmap
  description: |
    This Prometheus library is fork of Prometheus Ruby Client
    that uses memory-mapped files to share metrics from multiple processes

gitlab-build-images:
  name: gitlab-build-images
  path: gitlab-org/gitlab-build-images
  link: https://gitlab.com/gitlab-org/gitlab-build-images/
  description: |
    A repository for building Docker images for GitLab builds and tests.

gitlab-omnibus-builder:
  name: GitLab Omnibus Builder
  path: gitlab-org/gitlab-omnibus-builder
  link: https://gitlab.com/gitlab-org/gitlab-omnibus-builder
  description: |
    A repository for building base Docker images to be used while building
    Omnibus-GitLab packages for supported operating systems. It also contains
    a cookbook to setup builder machines.

labkit:
  name: labkit
  path: gitlab-org/labkit
  link: https://gitlab.com/gitlab-org/labkit
  description: |
    LabKit is minimalist library to provide functionality for Go services at GitLab.

labkit-ruby:
  name: labkit-ruby
  path: gitlab-org/labkit-ruby
  link: https://gitlab.com/gitlab-org/labkit-ruby
  description: |
    LabKit-Ruby is minimalist library to provide functionality for Ruby services at GitLab.

gitlabktl:
  name: gitlabktl
  path: gitlab-org/gitlabktl
  link: https://gitlab.com/gitlab-org/gitlabktl
  description: |
    `gitlabktl` is a tool that integrates GitLab with Kubernetes / Knative and
    is a part of GitLab Serverless platform.

gitter-webapp:
  name: Gitter webapp
  path: gitlab-org/gitter/webapp
  link: https://gitlab.com/gitlab-org/gitter/webapp
  description: |
    Gitter is a community for software developers. This project is the main monolith web chat application.

meltano:
  name: Meltano
  path: meltano/meltano
  link: https://gitlab.com/meltano/meltano
  description: |
    Meltano is an open source convention-over-configuration product for the whole data lifecycle, all the way from loading data to analyzing it.
    It does data ops, data engineering, analytics, business intelligence, and data science.

    Meltano stands for the steps of the data science life-cycle: Model, Extract, Load, Transform, Analyze, Notebook, and Orchestrate.

peopleops-employment-automation:
  name: PeopleOps Employment Automation
  path: gitlab-people-engineering/employment-automation
  link: https://ops.gitlab.net/gitlab-people-engineering/employment-automation
  description: |
    Automation related to various employment activities at GitLab, like
    onboarding issue creation, anniversary and new hire announcements,
    GreenHouse to BambooHR sync, general issue housekeeping, etc.

snowflake_spend:
  name: Snowflake Spend dbt Package
  path: gitlab-data/snowflake_spend
  link: https://gitlab.com/gitlab-data/snowflake_spend
  description: |
    This is a dbt package for understanding the cost your Snowflake Data Warehouse is accruing.

gitlab-data-utils:
  name: Data Utils
  path: gitlab-data/gitlab-data-utils
  link: https://gitlab.com/gitlab-data/gitlab-data-utils
  description: |
    Repo for commonly used utilities within the data org

data-image:
  name: Data Infrastructure
  path: gitlab-data/data-image
  link: https://gitlab.com/gitlab-data/data-image
  description: |
    The project for the Data Team's Docker Images/Kubernetes Infrastructure

gitlab_data:
  name: GitLab Data
  path: gitlab-data/analytics
  link: https://gitlab.com/gitlab-data/analytics
  description: |
    The primary project for the GitLab Data team

data_chatops:
  name: GitLab Data Chatops
  path: gitlab-data/chatops
  link: https://gitlab.com/gitlab-data/chatops
  description: |
    The Data team primarily uses chatops to troubleshoot data quality concerns.

secure-DS-gemnasium:
  name: Dependency Scannning - Gemnasium analyzer
  path: gitlab-org/security-products/analyzers/gemnasium
  link: https://gitlab.com/gitlab-org/security-products/analyzers/gemnasium
  description: |
    Gemnasium analyzer providing scanning capabilities for our Dependency Scanning category.

secure-DS-gemnasium-maven:
  name: Dependency Scannning - Gemnasium-Maven analyzer
  path: gitlab-org/security-products/analyzers/gemnasium-maven
  link: https://gitlab.com/gitlab-org/security-products/analyzers/gemnasium-maven
  description: |
    Gemnasium Maven analyzer providing scanning capabilities for our Dependency Scanning category.

secure-DS-gemnasium-python:
  name: Dependency Scannning - Gemnasium-Python analyzer
  path: gitlab-org/security-products/analyzers/gemnasium-python
  link: https://gitlab.com/gitlab-org/security-products/analyzers/gemnasium-python
  description: |
    Gemnasium Python analyzer providing scanning capabilities for our Dependency Scanning category.

secure-DS-gemnasium-db:
  name: Dependency Scannning - Gemnasium Database
  path: gitlab-org/security-products/gemnasium-db
  link: https://gitlab.com/gitlab-org/security-products/gemnasium-db
  description: |
    Repository providing security advisories for the Gemnasium analyzer (Dependency Scanning). Part of our Vulnerability Database category.

secure-DS-bundler-audit:
  name: Dependency Scannning - bundler-audit analyzer
  path: gitlab-org/security-products/analyzers/bundler-audit
  link: https://gitlab.com/gitlab-org/security-products/analyzers/bundler-audit
  description: |
    bundler-audit analyzer providing scanning capabilities for our Dependency Scanning category.

secure-DS-retireJS:
  name: Dependency Scannning - Retire.js analyzer
  path: gitlab-org/security-products/analyzers/retire.js
  link: https://gitlab.com/gitlab-org/security-products/analyzers/retire.js
  description: |
    Retire.js analyzer providing scanning capabilities for our Dependency Scanning category.

secure-analyzers:
  name: Secure Analyzers
  path: gitlab-org/security-products/analyzers
  link: https://gitlab.com/gitlab-org/security-products/analyzers
  description: |
    Several repositories for the various analyzers including language specific analyzers, container scanning, secret detection, and dependency scanning.

secure-dast:
  name: Dynamic Application Security Testing (DAST)
  path: gitlab-org/security-products/dast
  link: https://gitlab.com/gitlab-org/security-products/dast
  description: |
    Dynamic Application Security Testing for GitLab CI

secure-sast:
  name: Static Application Security Testing (SAST)
  path: gitlab-org/security-products/sast
  link: https://gitlab.com/gitlab-org/security-products/sast
  description: |
    Static Application Security Testing for GitLab CI

secure-LC-license-finder:
  name: License Management
  path: gitlab-org/security-products/license-management
  link: https://gitlab.com/gitlab-org/security-products/license-management
  description: |
     License Finder analyzer providing scanning capabilities for our License Compliance category.

protect-CS-klar:
  name: Container Scanning - Klar analyzer
  path: gitlab-org/security-products/analyzers/klar/
  link: https://gitlab.com/gitlab-org/security-products/analyzers/klar/
  description: |
   Klar analyzer providing scanning capabilities for our Container Scanning category.

verify-accessibility:
  name: Accessibility
  path: gitlab-org/ci-cd/accessibility
  link: https://gitlab.com/gitlab-org/ci-cd/accessibility
  description: |
    Accessibility tool for GitLab CI

verify-codequality:
  name: Code Quality
  path: gitlab-org/ci-cd/codequality
  link: https://gitlab.com/gitlab-org/ci-cd/codequality
  description: |
    Code Quality Analysis tool for GitLab CI

container-registry:
  name: Container Registry
  path: gitlab-org/container-registry
  link: https://gitlab.com/gitlab-org/container-registry
  description: |
    With the Docker Container Registry integrated into GitLab, every project can have its own space to store its Docker images.

iglu:
  name: Iglu
  path: gitlab-org/iglu
  link: https://gitlab.com/gitlab-org/iglu
  description: |
    Iglu Schema Registry for https://gitlab-org.gitlab.io/iglu

status-page:
  name: status-page
  path: gitlab-org/status-page
  link: https://gitlab.com/gitlab-org/status-page
  description: |
    Static Site for GitLab's Status Page feature

cng:
  name: CNG
  path: gitlab-org/build/CNG
  link: https://gitlab.com/gitlab-org/build/CNG
  description: |
    Docker images per component needed to run GitLab

release-cli:
  name: Release CLI
  path: gitlab-org/release-cli
  link: https://gitlab.com/gitlab-org/release-cli
  description: |
    The GitLab Release CLI consumes instructions in the :release node of the .gitlab-ci.yml to create a Release object in GitLab Rails.
    GitLab Release is a CLI application written in Golang.

gitlab-gollum-lib:
  name: gitlab-gollum-lib
  path: gitlab-org/gollum-lib
  link: https://gitlab.com/gitlab-org/gollum-lib
  description: |
    This gem provides some functionality for GitLab wikis.
    Forked from https://github.com/gollum/gollum-lib.

    The assignments also cover the dependant gem https://gitlab.com/gitlab-org/gitlab-gollum-rugged_adapter,
    which is forked from https://github.com/gollum/rugged_adapter.

sidekiq-reliable-fetch:
  name: GitLab Sidekiq Reliable Fetcher
  path: gitlab-org/sidekiq-reliable-fetch
  link: https://gitlab.com/gitlab-org/sidekiq-reliable-fetch
  description: |
    A Sidekiq module to make it handle jobs reliably.

marketing-operations:
  name: Marketing Operations
  path: gitlab.com/gitlab-com/marketing/marketing-operations
  link: https://gitlab.com/gitlab-com/marketing/marketing-operations
  description: |
    Marketing Operations (MktgOps) supports the entire Marketing team to streamline processes and manage related tools.

peopleops-splinter-pto:
  name: Splinter PTO
  path: gitlab.com/gitlab-com/people-group/peopleops-eng/splinter
  link: https://gitlab.com/gitlab-com/people-group/peopleops-eng/splinter
  description: |
    This gem is a wrapper for the PTO by Roots API.

peopleops-nominator-bot:
  name: Nominator
  path: gitlab.com/gitlab-com/people-group/peopleops-eng/nominatorbot
  link: https://gitlab.com/gitlab-com/people-group/peopleops-eng/nominatorbot
  description: |
    This is a slack bot created to use within GitLab to nominate team members for discretionary bonuses.

peopleops-assessment-tool:
  name: Assessment Tool
  path: gitlab.com/gitlab-com/people-group/peopleops-eng/assessment-tool
  link: https://gitlab.com/gitlab-com/people-group/peopleops-eng/assessment-tool
  description: |
    A tool to support GitLab people managers to perform performance/potential assessments.

quality-ssh-tunnel:
  name: Quality SSH Tunnel
  path: gitlab.com/gitlab-org/quality/ssh-tunnel
  link: https://gitlab.com/gitlab-org/quality/ssh-tunnel
  description: |
    Docker image that provides internet connection to docker containers from CI.

peopleops-compensation-calculator:
  name: Compensation Calculator
  path: gitlab.com/gitlab-com/people-group/peopleops-eng/compensation-calculator/
  link: https://gitlab.com/gitlab-com/people-group/peopleops-eng/compensation-calculator/
  description: |
    Project that houses all the code and data files for our compensation calculator.

gitlab-experiment:
  name: GitLab Experiment
  path: gitlab-org/gitlab-experiment
  link: https://gitlab.com/gitlab-org/gitlab-experiment
  description: |
    GitLab Experimentation library.

gitlab-rspec-profiling-stats:
  name: GitLab RSpec Profiling Statistics
  path: gitlab-org/rspec_profiling_stats
  link: https://gitlab.com/gitlab-org/rspec_profiling_stats
  description: |
    A simple CI job which generates basic statistics about most
    expensive RSpec tests. Identifying slowest / most expensive tests can help with
    optimizing our code and our specs.
