- name: Infrastructure Hiring Actual vs Plan
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  parent: "/handbook/people-group/performance-indicators/"
  definition: Employees are in the division "Engineering" and department is "infrastructure".
  target: greater than 0.9
  org: Infrastructure Department
  is_key: false
  health:
    level: 3
    reasons:
    - 1 new hire in Dec (Scalability BE Eng)
    - Currently hiring for 5 positions (3x DBRE, 2x SRE). 
  sisense_data:
    chart: 8637909
    dashboard: 528609
    embed: v2
  urls:
  - "/handbook/hiring/charts/infrastructure-department/"
- name: Infrastructure Non-Headcount Plan vs Actuals
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  parent: "/handbook/engineering/performance-indicators/#engineering-non-headcount-budget-vs-plan"
  definition: We need to spend our investors' money wisely. We also need to run a responsible business to be successful, and to one day go on the public market.
  target: Identified in Sisense Chart
  org: Infrastructure Department
  is_key: false
  health:
    level: 3
    reasons:
    - Currently running approximately 21% of plan YTD as of Nov 30
- name: GitLab.com Availability
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  definition: Percentage of time during which GitLab.com is fully operational and providing service to users within SLO parameters. Definition is available on the <a href="https://gitlab.com/gitlab-com/dashboards-gitlab-com/-/metrics/sla-dashboard.yml?environment=1790496&duration_seconds=2592000">GitLab.com Service Level Availability page</a>.
  target: equal or greater than 99.95%
  org: Infrastructure Department
  is_key: true
  urls:
  - https://gitlab.com/gitlab-com/dashboards-gitlab-com/-/metrics/sla-dashboard.yml?environment=1790496&duration_seconds=2592000
  health:
    level: 3
    reasons:
    - Dec 2020 Availability 99.96%
    - Nov 2020 Availability 99.90%
    - Oct 2020 Availability 99.74%
    - Sept 2020 Availabilty 99.95%
    - Aug 2020 Availability 99.87%
    - July 2020 Availability 99.81%
    - June 2020 Availability 99.56%
    - May 2020 Availability 99.58%
- name: Mean Time To Production (MTTP)
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  definition: Measures the elapsed time (in hours) from merging a change in <a href="https://gitlab.com/gitlab-org/gitlab">gitlab-org/gitlab projects</a> master branch, to deploying that change to gitlab.com. It serves as an indicator of our speed capabilities to deploy application changes into production.
  target: less than 12 hours
  org: Infrastructure Department
  is_key: true
  urls:
  - https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/170
  health:
    level: 3
    reasons:
    - In September 2020, the target is lowered to 12 hours (previously 24).
    - The next step for this metric is to move from Continuous Delivery to Continuous Deployment for GitLab.com. Work is tracked in <a href="https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/280"> epic 280 </a>.
    - As of Nov, iteration has been completed for some of the Assisted deployment work. In Q4 a key project, <a href="https://gitlab.com/gitlab-com/gl-infra/mstaff/-/issues/20"> implementation of release-tools </a> will assist this effort indirectly as well as be measured for not adversely impacting current MTTP performance.
    - MTTP increase in Nov as a result of the [PCL](https://about.gitlab.com/handbook/engineering/infrastructure/change-management/#production-change-lock-pcl) in place.  
    - The target will be considered for lowering further once we achieve three consecutive months below the target AND when we introduce(and successfully use) automated rollbacks for three consecutive months.
  sisense_data:
    chart: 10055732
    dashboard: 764878
    embed: v2
- name: Mean Time Between Failure (MTBF)
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  definition: MTBF measurement currently under discussion in <a href="https://gitlab.com/gitlab-com/gl-infra/mstaff/-/issues/17"> mstaff17</a>
  target: TBD
  org: Infrastructure Department
  is_key: false
  urls:
  - https://gitlab.com/gitlab-com/gl-infra/mstaff/-/issues/17
  health:
    level: 0
    reasons:
    - This metric was re-introduced in October 2020
    - The definition of "failure" will be updated based on the outcome of current discussions which are focused on timing between deployment failures or timing between SLA failures.
    - Another approach may be to extract deployment failures (and the mean time between them) into a separate metric to serve as a quality countermeasure for MTTP
    - Discussion about this metric is taking place in <a href="https://gitlab.com/gitlab-com/gl-infra/mstaff/-/issues/17">this issue</a>
- name: Mean Time Between Incidents (MTBI)
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  definition: Measures the mean elapsed time (in hours) from the start of one production incident, to the start of the next production incident. It serves primarily as an indicator of the amount of disruption being experienced by users and by on-call engineers. This metric includes only <a href="https://about.gitlab.com/handbook/engineering/quality/issue-triage/#availability">Severity 1 & 2 incidents</a> as these are most directly impactful to customers. This metric can be considered "MTBF of Incidents".
  target: more than 120 hours
  org: Infrastructure Department
  is_key: false
  urls:
  - https://app.periscopedata.com/app/gitlab/764913/MTBF-KPI-Dashboard?widget=10056392&udv=0
  health:
    level: 1
    reasons:
    - This metric updated in December 2020 to scope to only Severity 1 & 2 incidents as these are most impactful to customers.
    - Metric update also changed the target from 24 hours to 120 hour with the intent that we should not have such incidents more than approximately weekly (hopefully less).  Furter iterations will increase this target when we incorporate environment (production only).
    - Deployment failures (and the mean time between them) will be extracted into a separate metric to serve as a quality countermeasure for MTTP, unrelated to this metric which focuses on declared service incidents.  
  sisense_data:
    chart: 10056392
    dashboard: 764913
    embed: v2
- name: Infrastructure Hosting Cost per GitLab.com Unique Monthly Active Users
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  definition: This metric reflects an estimate of the dollar cost necessary to support one user in GitLab.com. It is an important metric because it allows us to estimate infrastructure costs as our user base grows. Infrastructure Hosting Cost comes from Netsuite; it is a sum of actual amounts within GitLab.com:Marketing and GitLab.com departments, with account names '5026 - Hosting Services COGS' or '6026 - Hosting Services'. This cost is divided by <a href="/handbook/product/performance-indicators/#unique-monthly-active-users-umau">UMAU</a>
  target: less than .80
  public: false
  org: Infrastructure Department
  is_key: true
  urls:
  - https://app.periscopedata.com/app/gitlab/764957/Infrastructure-Hosting-Cost-UMAU-KPI
  health:
    level: 2
    reasons:
    - See notes in Key Agenda Doc.
- name: Infrastructure Hosting Cost per Free GitLab.com Unique Monthly Active Users
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  definition: This metric reflects an estimate of the dollar cost necessary to support one user in GitLab.com at the Free tier. As we approach 2021 we're intending to dedicate effort to improving the spend efficiency for the Free user tier. 
  target: less than .50
  public: false
  org: Infrastructure Department
  is_key: true
  urls:
  - https://app.periscopedata.com/app/gitlab/753733/WIP:-Estimated-Infra-Cost-user?widget=9994620&udv=1137020
  health:
    level: 2
    reasons:
    - See notes in Key Agenda Doc.
- name: Infrastructure Average Location Factor
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  definition: We remain efficient financially if we are hiring globally, working asynchronously, and hiring great people in low-cost regions where we pay market rates. We track an average location factor by function and department so managers can make tradeoffs and hire in an expensive region when they really need specific talent unavailable elsewhere, and offset it with great people who happen to be in low cost areas.
  target: equal or less than 0.58
  org: Infrastructure Department
  is_key: false
  health:
    level: 2
    reasons:
    - We are close to our target, but must focus on more efficient LF sourcing for future headcount.
    - Two recent attritions have slightly increased the remaining ALF
    - We are currently hiring for backfills and new positions in support of increased database and search platform support where we will look to further improve LF.
  sisense_data:
    chart: 6847486
    dashboard: 528609
    embed: v2
- name: Infrastructure Department New Hire Average Location Factor
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  parent: "/handbook/engineering/performance-indicators/#engineering-division-new-hire-average-location-factor"
  definition: We remain efficient financially if we are hiring globally, working asynchronously, and hiring great people in low-cost regions where we pay market rates. We track an average location factor for team members hired within the past 3 months so hiring managers can make tradeoffs and hire in an expensive region when they really need specific talent unavailable elsewhere, and offset it with great people who happen to be in more efficient location factor areas with another hire. The historical average location factor represents the average location factor for only new hires in the last three months, excluding internal hires and promotions. The calculation for the three-month rolling average location factor is the location factor of all new hires in the last three months divided by the number of new hires in the last three months for a given hire month. The data source is BambooHR data.
  target: 0.58
  org: Infrastructure Department
  is_key: false
  health:
    level: 3
    reasons:
    - No recent new hires. Current hiring efforts will provide new opportunity to influence this metric.
  sisense_data:
    chart: 9389296
    dashboard: 719549
    embed: v2
- name: GitLab.com known application scaling bottlenecks
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  definition: This metric needs to show how much knowledge we have over the scaling bottlenecks for each of the signifficant GitLab services on GitLab.com. While any specific service is owned by Development department, operating the service, and keeping it performant, at scale is in purview of the Infrastructure department.
  target: 100%
  org: Infrastructure Department
  is_key: false
  urls:
  - https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/296
  - https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/382
  health:
    level: 1
    reasons:
    - In December 2020, we began to implement a Service Maturity Model. We will assess one service (Sidekiq) to check that the idea is valid https://gitlab.com/gitlab-com/gl-infra/scalability/-/issues/750 before introducing a goal and associated metric.
    - In October and November 2020, we found that the missing part of this metric is the understanding of the service maturity and how the service contributes to system failures. We introduced MTBF in October, and we are working on <a href="https://gitlab.com/gitlab-com/gl-infra/scalability/-/issues/605"> manually tracking service maturity </a> as a part of this PI.  
    - In September 2020, we have 13% of the target. We have gathered in depth knowledge of 2 important services and taking action to resolve some pressing issues. Additionally, we exposed the need to explicitly prioritise services prior to venturing service exploration.
    - This metric was introduced in August 2020, and is manually tracked. Work on this metric is tracked in <a href="https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/296"> the epic gl-infra/296 </a>.
    - This metric alone will not be sufficient to describe our knowledge of the system as a whole, but is proving to be useful as an indicator of the level of detail we have for each service.
- name: Infrastructure Handbook Update Frequency
  base_path: "/handbook/engineering/infrastructure/performance-indicators/index.html#infrastructure-handbook-update-frequency"
  parent: "/handbook/engineering/performance-indicators/#engineering-handbook-update-frequency"
  definition: The handbook is essential to working remote successfully, to keeping up our transparency, and to recruiting successfully. Our processes are constantly evolving and we need a way to make sure the handbook is being updated at a regular cadence. This data is retrieved by querying the API with a python script for merge requests that have files matching `/source/handbook/engineering/**` or `/source/handbook/support/**` over time.
  target: greater than 50 per month
  org: Infrastructure Department
  is_key: false
  health:
    level: 2
    reasons:
    - Continuing to watch this metric as well as work to implement the data changes which will incorporate more of our [activity from ops.gitlab.net](https://gitlab.com/gitlab-data/analytics/-/issues/2654).
  sisense_data:
    chart: 8073971
    dashboard: 621063
    shared_dashboard: b69578ca-d4a6-4a99-b06f-423a3683446c
    embed: v2
- name: Infrastructure Discretionary Bonus Rate
  base_path: "/handbook/engineering/infrastructure/performance-indicators/index.html#infrastructure-discretionary-bonuses"
  definition: Discretionary bonuses offer a highly motivating way to reward individual GitLab team members who really shine as they live our values. Our goal is to award discretionary bonuses to 10% of GitLab team members in the Infrastructure department every month.
  target: near 10%
  org: Infrastructure Department
  is_key: false
  health:
    level: 3
    reasons:
    - August was 3%, September 5% (2 bonuses), October 3% (1 bonus), November 3% (1 bonus)
    - Will continue to monitor. No corrective action at this point.
- name: Mean Time To Detection (MTTD)
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  definition: Measures the elapsed time it takes us to detect the onset of an anomalous condition and its actual detection, and serves as an indicator of our ability to monitor the environment and minimize incident resolution.
  target: TBD
  org: Infrastructure Department
  is_key: false
  urls:
  - https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/8090
  health:
    level: 0
    reasons:
    - We can’t properly evaluate the KPI at this time due to inconsistent data.
    - Metric will be refactored as part of GitLab Incident Management adoption in Q4 FY21
- name: Mean Time To Resolution (MTTR)
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  definition: Measures the elapsed time in hours it takes us to recover when an incident occurs, and serves as an indicator of our ability to execute said recoveries. (Only includes severity::1 & severity::2 incidents)
  target: less than 1 hour
  org: Infrastructure Department
  is_key: false
  urls:
  - https://gitlab.com/gitlab-com/gl-infra/production/-/boards/1717012?&label_name[]=incident
  - https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/8296
  health:
    level: 0
    reasons:
    - Data is not accurate as of May when a switch to using a workflow for incidents leveraging scoped labels was made.
    - Metric will be refactored as part of GitLab Incident Management adoption in Q4 FY21
- name: Disaster Recovery (DR) Time-to-Recover
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  definition: Tracks time to recover full operational status in case of a catastrophic incident in our primary production environment.
  target: TBD
  org: Infrastructure Department
  is_key: false
  urls:
  - https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/8293
  health:
    level: 0
    reasons:
    - Published handbook update at end of Q3 to establish <a href="https://about.gitlab.com/handbook/engineering/infrastructure/product-management/proposals/disaster-recovery/"> DR Strategy</a> (following on the recently published SaaS overall strategy).
    - <a href="https://about.gitlab.com/company/team/structure/working-groups/disaster-recovery/"> DR Workgroup</a> also started for effort in Q4 to accomplish Geo failover testing in Staging, then continue with expanding scope.
- name: PostgreSQL Recovery Health
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  definition: Tracking two key metrics for DB recovery health - Recovery Test Success Rate, Recovery Test Elapsed Time.
  target: TBD
  org: Infrastructure Department
  is_key: false
  urls:
  - https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/9240
  - https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues/11731
  health:
    level: 0
    reasons:
    - Recovery will be an expanding focus throughout FY21. While we have automated backup process as well as automated recovery testing, this PI intends to improve visibility to this important process as well as observe to ensure recovery time remains within reasonable bounds to support overall recovery capabilities.
- name: Infrastructure Department Narrow MR Rate
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  definition: Infrastructure Department <a href="/handbook/engineering/#merge-request-rate">MR Rate</a> is a performance indicator showing how many changes the Infrastructure team implements in the GitLab product, as well as changes in support of the GitLab SaaS infrastructure.  We currently count all members of the Infrastructure Department (Directors, Managers, ICs) in the denominator, because this is a team effort. The <a href="/handbook/engineering/merge-request-rate/#projects-that-are-part-of-the-product">projects that are part of the product</a> contributes to the overall product development efforts.
  target: Greater than TBD MRs per month
  org: Infrastructure Department
  is_key: false
  health:
    level: 1
    reasons:
    - We have known projects which are not currently included in the measure. [Working with the Data team to resolve this](https://gitlab.com/gitlab-data/analytics/-/issues/2654).
    - We intend to observe this metric over time to understand the behavior of the differing work patterns and MR content (product code + infrastructure logic + configuration data).
  urls:
  - https://gitlab.com/gitlab-data/analytics/-/issues/4445
  sisense_data:
    chart: 8934544
    dashboard: 686934
    shared_dashboard: 178a0fbc-42a9-4181-956d-a402151cf5d8
    embed: v2
- name: Corrective Action SLO
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  definition: The Corrective Actions (CAs) SLO focuses on the number of open severity::1/severity::2 Corrective Action Issues past their due date. All Corrective Actions are in scope, regardless of department or project. CAs and their due dates are defined in our <a href="/handbook/engineering/infrastructure/incident-review/#incident-review-issue-creation-and-ownership">Incident Review process</a>.
  target: 0
  org: Infrastructure Department
  is_key: false
  health:
    level: 1
    reasons:
    - We started tracking this in July 2020. Many of our Corrective Actions do not have a proper Severity assigned yet (88 as of 2020/12/03)
    - Current CA SLO performance: 5/33 (2020/12/03)
  urls:
    - https://about.gitlab.com/handbook/engineering/infrastructure/incident-review/#incident-review-issue-creation-and-ownership
- name: Production Access Risk Index
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  definition: The Production Access Risk Index provides a representative measure of the amount of access Team Members have to systems or sources of <a href="https://about.gitlab.com/handbook/engineering/security/data-classification-standard.html#red">Red or Orange data</a> as well as underlying infrastructure control systems which could directly impact this data.
  target: TBD
  org: Infrastructure Department
  is_key: false
  urls:
  - https://app.periscopedata.com/app/gitlab/792844/Infra-Prod-Console-Access
  - https://gitlab.com/gitlab-com/gl-infra/infrastructure/-/issues/11179
  health:
    level: 2
    reasons:
    - Current index is an MVC started in Dec 2020.
    - Current weights for underlying data elements are 18 for RW Rails Console, 10 for RO Database, and 18 for GCP production environment.
    - Future iterations to include more systems and further refinement of weighting.
    - Started tracking some key access base metrics in Sept.
  sisense_data:
    chart: 10520523
    dashboard: 792844
    embed: v2
