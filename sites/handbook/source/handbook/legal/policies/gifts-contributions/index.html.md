---
layout: handbook-page-toc
title: "Policies related to Gifts and Contributions"
description: "This set of policies relates to GitLab's stance and process for giving and accepting gifts and contributions"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

### Gifts and Entertainment

Modest gifts, favors, and entertainment are often used to strengthen business relationships. However, no gift, favor, or entertainment should be accepted or given if it obligates, or appears to obligate, the recipient, or if it might be perceived as an attempt to influence fair judgment.

In general, unless you have supervisory approval you should not provide any gift or entertainment to customers, suppliers, or others that you would not be able to accept from a customer, supplier, or other applicable parties.

All directors, executives, and anyone else in the company participating in vendor selection, must disclose all gifts and entertainment valuing over US$250 for the six months prior to the vendor selection and during the term of the services and for a period of twelve months after services have been completed. The disclosure shall be made to the Legal department, and shall include the value of the gift or entertainment, the individual or company providing the gift, favor, or entertainment, and the date on which it was received. If you have any questions relating to this section, feel free to contact the Legal department.

### Political Activities and Contributions

You may support the political process through personal contributions or by volunteering your personal time to the candidates or organizations of your choice. These activities, however, must not be conducted on company time or involve the use of any company resources. You may not make or commit to political contributions on behalf of GitLab.

### Charitable Contributions

We support community development throughout the world. GitLab employees or contractors may contribute to these efforts, or may choose to contribute to organizations of their own choice. However, as with political activities, you may not use company resources to personally support charitable or other non-profit institutions not specifically sanctioned or supported by our company. You should consult the Legal department if you have questions about permissible use of company resources.
