---
layout: handbook-page-toc
title: "Greenhouse"
description: "Greenhouse is GitLab's ATS (Applicant Tracking System). All Hiring Managers and Interviewers will use Greenhouse to review resumes, provide feedback, communicate with candidates, and more."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## For all Greenhouse users

### How to join Greenhouse

[Greenhouse](https://www.greenhouse.io) is GitLab's ATS (Applicant Tracking System). All Hiring Managers and Interviewers will use Greenhouse to review resumes, provide feedback, communicate with candidates, and more.

You will receive an email to log-in to Greenhouse for the first time. When you [log-in to Greenhouse](https://app2.greenhouse.io/users/sign_in), enter your GitLab email, click `Next`, and fill in your [Okta](/handbook/business-ops/okta/) information as prompted. All GitLab team-members are granted *Basic* access, which will allow you to track *Referral* submissions and share requisition postings on social media. Until your access is changed by an Admin, you will only have access to the Greenhouse [dashboard](https://app2.greenhouse.io/dashboard).

During onboarding, all new team members are assigned permissions based on their role in the hiring process. If a current team member needs to change their access level, they'll need to submit a [new access request issue](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/new?issuable_template=New_Access_Request) in the GitLab `access-requests` project. Their Manager and the Recruiting Team will be pinged to approve and action that request.

### Your dashboard

The Greenhouse [dashboard](https://app2.greenhouse.io/dashboard) will be your primary page for activities in Greenhouse. All team members have access to this page. However, depending on your role in the hiring process, you may have more dashboard widgets than others. All team members can see the `Add a Referral`, `My Referrals`, and `Share Jobs with your Social Network` sections (all of which are described in more detail below). On the righthand panel, there is a section called `Helpful Links`, which is home to several self-help resources, as well as the Greenhouse Support Team.

Members of a Hiring Team may have additional sections, such as `My Interviews`, which contains a preview of your next three scheduled interviews. That section will link to the candidate's profile and interview kit; click `See All Interviews` to view the full list of upcoming interviews and `See Past Interviews` to view completed interviews. You may also see `My Reminders`, which houses reminders that you set for yourself.

A Recruiter will see an additional section called, `Applications to Review`, which will list their requisitions and link to the *Application Review* workflow. There, they can review applications and advance, decline, or submit feedback. This panel only shows the five requisitions with the **most** pending applications. To see all requisitions, click `See All` and select yourself as the responsible user. To note, Hiring Managers do **not** have this section on their dashboard. However, they can view the `Application Review` section by going to a specific requisition and clicking `Application Review` in the righthand panel.

On the righthand panel, members of the Hiring Team can see a list of `My Tasks` and `All Tasks`, which shows how many candidates you have pending action items on. For example, *"Needs Decision"*, *"Candidates to Schedule"*, *"Take Home Tests to Send"*, or *"Offers"*. You can view all pending tasks by clicking `All Tasks`.

Members of the approval process (i.e. Executives, Finance Business Partners, and People Ops) will have an additional section titled `My Approvals`, which contains a list of any requisitions and candidates that require their approval.

There is also a quick link to `People I'm Following`, so you can easily get to their profile. To do that, you'll have to "follow" a candidate; that option can be found on the right side on a candidate's profile page.

The next section is called, `Helpful Links`, which includes a link to our [internal job board](https://app2.greenhouse.io/internal_job_board), a link to a list of our "in-house contacts" (which are GitLab team-members who are our Greenhouse account owners) with their names and emails, and a link to the Greenhouse [Help Center](https://support.greenhouse.io/hc/en-us).

The last button of note is the `Personalize Dashboard` button at the bottom of the righthand panel. You can use this to adjust your settings on your dashboard. For example, to hide or rearrange sections to your liking.

### Sharing the Talent Community on social media

You can share the link to our Talent Community sign-up page. To note, sharing the Talent Community link with someone or on social media does **not** count as a *Referral* and is thus, **not** eligible for the [Referral Bonus](/handbook/incentives/#referral-bonuses).

## For all Interviewers

### Application Review for Interviewers

All applicants begin our hiring process in the `Application Review` stage. If you're an Interviewer for a requisition, you may be part of the review process with the goal of identifying applicants of interest. If this is part of your responsibilities as an Interviewer, we recommend the following:

1. Evaluate new candidates for your vacancies often. You can see a list of jobs you're an Interviewer for by clicking `All Jobs` in Greenhouse and setting the `User` = `Me`.
1. Please note the Recruiter for each of your requisitions. After clicking on a requisition, you should have access to two tabs as an Interviewer: `Candidates` and `Job Setup`. Click `Job Setup` and scroll to the `Hiring Team` section to see who the Recruiter is.
1. Provide the Recruiting Team with your feedback on candidates:
   1. Click on a candidate in the `Candidates` tab to view them for a specific role or click, `All Candidates`, to view all candidates that you're an Interviewer for.
   1. Click `Application` on the left side of an applicant's profile to view their resume and cover letter, if provided. You can also click on the `Details` tab on the top of an applicant's profile under `All Attachements` to see a preview of both the resume and cover letter, if provided. Below the documents, click, `Job Post Questions` to expand and view their answers to specific application questions. If there are **no** job post questions answered, the candidate was likely sourced or exported from an internal job board and would need to have the questions [sent to them](https://support.greenhouse.io/hc/en-us/articles/360002568971-Job-Post-Request-Email#%E2%80%9Cindividual%E2%80%9D). You may send them or you can tag your recruiter to do so.
   1. Evaluate the candidate's resume, cover letter, and job post questions while being [mindful of our hiring principles](/handbook/hiring/principles/). Leave your recommendation on the candidate as a note in the `Make a Note` area in the right sidebar and tag the responsible Recruiter.
    1. If the candidate does not meet the requirements of the requisition or if other candidates are better qualified, you are welcome to directly decline the candidate by **1)** Selecting `Reject`, **2)** Selecting the `Rejection Reason` and adding notes if applicable, and **3)** Selecting the appropriate rejection email template.
            1. Please use the `Default Candidate Rejection` template and send from `noreply@greenhouse.io`
            1. For referred candidates, please select the `Reject referral after resume review` template and ensure it is coming from you so the referred candidates have a point of contact should they require reassigning for the rejection.    

Mentioning the Recruiter for this role is sufficient and leaving a detailed [scorecard](/handbook/hiring/greenhouse/#scorecards) is not necessary for the `Application Review` stage.

Note that these steps may be different for [Hiring Managers](/handbook/hiring/greenhouse/#application-review-for-hiring-managers).

### Feedback and Interview Kits

When you have an interview, you have two ways to get to your Interview Kit to leave feedback for a candidate. The first is by clicking the link in the calendar invitation for the interview. The second is by going to your Greenhouse [dashboard](https://app2.greenhouse.io/dashboard) where you will see a list of your upcoming interviews and can click `See Interview Kit`.

The Interview Kit consists of a few tabs:
- **Interview Prep**
  - Quick overview of what you'll want to look for in the interview, as well as any notes that previous Interviewers left to look into further.
- **Job Details**
  - The full requisition description posted on Greenhouse.
- **Resume**
  - This tab will only show up if the candidate provided a resume.
- **LinkedIn**
  - This tab will only show up if the candidate provided their LinkedIn profile.
- **Scorecard**
  - This is where you will enter all of your notes, score various attributes, and make a decision. (More detail below!)

Additionally, the Interview Kit shows the candidate's name, contact details, pop-out links to their resume, cover letter, and other details, as well as the details for the interview (location, time, etc.) on the lefthand side. It also includes the requisition that the candidate is interviewing for at the top of the page.

#### Scorecards

The scorecard consists of a few elements. In general, they feature text boxes for adding notes and text boxes for addressing specific questions. To note, if a text box is required, there will be a red asterisk by the question.

Underneath the first text box, `Key Take-Aways`, there are two additional links, `Private Note` and `Note for Other Interviewers`, which will open additional text boxes. A `Private Note` is typically used by the Recruiting Team when collecting compensation information and it's only viewable by the Recruiting Team and Hiring Managers for the requisition. The `Note for Other Interviewers` text box is extremely useful to all Interviewers, as it's where you can include information that you think would be relevant to future Interviewers. For example, specifying areas to dig into, further evaluate, or look out for. Any notes in this field will appear in the next Interviewer's Interview Kit on the `Interview Prep` tab.

We want to highlight the strengths and weaknesses of the candidate in an easy to absorb, standardised way. Every scorecard must include"  `Pros` and `Cons`. This helps the recruiting team gather data that will be presented to the candidate in the form of feedback.

Below the text boxes for notes, each role has a list of desired attributes. Each stage has certain attributes highlighted that are recommended points of evaluation. However, **no** attributes are required, so you're welcome to rate any attributes you've gained insight into. All stages include attributes for values-alignment, which all Interviewers are heavily encouraged to complete, so we can assess values-alignment for each candidate.

Below the attributes is the final piece where you will make your decision. Greenhouse says *"Overall Recommendation: Did the candidate pass the interview?"*. This should be interpreted as, *"Do you want to hire this candidate?"* answered appropriately. The final score is not required by Greenhouse, but it must be completed by the Interviewer. If the Interviewer does **not** add their vote, the Recruiting Team will follow-up to fetch their vote. If you are on the fence about a candidate, that's typically an indication that you do not want to hire the candidate. Though, if you're really unsure, feel free to reach out to the Recruiter to discuss further. Your Recruiter may agree with your hesitations and decline or you may agree that there is an element that should be explored in an additional interview.

Your scorecard will automatically save as you enter information. If there is an error and Greenhouse is **not** able to save the scorecard, it will say so at the top righthand corner and you'll be unable to submit your scorecard. You can either wait until it does save or open the Interview Kit again and copy/paste the information over. If you have further issues, please reach out to the Recruiting Team.

Finally, click `Submit Scorecard` and you're done! From there, you may go back to review and edit your scorecard, view the candidate profile, or return to your dashboard.  In order to ensure a timely response for each candidate and to reduce the possibility of forgetting important details and impressions of the candidate, all scorecards should be submitted within 3 business days of the interview.  

In order to help remove bias, Interviewers (unlike Hiring Managers) are **not** able to see another Interviewer's scorecard until they've first submitted their own. If there are certain flags or notes that should be highlighted to the rest of the Interview Team, the Interviewer should add a note in their scorecard by clicking the `Note for Other Interviewers` section right underneath the `Key Take-Aways` text box in their scorecard. You are also able to add notes outside of your scorecard that will be visible to future Interviewers by going to the candidate profile and to the text box on the right side under `Make a Note`, adding your note, and checking off the radio button to make it `Visible to Interviewers`.

#### Interview notifications

You can set up reminders in Greenhouse by going to your [account settings](https://app2.greenhouse.io/myinfo) and turning on `Daily interview reminder email` which will email you each morning with a list of your interviews for the day. You can also [connect your Slack account](https://support.greenhouse.io/hc/en-us/articles/207344866-I-use-Slack-How-do-I-set-up-the-integration-to-receive-notifications-directly-in-Slack-) to your Greenhouse account and receive reminders after the interview is over if your scorecard is still due, as well as other notifications depending on your settings.

### Searching candidates

Since we have a great pool of talent within our ATS, Greenhouse enables the Hiring Teams to search for specific attributes of candidates. To search for keywords, go to the candidates tab at the top of the Greenhouse website, use the search bar on the lefthand sidebar to input your keywords, and enable to option `Full Text Search`.

A popular method of searching is [with boolean queries](https://support.greenhouse.io/hc/en-us/articles/202360199-Search-Candidates-Using-Boolean-Queries-), which allow you to combine keywords or phrases to get more relevant results. For example, `engineer AND "ruby on rails"`. All fields in a candidate resume, application, forms are searchable. You're also able to sort candidates via defined filters in the sidebar.

### Email syncing

All email communication with candidates must be kept in Greenhouse. Only Job Admins (Hiring Managers, Executives, and People Ops) are able to email candidates from Greenhouse. If you are **not** a Job Admin and want to email your candidate, please reach out to your Recruiter. In order to ensure that the candidate's responses are also kept in Greenhouse, you have two options (there is no automatic sync). The first option is to use a [Google Add-on "Greenhouse for Gmail"](https://support.greenhouse.io/hc/en-us/articles/360003111031-Greenhouse-for-Gmail-Google-Add-on-) which will allow you to sync any emails you receive from within your gmail inbox. The second option is to either bcc your outgoing email or forward your received email to `maildrop@ivy.greenhouse.io`.

### Leaving notes

It's always recommended to leave notes in a candidate profile to maintain communication between the Hiring Managers, Interviewers, and Recruiting Team. To leave a note, go to a candidate's profile, and, on the right side under `Make a Note`, type your note and tag anyone you like to see the note. If you do **not** tag anyone, **no** one will receive a notification. Once you're done with your note, click `Save` and it will notify anyone tagged and be logged in the candidate's profile in the `Activity Feed`. Anyone who has access to the requisition will be able to view it there.

Again, `Private Notes` should only be used to discuss compensation or other confidential items related to the candidate. To leave such a note, go to the candidate's profile, click `Private`, scroll to the bottom, and click `Add Private Note`. Be sure to tag anyone that should be notified. Please note that only Job Admins (i.e. **not** Interviewers) are able to view these notes. All Job Admins, including Hiring Managers, are able to see private notes whether or not they are tagged.

### Access to roles

If you need access to review or interview candidates for a requisition, please reach out to the Recruiting Team through Slack or email. The Recruiting Team will verify with the Hiring Manager and provision access accordingly.

## For Hiring Managers

### What access do I have?

Hiring Managers and Executives have access to view all candidates for any requisition where they're listed as a `Hiring Manager`. Additionally, they can view `Private Notes` for those candidates. To note, a Hiring Manager is **not** able to view candidates once they are hired or any candidates outside of a requisition where they're a Hiring Manager.

### How do I view my roles and candidates?

When you log in to Greenhouse, you will see your dashboard; however, this will not paint the full picture for all of your roles. In order to view the status of your current vacancies, click `All Jobs` at the top of the page, then choose whichever job you want to review. There will be an indicator if you are the Hiring Manager for a role or not below the job name, and you will only be able to view the jobs you have access to as a member of the Hiring Team for that role; please note the below details are only accessible to Job Admins, and Interviewers have less functionality available to them.

After you click on a requisition, you will be brought to the job dashboard for that specific requisition, which contains all of the information you should need. At the top of the page, there is a quick snapshot of application trends, showing you how many applications have been received, how many have been rejected, and how many are active. Below that you can see a snapshot of the sources where you are getting your applicants as well as the average quality of the applicants from each source. Next, you'll see a list of any candidates you're following in this role. The last section on this page is `Pipeline Tasks` where any pending tasks for candidates in this role will be shown. You can also view a less detailed version of this task list at the top right sidebar on this page, where you can easily click `Review Applications` (which is the quickest and easiest way to bulk review applications) or click on the number next to each stage to view the candidates in each stage. If you wanted to view all candidates regardless of stage, simply click `Candidates` right above the `Application Trends` section (**not** the `All Candidates` at the very top of the page). Below the snapshot pipeline at the right sidebar, you can view any prospects for this role, Greenhouse predictions of when the role will be filled, and finally job setup links that enable you to adjust the settings for the requisition. It's recommended for only the Recruiting Team to edit the requisition setup, but please reach out to them if you have any questions.

If you want to review the candidates for multiple jobs at the same time, click `All Candidates` instead, and then use the filter on the left sidebar, `Jobs` > `Filter by Job` > select the appropriate jobs. You can use the filters to further narrow down candidates.

### How do I review candidates in the Talent Community prospect pool?

As a non-recruiting team member, you would need to be added as a `Pool Admin` in a specific prospect pool within Greenhouse. You can reach out to your recruiter or [create an issue](https://gitlab.com/gl-recruiting/operations/-/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=) to request access.

### Configuring notifications

Notifications are configured as part of the [requisition creation process](/handbook/hiring/recruiting-framework/req-creation/#requisition-creation-process) but can be adjusted at any time. To set up notifications, go to the requisition's job dashboard and click on `Job Setup` at the top, then `Notifications` at the left. Under each section, you can then click `Edit` to add or remove yourself for that section.

### Reporting

Hiring Managers have the ability to quickly view and pull reports for their vacancies. To do so, go to the job dashboard of the requisition you want to report on, then click `Reports`. From there, it will show you a variety of different reports you can pull depending on what you're looking for, and each one will dynamically show you the report which you are able to use some filters on. You're also able to [save reports](https://support.greenhouse.io/hc/en-us/articles/115003218066-Saved-Reports) to come back to them later, [share a report via email](https://support.greenhouse.io/hc/en-us/articles/115003338503-Email-a-Report) to [set up recurring reports](https://support.greenhouse.io/hc/en-us/articles/115003711906-Schedule-Recurring-Reports) to have reports emailed to you or someone else on a weekly basis, and to download the report as an excel file for easy sharing through Google Sheets.

### Candidate Review for Hiring Managers

To view candidates in the `Application Review` stage, go to the job dashboard for that requisition. On the right sidebar, under `Pipeline`, you can see how many applicants are in that stage. If you click, `Review Applications`, you'll be taken to a portal that shows each candidate's name (which is an active link that will open a new tab to the candidate's profile), submission date, answers to application questions (if applicable), resume, cover letter, and any other details of their candidacy. `Application Review` will show you the submissions with the least recent activity (whether that is their submission date or any activity on their profile took place), so you can work your way from oldest to newest. You are able to advance, skip, or reject candidates, as well as leave feedback with any of those decisions. It's recommended to always include feedback when reviewing applications so the Recruiting Team is aware of your thought process. It is perfectly acceptable to skip candidates you are not sure of and tag your recruiter in the feedback section for them to review and take action on. If you do advance a candidate, be sure that your recruiter either receives notifications when candidates change stages or tag them in feedback and ask them to move forward (the safest option). You can also "skip" the candidate and leave feedback for the recruiter to reject, which is recommended.

Some roles may be sent questions after we've sourced them. If this is applicable you can view the questions and answers outside of the review portal, go to their profile, click `Application` on the left side, scroll down and expand `Job Post Questions`. If there are no questions listed this was not a step in this search.

## For Recruiting

### Access levels and permissions

Greenhouse has a variety of user-permissions for ensuring that team members have the appropriate access per assigned vacancies. By default, any `Job Admin` will have the following permissions on an assigned requisition:
- The ability to see dashboard, pipeline, and reports.
- The ability to see all candidates.
- The ability to add and edit candidates and referrals.
- The ability to be assigned Hiring Team roles.

Below is a list of the various access levels, by team, and what they generally entail:

- The **Recruiting Team** (*excluding* the VP of Recruiting, Recruiting Program Analyst, Recruiting Operations Coordinator, and  Manager, Recruiting Operations) should be assigned `Job Admin: Recruiting` permissions for all *Open-*, *Closed-*, and *All Future Jobs* regardless of their involvement with a particular department (e.g. if they're an Interviewer or Coordinator).
- The **People Ops Team** (*excluding* the CPO and Recruiting Team) should be assigned `Job Admin: People Ops` permissions for all *Open-*, *Closed-*, and *All Future Jobs* regardless of their involvement with a particular department (e.g. if they're an Interviewer).
- All **Executives** (*excluding* the CEO and CPO) should be assigned `Job Admin: Job Approval` permissions for all *Open-*, *Closed-*, and *All Future Jobs* in their respective department. Such permissions allow them the same access as a Hiring Manager, plus the ability to approve new vacancies and offers.
- All **Hiring Managers** (*excluding* Executives) should be assigned `Job Admin: Hiring Manager` permissions for all *Open-*, *Closed-*, and *All Future Jobs* in their respective department. However, there are a few exceptions:
    1. If they're a Hiring Manager for **only** one requisition.
        * In this scenario, they should **not** be given access to *All Future Jobs*, but only to the current vacancies where they are the Hiring Manager.
    2. If they're on the Hiring Team for a requisition **outside** of their own team.
        * In this scenario, they should be given `Interviewer` access for that requisition only.
- All **Executive Assistants** should be assigned `Job Admin: Scheduling` permissions for all *Open-*, *Closed-*, and *All Future Jobs* regardless of their involvement with a particular department (e.g. if they're an Interviewer).
- Any user that needs to access all vacancies and data, including diversity data, should be assigned `Site Admin` permissions.
    * The current `Site Admins` include: the CEO, CFO, CPO, VP of Recruiting, Recruiting Program Analyst, Recruiting Operations Coordinator, and Manager, Recruiting Operations.
- Any user on a Hiring Team that is expected to interview candidates or review applications should be assigned `Interviewer` permissions for **only** the vacancies where their participation is expected. Though, if the user is expected to interview candidates for **all** vacancies within a particular department, then they can be assigned the `Interviewer` permission for *All Future Jobs*.
    * This permissions will allow the user to see all candidates in the assigned requisition. However, when it comes to viewing *Scorecards*, they'd only be able to view the *Scorecards* of others **after** submitting their own.
- All other users should be assigned `Basic` permissions.

There are a few other permission levels in Greenhouse that are not frequently used:
- `Job Admin: Standard`: grants default `Job Admin` permissions, plus the ability to advance or reject candidates.
- `Job Admin: Limited`: grants default `Job Admin` permissions, but it **doesn't** have the ability to advance or reject candidates.
- `Job Admin: Recruiting Agency`: this access level is designated for **Recruiting Agencies** only when they need the ability to advance or reject candidates.

To note, if a requisition is marked as "Closed" in the `Job Setup` > `Job Info` > `Job Status`, then `Interviewers` only will **not** be able to see any candidates in that requisition.

#### How to upgrade or change access levels

Only Admins can upgrade another team member's access level. During onboarding, all new team members are added to Greenhouse at the appropriate level listed above. If a current team member needs to change their access level, they will need to create a [new access request issue](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/new?issuable_template=New_Access_Request) in the GitLab `access-requests` project.

The default access level is *Basic*, but anyone reviewing and interviewing candidate profiles should be upgraded to the *Interviewer* role. To upgrade access levels:
 1. Go to [settings](https://app2.greenhouse.io/configure) and click on [users](https://app2.greenhouse.io/account/users?status=active)
 1. Search for the team member in the search bar; if their name appears, click on their name
 1. Click `Edit` in the permissions section. Choose the `Job Admin/Interviewer` option
 1. Click `Add` under `Job-Based Permissions`
 1. Search for the job they should have access to and click `Add` next to it, then choose `Interviewer`

For any team members who are Hiring Managers (or above), choose `Job Admin: Hiring Manager` for the specific roles they should have access to; if they are a Hiring Manager for an entire department, they should also be added as `Job Admin: Hiring Manager` for future roles within that department. If they are a Hiring Manager or above for a role, they should always be marked as `Job Admin: Hiring Manager` and **no** `Interviewer` even if they will be interviewing, since that will limit their access. Meanwhile, the Executive for a division (e.g. CRO, CMO, VPE, etc.) should receive `Job Admin: Job Approval` for all of their division's current and future roles in order to give them the same permissions as `Job Admin: Hiring Manager` as well as the ability to approve new vacancies.

Similarly, Recruiting receives `Job Admin: Recruiting` for all current and future roles; People Ops receives `Job Admin: People Ops` for all current and future roles.

There are additional "User-Specific Permissions" listed beneath the job-based permissions.
1. For the People Ops Team, check off `Can invite new users to Greenhouse and reactive disabled users`, `Can manage unattached prospects`, and `Can invite and deactivate agency recruiters`.
1. For the Recruiting Team, check off each additional option.
1. Site Admins should typically have access to all of the additional options, but *always* be sure to check off `Can see EEOC and demographic reports`, `Can create and view private candidates`, and `Can see private notes, salary info, manage offers, and approve jobs/offers`.

#### Adding an agency recruiter to Greenhouse

To add an agency recruiter to Greenhouse and their agency doesn't exist, then a [new agency](https://support.greenhouse.io/hc/en-us/articles/201078255-Add-a-New-Agency) will need to be created. If the agency does exist, this is how to add a [new recruiter](https://support.greenhouse.io/hc/en-us/articles/200666389).

If an agency recruiter will be advancing-  and/or declining candidates, then they'll need an Okta account in order to have `Job Admin` permissions.

To provision an Okta account:

- Complete and submit an [access request issue](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/new) using the `okta access to greenhouse (external)` template.
- A member of the **Security Team** will create the account and a member of the **People Group** will set the necessary permissions in Greenhouse.

#### Linking users to their profiles

All hired candidates are marked as private when they are hired. Meanwhile, all Site Admins, Job Admins: Recruiting, and Job Admins: People Ops have access to view private candidates. As a result, any new team members who fall into one of those permission levels needs to have their Greenhouse user account linked to their candidate profile. Please note **only** Site Admins can link a user to a candidate profile. To do so, go to [settings](https://app2.greenhouse.io/configure), click on [users](https://app2.greenhouse.io/account/users?status=active), search for the team member in the search bar, click on their name, click `Link to Candidate`, then search for the team member's name again, and click `Link` on the appropriate profile, then scroll to the bottom and click `Save`. You can also follow [the instructions](https://support.greenhouse.io/hc/en-us/articles/360020922752-Link-User-Account-to-Candidate-Profile-) which include screenshots on Greenhouse's website.

Any internal candidate who applies to a requisition via the internal job board will automatically have their user profile and candidate profile linked.

#### Enabling LinkedIn Recruiter System Connect

Assuming that you've already been assinged a LinkedIn seat, here's how to enable the LinkedIn _Recruiter System Connect_ integration in Greenhouse. There are **two** steps to the process.

**Step 1:**

1. Click `•••` > `Integrations` from any page in Greenhouse
2. Search *"LinkedIn Recruiter System Connect"* and select the result of the same name
3. Enable the integration

**Step 2**

1. Click `Hi [NAME]` in the top-right corner of Greenhouse > `Account Settings`
2. Scroll down to the **LinkedIn Recruiter System Connect** section in the right column and click `Connect`
3. Click `Allow` and you'll be set-up to access information seamlessly between both systems

If you have **not** been assigned a seat and would like one, please add your GitLab email to your LinkedIn profile and submit an [Access Request](https://gitlab.com/gl-recruiting/operations/issues/new?issue%5Bassignee_id%5D=&issue%5Bmilestone_id%5D=#) issue using the `LinkedIn Access Request` template.

To add your email to your LinkedIn profile, click `Me` > `Settings & Privacy` > `Sign in & security` > `Email addresses` > add your GitLab email address and verify it.

### Configuring notifications

Notifications are configured as part of the [requisition creation process](/handbook/hiring/recruiting-framework/req-creation/#requisition-creation-process) but can be adjusted at any time. To set up notifications, go to the requisition's job dashboard and click on `Job Setup` at the top, then `Notifications` at the left. Under each section, you can then click `Edit` to add or remove people for that section.

It is recommended for the Recruiter of a requisition to set up notifications for themselves regarding new internal applicants, new referrals, new agency submissions, approved to start recruiting, offer fully approved, stage transitions, and new scorecards.

For stage transition and new scorecards, it is possible to select `Candidate's Recruiter` or `Candidate's Coordinator` instead of a particular person for each stage, which is highly recommended.

### Configuring email permissions

Greenhouse allows Job Admins to grant other Job Admins the permissions to send out emails on their behalf. This is helpful, for example, when a recruiter is sending out an availability request email but wants the responding email with the times to go to the coordinator so they can schedule next interviews, they could send the email from the coordinator's email address to ensure that happens. In Greenhouse, the activity feed on the candidate's profile shows who actually sent it and who it came from, so that it is clear.

In order to set up these permissions, the person allowing another person to send emails on their behalf should log in to Greenhouse, hover over their name in the top right corner, and click `Account Settings`. Then scroll down to the section called `Email Permissions`. You can select the first option to allow any Job Admin or Site Admin to send emails on your behalf, or you can select the second option and choose specific people (who are Job Admins or Site Admins) only who are able to send emails on your behalf. You can remove these users or this functionality at any time.

Once you have the permissions to send emails on someone else's behalf, when you are sending an email to a candidate, you can click the "From" button at the top of the email pop up and choose from the list of available email addresses. If you are using an email template, remember to choose the template and then change the sender, or the template will override any changes you've made.

### Changing jobs

If a candidate applies for a job but is a better fit for another job, they will need to be added to a new job. There are a few options to do this. To start, go to the candidate profile and click `Add or Transfer Candidate's Jobs` at the bottom right. From there, you can either add a new job to the candidate profile, which will keep all data within the original job and start a clean slate for the new job. Alternatively, you can also transfer the candidate data to the new job, but this should be done with caution, as it will not transfer any scheduled (i.e. not completed and submitted) interviews or scorecards, and it will remove the candidate from any reports about the original job. Admins are also able to remove a job and its history from a candidate profile, which should be done only when absolutely necessary.

### Auto-tags

It is possible to add tags that automatically link to applicants when they apply based on their answers to certain application questions. To add an auto-tag to a requisition, go to the requisition's `Job Setup` page, then `Job Post`. To the right of the requisition name, there will be a link called `Manage Rules`. Click that, then `Add a Rule`. You can then select which application question on this requisition and what answer you want to create the auto-tag for. (Please note: only application questions that are yes/no, single select, or multi-select supports auto-tags.) For example, you could choose the question, *"Do you have experience with Ruby on Rails?"* If an applicant answers *"No"*, they'd be auto-tagged or auto-rejected based on the given rule. Once you've selected any and all tags you want to be associated with this question and answer, click `Save`. If you want to create auto-tags for multiple questions and/or multiple answers, you will need to repeat this process.

Going forward, all new applicants whose answers in their application questions correlate to the rule you set up will automatically also have the tags you chose. To quickly find candidates, go to either `All Candidates` or the candidates for a specific requisition, open the `Profile Details` section on the left sidebar, and under `Candidate Tag` select the appropriate tag. You can add additional filters as desired, and you can then perform bulk actions on these candidates by using the bulk function on the top right.

### Global Self-Identification Survey

Anyone who reaches the `Screening` stage must be sent out the **Global Self-Identification Survey**, including current team members. This should be sent separately, yet at the same time, as the Screening invite, or any other communication. Keep in mind, if the role is **only** open to current team members, the GSIS does not need to be created or sent to internal candidates. Ensure the req is labeled accordingly; additional details found [here](https://about.gitlab.com/handbook/hiring/recruiting-framework/req-creation/#publishing-the-job/#9).

**To Send Survey**

1. From Candidate profile, navigate to `Application` (on left hand side)
1. Navigate to `Job Post` > `Job Post Questions`
1. Select `Send`
1. Ensure Job Post: `Global Self-Identification Survey (Remote)` is [offline]
1. Check: `Hide basic fields (i.e. name, email, phone number, resume, cover letter, location, education)`
1. Select Template: `Global Self-Identification Survey`
1. Send email from: `noreply@gitlab.com` (should default in)

### Scheduling interviews with Greenhouse

The Recruiting Team will schedule most interviews, which is a three-step process.

To schedule an interview, first make sure to move the candidate to the correct stage (Screening, Team Interview, Executive Interview, etc.), then request the candidate's availability by pressing the `Request Availability` button when viewing the stage. It is recommended that you modify the email with the general work hours of the Interviewer for smoother scheduling. You are also able to provide suggested times that the Interviewer is available by quickly pulling up their calendar within Greenhouse and selecting times. If you need to re-request availability (for rescheduling, scheduling a new interview in the same stage, etc.), click where it now says one of "Requested", "Received", or "Confirmation Sent" and choose "Request Availability". The button to request availability will now re-appear.

Once the candidate provides their availability, you will receive an email and can schedule the interview by clicking the "Schedule Interview" button on the candidate's profile next to the appropriate interview stage. Choose the Interviewer and click the "Find Times" button at the top right, and find a free spot on the Interviewer's calendar (making sure it is within their working hours). Note both the candidate's and Interviewer's timezone for easier scheduling. DO NOT change the calendar's timezone from the drop-down view. Greenhouse will show the available times of the interviewee in white on the calendar. If you do not find a suitable time on the first day, you can change days by clicking the arrows near the current day. Note that Greenhouse **does not** currently update timezones once you change days, so you will need to reselect the timezone again even if it looks like it is selected in the timezone bar. When scheduling, be sure to not schedule more than 3 interviews per day for an Interviewer. If you are not able to see their calendar, you can either check the "Interview Calendar" in Google Calendar to view if they have any other interviews that day or reach out to the Interviewer to confirm. The only exception to not scheduling more than three interviews per day is if you are scheduling screening calls for a recruiter.

When sending out the calendar invites to the Interviewers, the Recruiting Team will include a link to a Zoom room for *all* Interviewers under the "location" field, which will follow this format: `https://gitlab.zoom.us/my/gitlab.firstnamelastname` (e.g. Joe Smith's would be: `https://gitlab.zoom.us/my/gitlab.joesmith`). This room will be each Interviewer's consistent location for their interviews, and the naming convention is standard across GitLab for anyone with a Pro Zoom account. All interviews will be conducted via Zoom to maintain a streamlined hiring process. To create a personal Zoom Room for someone, please follow the [instructions in the handbook](/handbook/tools-and-tips/zoom/#making-a-customized-personal-link).

Once a time is selected, you'll need to send the candidate an interview confirmation. You can do so by clicking the "Send Interview Confirmation" button, which will bring up an email template. Note that the timezone defaults to your timezone even on the candidate's side. Therefore, it is recommended to either do the conversion for the candidate to avoid confusion or highlight that it is in your timezone, not necessarily theirs. There are [timezone tools](https://www.worldtimebuddy.com/) you can use for timezone conversions. Be sure to update the template to the correct Zoom room for the Interviewer, and always select the `Include calendar files` button below the email and then click `Send Email`.

Please be sure to include the role for both the Interviewer and the interviewee so that each side is aware of who they will be speaking with. When sending out the calendar invite to the interview, the role the candidate is interviewing for should automatically populate in the title, but be sure to double check. When sending the interview confirmation email to the candidate, be sure to include the title of the person they will be meeting with, as this is not automatic.

At this time, it is not possible to bulk schedule candidates; however, it is helpful to filter the candidates under `Pipeline Tasks` > `To be scheduled` so you can see everyone who needs to be scheduled on the page and easily scroll through them.

Scheduling [executive interviews](/handbook/hiring/recruiting-framework/coordinator/index.html#executive-interview-scheduling) varies slightly for collecting and confirming interview times and typically involves the executive's Executive Assistant.

#### Important scheduling and interview notes

* Please be sure to go to your [Google Calendar settings](https://calendar.google.com/calendar/r/settings?tab=mc) and input your current timezone.
* Make sure that a candidate is in the proper stage to schedule when scheduling.
* If you cannot attend the interview, please let the Recruiting Team know so they can reschedule. **Simply declining the interview does not effectively notify the Recruiting Team.** The Recruiting Team should first cancel the currently scheduled interview through either Greenhouse or Google Calendar (specifically the `Interview Calendar`) before creating the new one to avoid any confusion.
* All outgoing emails and scheduling must be done through Greenhouse. Scheduling details, emails, and communication between team members can be viewed in the activity feed of a candidate.
* Timezones can be tricky with Greenhouse. Always double check timezones before sending confirmations, noting that all interview confirmations and the time shown on the candidate's profile is in your own timezone.
* You can find a candidate's timezone by viewing their application.
* To ensure that someone else (another candidate, for example) does not join your video call without your permission, log into your Zoom account online.
  Click on "My Account" in the top-right corner, and click on "Meeting Settings" in the left sidebar. Scroll down to "In Meeting (Advanced)," and toggle on the option "Waiting Room".
  You will also need to enable Waiting Room for your Personal Meeting ID. Click on "Meetings" in the left sidebar. Select the "Personal Meeting Room" tab and check the "Enable waiting room" option.

### Rejecting candidates

To remove a candidate from the active pipeline, they need to be rejected in Greenhouse, which is typically done only by recruiting.

The candidate should always be notified if they've been rejected. The recruiting team is primarily responsible for declining the candidate, but the hiring manager should be prepared to let the candidate know why they were declined if they had progressed to the team or manager interviews. The hiring manager can also share this feedback with the recruiting team, who will relay it to the candidate.

|Stage|Timing|Medium|Type|
| ------ | ------ | ------ |------ |
|Screening Interview with Recruiter|Within 5 days of Screening Interview|Email OR Verbally during the Screening Interview |Standard Template by default, may provide specific feedback, but not required|
|Assessment|Within 5 days of Asessment Submission|Email |Standard Template by default, may provide specific feedback, but not required|
|Team Interview|Within 5 days of Team Interview|Email or via Zoom Call |Must provide feedback that is personalized and customized.  Only offer frank feedback. This is hard, but it is part of our [company values](/handbook/values/). All feedback should be constructive and said in a positive manner. Keep it short and sweet.  Feedback should always be applicable to the skill set and job requirements of the position the candidate applied and interviewed for.|

Any time a candidate is rejected, the Recruiting Team will notify them, as they will not automatically be notified if we reject them. When clicking the reject button on a candidate, Greenhouse will open a pop up where you can choose the appropriate rejection reason, as well as a rejection email template. Feel free to adjust the template per the [guidelines in the handbook](/handbook/hiring/interviewing/#rejecting-candidates). You can also select a time delay to send out the rejection email. Finally, you are also able to start a new prospect process for a candidate when rejecting them, in the event you want to reach out to them again in the future.

If you want to consider a candidate for the future, the best practice is to archive them with the reason "Future Interest"; however, you can also start a prospect process when rejecting them so they are including in a pool of future interest candidates for the role, or you can simply add a follow-up reminder for yourself, the recruiter, and/or the coordinator to reach out to the candidate in a certain amount of time.

Please ensure that if candidates are rejected or remove themselves from the process that all further scheduled interviews are cancelled either through Greenhouse or through the Google Calendar "Interview Calendar" in order to remove the calendar event from Interviewers' calendars. Rejecting a candidate will not automatically remove the calendar event from an Interviewer's calendar.

You can also reject candidates in bulk by going to the [candidates page](https://app2.greenhouse.io/people), filtering accordingly, clicking `Bulk Actions` at the top right, selecting the appropriate candidates or by clicking "Select All", clicking "Edit Selected", clicking "Reject" in the pop up, and following the same procedure as above to reject and email. Note: you can only choose one reason when archiving in bulk.

To unarchive a candidate, click "Unreject" on the candidate profile under the appropriate role.

### Consideration of previously rejected candidates

Rejected candidates will stay in our talent community and we may revisit them for future roles.

Generally, we will leave a minimum gap of 6-months before reconsidering someone. We will only reconsider candidates who were rejected because they lacked skills or experience. 

We will not reconsider candidates who were mismatched with our [values competencies](https://about.gitlab.com/handbook/competencies/#values-competencies) and/or our [remote work competencies](https://about.gitlab.com/handbook/competencies/#remote-work-competencies). 


#### Single-vetos

We do not operate under a single-veto basis. This means that even if a candidate receives a "No," they still may be moved on in the interview process depending on the feedback submitted by the Interviewer. The Hiring Manager is responsible for reviewing all scorecards and making the decision to move the candidate forward or not. As a good heuristic, two "No" votes will result in the candidate's rejection. A "Definitely not" vote should be carefully looked at, and in most cases this would result in the candidate's rejection.

In situations where scorecards greatly differ, it is helpful to meet with everyone who interviewed the candidate to better understand each Interviewer's experience. The Hiring Manager can then evaluate each experience and make a collective decision.

While Hiring Managers are the directly responsible individual (DRI) for hiring decisions and are allowed to override Interviewers, making great hiring decisions is part of their own individual performance and they will be held accountable for them.

### Changing stages for a candidate

Each role has similar stages, with various interviews within each stage. Below is a typical outline of the stages:

- **Application Review**
  - This stage typically consists only of one event, which is the application review, where all new applicants are added. Hiring Managers and/or Interviewers can leave a scorecard in this stage on if they want to advance or reject a candidate.
- **Assessment** (added per role)
  - This stage is only added to roles that use questionnaires as part of the interview process. The assessment is done completely within Greenhouse, where the questions are added per job, the candidate submits their answers via a link in Greenhouse, and the graders review the answers in Greenhouse and leave a scorecard.
- **Screening**
  - This stage has only the screening call, where the Recruiting Team does an initial call with the candidate to review their experience, skills, and values-alignment.
- **Team Interview**
  - The "Team Interview" usually has several interviews within it, typically with the Hiring Manager, two peers, a director, and/or a panel interview.
- **Executive Interview**
  - This stage consists of two interview events, one for the executive of the division and another for the CEO. Both of these interviews are optional.
- **References**
  - There are two spots for completed reference checks, one from a former peer and one from a former manager.
- **Offer**
  - This is where the Recruiting Team prepares the offer and the offer is approved, and is explored more in detail in the [interviewing section](/handbook/hiring/offers/#offer-package-in-greenhouse) of the handbook.

On rare occasion, there may be additional or less stages than represented here, but these stages should be consistent as much as possible in order to maintain data integrity for reporting. The interviews within the stages can be adjusted as needed, as long as they follow the same names (e.g. there should only be one `Peer Interview 1` across all jobs and not a `Peer Interview 1` on one job and a `Peer Interview One` on another). If there is any doubt or confusion, feel free to reach out to the Recruiting Operations Team.

If a candidate will have more interviews in a stage than predetermined, you can add additional interview events as long as the candidate is in the stage where you need to add the additional event.

Please take caution when changing or deleting stages for existing vacancies. If a stage is deleted in a requisition and candidates are scheduled for interviews on that stage, the interview will be deleted from Greenhouse. It will still exist on the Google Calendar, but it will need to be rescheduled through Greenhouse using one of the current stages for the requisition.

### Updating candidate information

To update a candidate source, go to the candidate's profile and, underneath the name of the job they're being considered for, click the pencil button. Where it says "Source", search for the correct source; some common examples are "Referral", "LinkedIn (Ad Posting)", "LinkedIn (Prospecting)", "Internal Applicant", and "Jobs page on your website". If you need a source that is not listed and you're not able to add a new source, reach out to the Recruiting Team. If you select the referral option or one of the outbound sourcing (also called prospecting) options (such as "LinkedIn (Prospecting)"), be sure to then select "who gets credit" by searching for the referrer or sourcer who found the candidate. Then click "Update Source". If they were referred or are an internal applicant, their profile will now have a highlighted field next to their name indicating this.

To update a candidate's personal details, go to the candidate's profile and click "Edit Profile" on the top right. You'll be able to change the candidate's name, current company, current title, tags, phone numbers, email addresses, social media accounts, websites, address, and education details. You can also go to the "Details" tab of their profile and click "Edit" next to "Info" as these fields are connected. 

To add a candidate's pronouns in a visible location, navigate to the the Headline section under the candidate's name and email on their Greenhouse profile. Click the pencil next to "Add a Headline", enter the pronouns and click save. This allows any interviewer to see the pronouns.

To update a candidate's assigned recruiter and coordinator, go to the candidate's profile and go to the "Details" tab, then scroll down to where it says "Source & Responsibility". Click the pencil icon that will appear when hovering over either the recruiter or coordinator, then update accordingly. (This can also be done in bulk if needed using the bulk actions button on the candidates page.) You can update their source in the "Source & Responsibility" section as well.

To add attachments or documents to a candidate's profile, go to their profile and to the "Details" tab, then scroll to the bottom where it says "All Attachments". Next to the appropriate job they are being considered for, click "Add file" and choose if it is a resume, cover letter, or other document, then click "Choose File" and upload the document from your computer. You can change the visibility of a document by clicking the three dots (...) above the document so that only Super Admins and Recruiting can view it. Please note that cover letters and resumes are by default public to the hiring team for that requisition and cannot be made private.

### Updating and creating email templates

The Recruiting Team and Site Admins are able to add and adjust email templates. To access the email templates, go to the [configure](https://app2.greenhouse.io/configure) section by clicking the gear at the top right corner, then click "[Email Templates](https://app2.greenhouse.io/account/email_templates)". It is possible to create personal templates that only the user can access under "My Templates", but we strongly recommend all templates be added to the "Organization-Wide Templates" section.

To [edit an existing template](https://support.greenhouse.io/hc/en-us/articles/115004020946-How-to-edit-email-templates), click the pencil icon next to the template, make necessary adjustments, and click "Save" at the bottom of the page.

To create a new template, click "+ New" to the right of "Organization-Wide Templates".
- First, add a name for your template that will allow you and your team to recognize what the template is for. Try to keep the name as consistent as possible with other templates (e.g. rejection emails all begin with `Reject` and then the reason why, such as `Reject after no show`).
- Next, you will need to select an email type, which categorizes the emails so that they appear as options when they are relevant (e.g. so that a interview availability request does not populate when you are trying to reject a candidate). Please review the [available email types and when each appears on Greenhouse's support page](https://support.greenhouse.io/hc/en-us/articles/115002573326-Email-Template-Types).
- You can add an optional description, but it is typically not needed due to the descriptive template name.
- You can choose to have the email sent from the sender's email address, the recruiting distribution email address, or the no-reply email address, depending on the nature of the email.
- You can opt to cc specific GitLab team-members and/or the specific recruiter/coordinator for the role that the candidate you are communicating with is in process for.
- Next, create a subject for the email. You can include tokens (elaborated below) in the subject line in order to customize the subject with the candidate's name, requisition, etc.
- You can also include attachments to the email.
- Next, you will see in a blue box the various tokens that are able to be included in your email template. These will vary based on the email type and the available tokens will show up as options when you are creating/editing a template. You can also review the [token glossary](https://support.greenhouse.io/hc/en-us/articles/360007039771-Token-Glossary) to understand what each token means.
- Finally, you will type out the body of your email template, including tokens and formatting as needed.
- Click save, and your template is ready to be used!

Greenhouse has also created a [quick video to walk through email templates](https://support.greenhouse.io/hc/en-us/articles/115002226526-VIDEO-Customize-Create-E-mail-Templates) as a resource.

### Updating requisition and offer approval flows

There are two separate approval flows: one for creating a **requisition** and one for creating an **Offer**. Please note that only `Site Admins` are able to update or add new approvals in Greenhouse.

To configure approval flows, click the `Configure` section (gear icon) in the top right corner, then `Approvals`. There will be two columns; one for **requisition "Job" Approvals** (left) and the other for **Offer Approvals** (right). Each Department that either has a unique sub-deparment in Greenhouse **or** unique Executive should have its own section. For example, *Customer Success* and *Sales* are separate Departments and therefore require separate sections even though they're both under the same Executive. Similarly, *G&A* is a Department that has multiple sub-departments with multiple Executives, so each sub-department under G&A needs to be separated into its own approvals by the corresponding Executive (e.g. Finance for CFO, People Group for CPO, etc.). Approvals are always split out by the Executive.

To note, approval flows must be created **prior** to the creation of a requisition or offer approval or it will **not** be submitted correctly.

To create a new section of approvals:

1. Scroll to the bottom of the page and clicking `Add Approval by office/department`.
1. Then choose what Department this section will be for (e.g. *Sales* for a Department that has only one Executive or *Finance* for a sub-department that has multiple executives).
1. Do **not** select an *Office* and click `Create`.
1. Click `Add Approval Step` under the jobs approval column. Then click `Add` and search for appropriate team member for Step 1 ([as outlined below](/handbook/hiring/greenhouse#approval-flows)) and select their name. If there should be more than one team member, click `Add` again and search for and select their name. If you choose more than one person, you'll notice above their names is a dropdown that says *"1 of 2 required"* - keep this as is. If for any reason we do need both approvals, you can change it to have both be required. Click `Save`.
1. Click `Add Approval Step`, then click `Add` and search for appropriate team member(s) for Step 2 ([as outlined below](/handbook/hiring/greenhouse#approval-flows)) and select their name(s). Click `Save`.
1. Click `Add Approval Step`, then click `Add` and search for appropriate team member(s) for Step 3 ([as outlined below](/handbook/hiring/greenhouse#approval-flows)) and select their name(s). Click `Save`.
1. Repeat steps 4-6 above for the offer approvals column.
1. You are ready to start creating and approving vacancies and offers!

To change an existing section of approvals:

1. Find the appropriate section, then hover over what you want to change.
- If you want to change the participants in an approval step, click the `Pencil` icon, so that you can remove and/or add team members.
     - Hover over the team member's name and click the `"X"` to remove them.
- If you want to delete an approval step in its **entirety**, click the `"X"`.
- If you want to change the order of an approval flow, click and drag that step to the appropriate placing.

##### Approval flows

For **requisition "Job" Approvals**, the approval chain is as follows:

- Step 1: People Ops Analyst (1 of 2 required)
- Step 2: Finance Business Partner of the Division/Department (1 of 2 required)
- Step 3: Executive of Division/Department (1 required if multiple people are listed)

For the **Official Job Approval** (a.k.a. *2-Stage Job Approvals*), please list the Finance Business Parnters again (1 of 2 required).

For **Offer Approvals**, the approval chain is as follows:

- Step 1: People Ops Analyst (1 of 2 required)
- Step 2: Finance Business Partner of the Division/Department (1 of 2 required)
- Step 3: Executive of Division/Department (1 required if multiple people are listed)

For **Director-level roles and above**, please add a **fourth step** and list the CEO and CPO (1 of 2 required).

Delays at the **Offer Approval** stage can negatively impact our [time to hire](/handbook/hiring/metrics/#time-to-offer-accept-days) metric. To keep delays to a minimum, we ask for approvals to be completed within 24 hours of reaching the reviewer. 

*Please note* that the approval order should always be `In Order` and **not** `All at once` (located at the top of each section).

### Updating or adding new departments

Occasionally, the Finance team updates our organization's divisions and departments. We try to keep Greenhouse as aligned as possible to Finance's structure so that we can maintain accurate alignment and headcount planning.

Please note that only Site Admins are able to update or add new departments in Greenhouse. In order to update or add divisions or departments, log in to Greenhouse and go to the [configure](https://app2.greenhouse.io/configure) section by clicking the gear at the top right corner. Then click ["Organization"](https://app2.greenhouse.io/account/organization) and scroll down the "Departments" section. You will see each current division listed and, when clicking on the expand button, each department that falls under that division.

To change the name of a division or department, click the pencil button next to it. Please note you are unable to have a division and department with the same name. To add a new division or department, click "New Department" and type in the name; if it is a new division, simply click create, but if it is a new department click the "Subordinate to" dropdown and choose the division it is associated to (for example, the "Security" department is a subordinate to the "Engineering" division).

There are currently two exceptions to the official divisions and departments, in order to keep things organized and clear for both applicants and reporting purposes. Firstly, "Customer Success" is its own division with subordinate departments instead of falling under "Sales". Secondly, "Engineering Management" is a department under "Engineering" instead of being portioned out to various teams.

## Additional resources

### Training

Internal GitLab trainings were done on Greenhouse, for basic users, Interviewers, and Hiring Managers. These trainings can be found in the [GitLab Videos folder](https://drive.google.com/drive/u/1/folders/1IK3Wb3P9_u0akMx5ASG26cuehY_LeRe8) on the Google Drive and are only accessible to GitLab team-members, as there is confidential information contained within the videos.

Greenhouse has training material specifically for new [Recruiters](https://support.greenhouse.io/hc/en-us/articles/360016249232-Training-Your-Team-Recruiter) and [Coordinators](https://support.greenhouse.io/hc/en-us/articles/360016249292-Training-Your-Team-Coordinator) that cover what each team member needs to know while working with the system.

### Greenhouse CRM

The Greenhouse CRM is the entry point for prospective candidates (a.k.a. *Prospects*). There, they have the option to join our **Talent Community**. Below is an overview of system resources and its general workflow.

#### CRM Resources

* [CRM Overview](https://support.greenhouse.io/hc/en-us/articles/360022793612-CRM-Overview)
* [CRM License Functionality](https://support.greenhouse.io/hc/en-us/articles/360014882832-Greenhouse-CRM-License-Functionality-Grid)
* [Leveraging Greenhouse CRM (Webinar)](https://support.greenhouse.io/hc/en-us/articles/360027741072)
* [Greenhouse Prospecting Chrome Plug-In](https://chrome.google.com/webstore/detail/greenhouse-prospecting/plmlekabedgfocpkpncacfblnjkgonpk?hl=en-US)

#### High-Level Workflow

* When a *Prospect* signs-up for the **Talent Community**, their default source will be `Talent Community`.
* **Prospect Pool Stages** are as follows:
    * No Stage Specified
    * Not Contacted
    * Nurture
    * In Discussion
    * Not Interested
* `Tags` can be used to keep track of *Prospects* just as they are with *Candidates*.
    * If additional `tags` are needed, please contact [Recruiting Ops])(https://gitlab.com/gl-recruiting/operations/-/issues/new).
* All *Prospect* communications are tracked in Greenhouse just as they are with *Candidates*.    
* When a Team Member identifies a *Prospect* for a requisition, they'll need to click the `Convert to Candidate` button on the *Prospect's* profile in order to move them to the requisition's active candidate pool.
    * Doing this will **remove** the *Prospect* from the **Prospect Pool**.
* To claim credit for a CRM-sourced *Candidate*, the Team Member will need to **manually** update the *Candidate's* source and add their name in the `Who Gets Credit` field.
    * `Details` > `Source & Responsibility` > Source (`Pencil` icon) > `Who Gets Credit` > `Update Source`
* If there are duplicate *Prospect* profiles, they can be merged by an `Admin`. Please tag @Recruiting Operations and Insights within the Greenhouse candidate profile.  

    * `Name` and `Email` are the two fields Greenhouse looks at when suggesting a potential duplicate profile.  

### Greenhouse Forms

Greenhouse offers the ability to create and send **Forms** to candidates to gather additional information. 

* **Creating a Form**

Forms are created on a *per requisition basis*. Fortunately, `Admins` can apply it to requisitions en masse via a bulk update. More information on how to create and set-up forms can be found on Greenhouse's [Create a Form](https://support.greenhouse.io/hc/en-us/articles/360019754651-Create-a-Form) help page.

* **Reviewing Form Responses**

When an emailed form is completed by a candidate, the designated recipients will receive an email notifying them of its completion. More information about that workflow can be found on Greenhouse's [Review Candidate Response to Form](https://support.greenhouse.io/hc/en-us/articles/360019755771-Send-Forms#response) help page.

### Greenhouse Inclusion

We use Greenhouse Inclusion to mitigate unconscious bias, ensure consistent candidate evaluations, and to measure the impact of these practices in our [Diversity, Inclusion & Belonging ](/company/culture/inclusion/) efforts.

Enabled features include:
- Nudges, or reminders, to all users when creating a requisition description, adding a referral, and/or filling out a scorecard.
- Anonymous take-home test grading.
- Hiding a candidate's source to Interviewers.
- Hiding specific sections of a candidate's resume in various workflow stages.

### Greenhouse Support

Greenhouse has a very robust [Support Center](https://www.greenhouse.io/support), with articles, how-to videos, webinars, and more.

If you have a **Product Enhancement Request** or need to submit a **Support Ticket**, please reach out to the [Recruiting Ops Team](mailto:recruitingops@gitlab.com). Alternatively, you have three options to reach Greenhouse's Support Team directly

1. By emailing `support@greenhouse.io`
1. By using the *Live Chat* feature from anywhere within Greenhouse (bottom left corner)
1. By submitting a request on [Greenhouse Support's Website](https://support.greenhouse.io/hc/en-us/requests/new).

To note, when contacting Greenhouse's Support Team directly, please cc [Erich Wegscheider](mailto:ewegscheider@gitlab.com) on **all** tickets.

=======

[^fn1]:[Here is Why Employee Referrals are the Best Way to Hire](https://business.linkedin.com/talent-solutions/blog/2015/08/5-reasons-employee-referrals-are-the-best-way-to-hire)
[^fn2]:[Why Employee Referrals are the Best Source of Hire](https://theundercoverrecruiter.com/infographic-employee-referrals-hire/)
[^fn3]:[5 Reasons Employee Referrals are the Best Source of Hire](https://hr.sparkhire.com/best-hiring-practices/5-reasons-employee-referrals-are-the-best-source-of-hire/)
[^fn4]:[4 Reasons Employee Referrals are Crucial to Your TA Strategy](https://www.smartrecruiters.com/blog/4-reasons-employee-referrals-are-important-to-your-ta-strategy/)
