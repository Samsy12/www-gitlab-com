---
layout: handbook-page-toc
title: " Engineering Team Hiring Factsheets"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

### Purpose

The Purpose of these Hiring team factsheets is to allow you to be more informed with regards to the teams you are applying too or interviewing for. The factsheet will supply you with information regarding various aspects of the team and important links that will help you understand the team a bit more. 

### General Questions & Information

TBC 

### Software Development Teams 

## Monitor 

# Frontend Engineering

**Team Size:** 5
**Projected Team Size:** 7
**Leadership Team:**
Sam Goldstein - Director of Engineering, Ops
Clement Ho - Frontend Engineering Manager, Monitor 
Adriel Santiago - Interim Frontend Engineering Manager, Monitor
**Product Vision page:** https://about.gitlab.com/direction/product-vision/#monitor
**How the team works:** The team cross collaborates with the product team and other engineering departments to build out features that drive their respective product stages to maturity and beyond. 
**Links to relevant Handbook pages:** 
[Product categories page](/handbook/product/categories/#ops-section)
[Frontend Monitor Team Page](/handbook/engineering/frontend/monitor/)
[Frontend Engineer job posting](https://boards.greenhouse.io/gitlab/jobs/4065693002)
[Frontend Engineer job family](/job-families/engineering/frontend-engineer/)
[Engineering Workflow](/handbook/engineering/workflow/)
[Hiring Process](https://gitlab.com/gitlab-com/people-ops/hiring-processes/tree/master/Engineering/Frontend)
[Technical Assessment](https://gitlab.com/gitlab-com/people-ops/hiring-processes/blob/master/Engineering/Frontend/1-Assessment.md)

# Backend Engineering

**Team Size:** 6
**Projected Team Size:** 11
**Leadership Team:**
Sam Goldstein (Director of Engineering, Ops)
[Product Vision page](/direction/product-vision/#monitor)
**How the team works:** Performance is a critical aspect of the user experience, and ensuring your application is responsive and available is everyone's responsibility. We want to help address this need for development teams, by integrating key performance analytics and feedback into the tool developers already use every day.
**Links to relevant Handbook pages:** 
[Product categories page](/handbook/product/categories/#ops-section)
[Monitor Team page](/handbook/engineering/development/ops/monitor/)
[Backend Engineer job family](/job-families/engineering/backend-engineer/)
[Engineering Workflow](/handbook/engineering/workflow/)
[Backend Engineer job posting](/jobs/apply/backend-engineer-x-30-4055697002/)
 [Hiring process](https://gitlab.com/gitlab-com/people-ops/hiring-processes/tree/master/Engineering/Backend)

## Configure

# Frontend Engineering

**Team Size:** 3
**Projected Team Size:** 5
**Leadership Team:**
Sam Goldstein - Director of Engineering, Ops
Clement Ho - Frontend Engineering Manager, Monitor (Interim for Configure/Serverless)
[Product vision page](/direction/product-vision/#configure)
How the team works: The team cross collaborates with the product team and other engineering departments to build out features that drive their respective product stages to maturity and beyond.
**Links to relevant Handbook pages:**
[Product categories page](/handbook/product/categories/#ops-section)
[Configure Team page](/handbook/engineering/development/ops/configure/)
[Frontend Engineer job posting](https://boards.greenhouse.io/gitlab/jobs/4065693002)
[Frontend Engineer job family](/job-families/engineering/frontend-engineer/)
[Engineering Workflow](/handbook/engineering/workflow/)
[Hiring Process](https://gitlab.com/gitlab-com/people-ops/hiring-processes/tree/master/Engineering/Frontend)
[Technical Assessment](https://gitlab.com/gitlab-com/people-ops/hiring-processes/blob/master/Engineering/Frontend/1-Assessment.md)

# Backend Engineering

**Team Size:** 5
**Projected Team Size:** N/A
**Leadership Team:**
Sam Goldstein - (Director of Engineering, Ops)
[Product Vision page](/direction/configure/)
**How the team works:** The Configure stage deals with the configuration and operation of applications and infrastructure, including Auto DevOps, Kubernetes integration, and ChatOps. We aim to make complex tasks (such as standing up new environments) fast and easy as well as providing operators all the necessary tools to execute their day-to-day actions upon their infrastructure.
**Links to relevant Handbook pages:** 
Product categories page: /handbook/product/categories/#ops-section
[Configure Team Page](/handbook/engineering/development/ops/configure/)
[Backend Engineer job family](/job-families/engineering/backend-engineer/)
[Engineering Workflow](/handbook/engineering/workflow/)
[Backend Engineer job posting](/jobs/apply/backend-engineer-x-30-4055697002/)
 [Hiring process](https://gitlab.com/gitlab-com/people-ops/hiring-processes/tree/master/Engineering/Backend)

## Verify

# Frontend Engineering

**Team Size:** 3
**Projected Team Size:** 5
**Leadership Team:**
Darby Frey (Senior Engineering Manager, Verify)
Daniel Cipoleetti (Frontend Engineering Manager, Verify)
[Product vision page](/direction/product-vision/#verify)
**How the team works:** The Verify stage is all about making sure that your code does what you expected it to do, meets quality standards, and is secure; all via automated testing. We bring best practices from top development teams, and make them the easy, default way to work.
**Links to relevant Handbook pages:**
[Product Categories page](/handbook/product/categories/#verify-stage)
[Frontend Verify Team page](/handbook/engineering/frontend/verify-release/)
[Frontend Engineer job posting](https://boards.greenhouse.io/gitlab/jobs/4065693002)
[Frontend Engineer job family](/job-families/engineering/frontend-engineer/)
[Engineering Workflow](/handbook/engineering/workflow/)
[Hiring Process](https://gitlab.com/gitlab-com/people-ops/hiring-processes/tree/master/Engineering/Frontend)
[Technical Assessment](https://gitlab.com/gitlab-com/people-ops/hiring-processes/blob/master/Engineering/Frontend/1-Assessment.md)


# Backend Engineering

**Team Size:** 5
**Projected Team Size:** 8
**Leadership Team:**
Darby Frey (Senior Engineering Manager, Verify)
Elliot Rushton (Engineering Manager, Verify)
[Product Vision page](/direction/verify/)
**How the team works:** The Verify stage of the DevOps pipeline covers the CI lifecycle as well as testing (unit, integration, acceptance, performance, etc.). Our mission is to help developers feel confident in delivering their code to production. Asynchronous communication, working with Agile methodologies, incorporating a more continuous delivery model, all of the collaboration is done through GitLab Issues and Slack. 
**Links to relevant Handbook pages:** 
[Product categories page](/handbook/product/categories/#ops-section)
[Verify team page](/handbook/engineering/development/ops/verify/)
[Backend Engineer job family](/job-families/engineering/backend-engineer/)
[Engineering Workflow](/handbook/engineering/workflow/)
[Backend Engineer job posting](/jobs/apply/backend-engineer-x-30-4055697002/)
[Hiring process](https://gitlab.com/gitlab-com/people-ops/hiring-processes/tree/master/Engineering/Backend)


## Release 

# Frontend Engineering

**Team Size:** 3
**Projected Team Size:** 5
**Leadership Team:**
Darby Frey (Senior Engineering Manager, Verify)
Nicole Williams (Frontend Engineering Manager, Release)
[Product vision page](/direction/product-vision/#release)
**How the team works:** A key part of CD is being able to deploy. Continuous Delivery and Deployment consists of a step further CI, deploying your application to production at every push to the default branch of the repository.
**Links to relevant Handbook pages:**
[Product Categories page](/handbook/product/categories/#cicd-section)
[Frontend Release Team page](/handbook/engineering/frontend/verify-release/)
[Frontend Engineer job posting](https://boards.greenhouse.io/gitlab/jobs/4065693002)
[Frontend Engineer job family](/job-families/engineering/frontend-engineer/)
[Engineering Workflow](/handbook/engineering/workflow/)
[Hiring Process](https://gitlab.com/gitlab-com/people-ops/hiring-processes/tree/master/Engineering/Frontend)
[Technical Assessment](https://gitlab.com/gitlab-com/people-ops/hiring-processes/blob/master/Engineering/Frontend/1-Assessment.md)

# Backend Engineering

**Team Size:** 5
**Projected Team Size:** 10
**Leadership Team:**
Darby Frey (Senior Engineering Manager, Verify)
Chase Southard (Engineering Manager, Release:Progressive Delivery)
John Hampton (Engineering Manager, Release:Release Management)
[Product Vision page](/direction/release/)
**How the team works:** The Release stage comes after your build and test pipeline is complete and ensures that your software makes it through your review apps, pre-production environments, and ultimately to your users with zero touch needed. It is part of the CI/CD department. Asynchronous communication, working with Agile methodologies, incorporating a more continuous delivery model, all of the collaboration is done through GitLab Issues and Slack. 
**Links to relevant Handbook pages:**
[Product categories page](/handbook/product/categories/#ops-section)
[Release Team page](/stages-devops-lifecycle/release/)
[Backend Engineer job family](/job-families/engineering/backend-engineer/)
[Engineering Workflow](/handbook/engineering/workflow/)
[Backend Engineer job posting](/jobs/apply/backend-engineer-x-30-4055697002/)
[Hiring process](https://gitlab.com/gitlab-com/people-ops/hiring-processes/tree/master/Engineering/Backend)


## Package

# Frontend Engineering

**Team Size:** 1
**Projected Team Size:** 3
**Leadership Team:**
Darby Frey (Senior Engineering Manager, Verify)
Daniel Croft  (Engineering Manager, Package)
[Product vision page](/direction/product-vision/#package)
**How the team works:** A key part of CI is being able to deploy. Continuous Integration works by pushing small code chunks to your application’s code base hosted in a Git repository, and, to every push, run a pipeline of scripts to build, test, and validate the code changes before merging them into the main branch.
**Links to relevant Handbook pages:**
[Product Categories page](/handbook/product/categories/#cicd-section)
[Frontend Engineer job posting](https://boards.greenhouse.io/gitlab/jobs/4065693002)
[Frontend Engineer job family](/job-families/engineering/frontend-engineer/)
[Engineering Workflow](/handbook/engineering/workflow/)
[Hiring Process](https://gitlab.com/gitlab-com/people-ops/hiring-processes/tree/master/Engineering/Frontend)
[Technical Assessment](https://gitlab.com/gitlab-com/people-ops/hiring-processes/blob/master/Engineering/Frontend/1-Assessment.md)


# Backend Engineering

**Team Size:** 5
**Projected Team Size:** 5
**Leadership Team:**
Darby Frey (Senior Engineering Manager, Verify)
Daniel Croft  (Engineering Manager, Package)
**Product Vision page:** https://about.gitlab.com/direction/package/
**How the team works:** The Package stage allows you to publish, consume and discover packages, all in one place. We believe that source code and dependencies should live in one secure environment, protected by your GitLab credentials. The efficiencies gained by centralization allow you to focus on productivity. By integrating with GitLab's CI/CD product, we can ensure efficient and reliable pipelines. Asynchronous communication, working in Kanban, all of the collaboration is done through GitLab Issues and Slack, always iterating on the process. 
**Links to relevant Handbook pages:** 
[Product categories page](/handbook/product/categories/#ops-section)
[Package Team page](/handbook/engineering/development/ops/package/)
[Backend Engineer job family](/job-families/engineering/backend-engineer/)
[Engineering Workflow](/handbook/engineering/workflow/)
[Backend Engineer job posting](/jobs/apply/backend-engineer-x-30-4055697002/)
[Hiring process](https://gitlab.com/gitlab-com/people-ops/hiring-processes/tree/master/Engineering/Backend)



## Secure

# Frontend Engineering

# Backend Engineering
**Team Size:** 12
**Projected Team Size:** 20
**Leadership Team:**
Todd Stadelhofer (Director of Engineering, Secure)
Thomas Woodham  (Engineering Manager, Secure) - 4 People Team
Olivier Gonzalez (Engineering Manager, Secure) - 5 People Team

[Product Vision page](/direction/secure/)

**How the team works:** 

**Links to relevant Handbook pages:** 
[SAST Project](https://gitlab.com/gitlab-org/security-products/sast)
[Analyzers](https://gitlab.com/gitlab-org/security-products/analyzers)
[DAST Project](https://gitlab.com/gitlab-org/security-products/dast)
[Dependency scanning Project](https://gitlab.com/gitlab-org/security-products/dependency-scanning)
[Gemnasium-db Project](https://gitlab.com/gitlab-org/security-products/gemnasium-db)
**Analyzers**
- [bundler-audit](https://gitlab.com/gitlab-org/security-products/analyzers/bundler-audit)
- [gemnasium](https://gitlab.com/gitlab-org/security-products/analyzers/gemnasium)
- [gemnasium-maven](https://gitlab.com/gitlab-org/security-products/analyzers/gemnasium-maven)
- [gemnasium-python](https://gitlab.com/gitlab-org/security-products/analyzers/gemnasium-python)
- [retire.js](https://gitlab.com/gitlab-org/security-products/analyzers/retire.js)
[License Management Project](https://gitlab.com/gitlab-org/security-products/license-management)
[Container Scanning Project](https://gitlab.com/gitlab-org/gitlab-ee/blob/master/lib/gitlab/ci/templates/Security/Container-Scanning.gitlab-ci.yml) no project yet, just a job definition

## Fulfillment 

# Frontend Engineering 
**Team Size:** 2
**Projected Team Size:** 5
**Leadership Team:**
Bartek Marnane (Director of Engineering, Growth)
Dennis Tang (Frontend Engineering Manager, Manage and Fulfilment)
[Product vision page](/direction/fulfillment/)
**How the team works:** The team cross collaborates with the product team and other engineering departments to build out features that drive their respective product stages to maturity and beyond. 
**Links to relevant Handbook pages:**
[Product Categories page](/handbook/product/categories/#growth-section)
[Frontend Engineer job posting](https://boards.greenhouse.io/gitlab/jobs/4065693002)
[Frontend Engineer job family](/job-families/engineering/frontend-engineer/)
[Engineering Workflow](/handbook/engineering/workflow/)
[Hiring Process](https://gitlab.com/gitlab-com/people-ops/hiring-processes/tree/master/Engineering/Frontend)
[Technical Assessment](https://gitlab.com/gitlab-com/people-ops/hiring-processes/blob/master/Engineering/Frontend/1-Assessment.md)

# Backend Engineering
**Team Size:** 4
**Projected Team Size:** 6
**Leadership Team:**
Bartek Marnane (Director of Engineering, Growth)
James Lopez - Backend Engineering Manager, Fulfillment:Provision

[Product Vision page](/direction/fulfillment/)

**How the team works:** 

**Links to relevant Handbook pages:** 
[Fulfillment Team Page](/handbook/engineering/development/growth/fulfillment/#welcome-to-the-fulfillment-group-page)
[Backend Engineer job family](/job-families/engineering/backend-engineer/)
[Engineering Workflow](/handbook/engineering/workflow/)
[Backend Engineer job posting](/jobs/apply/backend-engineer-x-30-4055697002/)
[Hiring process](https://gitlab.com/gitlab-com/people-ops/hiring-processes/tree/master/Engineering/Backend)

## Growth

# Fullstack Engineering Team 

**Team Size:** 3
**Projected Team Size:** 7
**Leadership Team:**
Bartek Marnane (Director of Engineering, Growth)

[Product vision page](/handbook/product/categories/#growth-section)

**How the team works:** The team cross collaborates with the product team and other engineering departments to build out features that drive their respective product stages to maturity and beyond.
**Links to relevant Handbook pages:**
[Product categories page](/handbook/product/categories/#growth-section)
[Growth Team page](/handbook/engineering/development/growth/)
[Growth Product Analytics](/handbook/engineering/development/growth/product-analytics/)
[Fullstack Engineer job posting](https://boards.greenhouse.io/gitlab/jobs/4247953002)
[Fullstack Engineer job family](/job-families/engineering/fullstack-engineer/)
[Engineering Workflow](/handbook/engineering/workflow/)
[Hiring Process](https://gitlab.com/gitlab-com/people-ops/hiring-processes/tree/master/Engineering/Frontend)

## Memory 

**Team Size:** 5
**Projected Team Size:** 8
**Hiring Managers:**
Chun Du (Director of Engineering, Enablement)
Craig Gomes	(Engineering Manager, Memory)
[Product Vision page] https://about.gitlab.com/direction/create/ecosystem/
[How the team works] https://about.gitlab.com/direction/create/ecosystem/#overview
**Links to relevant Handbook pages:** 
[Product categories page](/handbook/product/categories/)
[Memory team page](/handbook/engineering/development/enablement/memory/#team-members)
[Backend Engineer job family](/job-families/engineering/backend-engineer/)
[Engineering Workflow](/handbook/engineering/workflow/)
[Backend Engineer job posting](/jobs/apply/backend-engineer-x-30-4055697002/)
[Hiring process](https://gitlab.com/gitlab-com/people-ops/hiring-processes/tree/master/Engineering/Backend)

## Product Analytics

**Team Size:** 1
**Projected Team Size:** 4
**Hiring Managers:**
Jerome Ng (Engineering Manager, Growth:Conversion and Product Analytics)
Bartek Marnane (Director of Engineering, Growth)
[Product Vision page](/direction/product-intelligence/)
**How the team works** The overall vision for the Product Analytics group is to ensure that we have a robust, consistent and modern product analytics data framework in place to best serve our internal Product, Sales, and Customer Success teams. The group also maintains the Version Check application as described above and also ensures that GitLab's Product team have the right analysis tool in place that allows the best possible insights into the data provided through the Collection tools.
**Links to relevant Handbook pages:** 
[Product categories page](/handbook/product/categories/#ops-section)
[Product Analytics Team Page](/handbook/engineering/development/growth/conversion-product-analytics)
[Backend Engineer job family](/job-families/engineering/backend-engineer/)
[Engineering Workflow](/handbook/engineering/workflow/)
[Backend Engineer job posting](/jobs/apply/backend-engineer-x-30-4055697002/)
[Hiring process](https://gitlab.com/gitlab-com/people-ops/hiring-processes/tree/master/Engineering/Backend)

## Ecosystem 

**Team Size:** 1
**Projected Team Size:** 5
**Hiring Managers:**
Chun Du (Director of Engineering, Enablement)
[Product Vision page](handbook/product/categories/#ecosystem-group)
[How the team works](/handbook/engineering/development/enablement/ecosystem/#mission)
**Links to relevant Handbook pages:** 
[Product categories page](/handbook/product/categories/#ecosystem-group)
[Ecosystem Team page](/handbook/engineering/development/enablement/ecosystem/#team-members)
[Backend Engineer job family](/job-families/engineering/backend-engineer/)
[Engineering Workflow](/handbook/engineering/workflow/)
[Backend Engineer job posting](/jobs/apply/backend-engineer-x-30-4055697002/)
[Hiring process](https://gitlab.com/gitlab-com/people-ops/hiring-processes/tree/master/Engineering/Backend)






## Protect

# Frontend Engineering

TBC

# Backend Engineering  

TBC 

### Non Software Development Teams

## Support Engineering

**Team Size:** 55 (as of Jun. 2019)
**Projected Team Size:** 85 (by the end of Jan. 2020)
**Leadership Team:**
Tom Cooney (Director)
Lee Matos (AMER East)
Jason Colyer (AMER Central)
Lyle Kozloff (AMER West)
Tom Atkins (EMEA)
Shaun McCann (APAC)
How the team works: Support Engineers and Agents work with GitLabCustomers.  They focus on getting bugs and feature proposals triaged and routed to the appropriate team. Support Engineers and Agents are often on the vanguard of new and interesting ways GitLab is used, deployed, and managed. Support Engineers and Agents sit at the intersection of Systems Administration, DevOps, and Customer Service.
Links to relevant handbook pages:
[Support Handbook Page](/handbook/support/)
[Support Engineer Job Family](/job-families/engineering/support-engineer/#hiring-process)
[Support Agent Job Family](/job-families/engineering/dotcom-support/)


## Security

**Leadership Team**
Senior Director: Kathy Wang
Team Size: 32 (as of Jun. 2019)
Projected Team Size: 55 (by the end of Jan. 2020)


# Application Security 

**Team Size:** 6 (as of Jun. 2019)
Projected Team Size: 10 (by the end of Jan. 2020)
**Leadership Team:**
James Ritchey
Ethan Strike
**How the team works:** Application Security specialists work closely with development, product security PMs and third-party groups (including paid bug bounty programs) to ensure pre and post-deployment assessments are completed. 
**Links to relevant handbook pages:**
[Security Handbook](/handbook/engineering/security/#application-security)
[Security Engineer Job Family](/job-families/engineering/security-engineer/)


# Red Team

**Team Size:** 2 (as of Jun. 2019)
**Projected Team Size:** 4 (by the end of Jan. 2020)
**Leadership Team:**
Jan Urbanc (interim until July 8, 2019)
Steve Manzuik (starting July 8, 2019)
**How the team works:** Red Team specialists emulate adversary activity to better GitLab’s enterprise and product security. The role requires the ability to think like an advanced persistent threat. Creativity is key. For example, develop attack plans and stealthily execute them to compromise sensitive information on GitLab.com such as private repos, or develop and distribute malware to GitLab team-members to demonstrate how the corporate enterprise could be compromised.
**Links to relevant handbook pages:**
[Security Handbook](/handbook/engineering/security/#red-team)
[Security Engineer Job Family](/job-families/engineering/security-engineer/)


# Field Security 

**Team Size:** 1 (as of Jun. 2019)
**Projected Team Size:** 3 (by the end of Jan. 2020)
**Leadership Team:**
Robert Mitchell
**How the team works:** The Field Security team is the public representation of GitLab's internal Security Team and function. They are responsible for evangelizing GitLab's Security practices both internally and externally, as well as providing expert guidance to internal and external customers about the best practices to secure GitLab assets. As a member of the Field Security Team at GitLab, you will be working towards raising the bar on security. We will achieve that by working and collaborating with cross-functional teams and global customers to provide guidance on security best practices.
**Links to relevant handbook pages:**
[Security Handbook](/handbook/engineering/security/)
[Security Engineer Job Family](/job-families/engineering/security-engineer/)
[Field Security Analyst Description](/jobs/apply/senior-security-analyst-field-security-4305883002/)


# Vunerability research

**Team Size:** 0 (as of Jun. 2019)
**Projected Team Size:** 4 (by the end of Jan. 2020)
**Leadership Team:**
TBD - Kathy Wang interim until Manager is hired
**How the team works:** Vulnerability Research Engineers are responsible for performing deep assessments of software and web application vulnerabilities, tracking exploit code releases and exploitation activities, and the creation of detailed and actionable reports in support of our global commercial and government customers.
**Links to relevant handbook pages:**
[Security Handbook:](/handbook/engineering/security/)
[Security Engineer Job Family:](/job-families/engineering/security-engineer/)
[Vulnerability Research Engineer Description:](/job-families/engineering/vulnerability-research-engineer/)


# Security research

**Team Size:** 2 (as of Jun. 2019)
**Projected Team Size:** 3 (by the end of Jan. 2020)
**Leadership Team:**
Kathy Wang 
**How the team works:** Security research specialists conduct internal testing against GitLab assets, and against FOSS that is critical to GitLab products and operations. 
**Links to relevant handbook pages:**
[Security Handbook](/handbook/engineering/security/)
[Security Engineer Job Family](/job-families/engineering/security-engineer/)
[Security Research Engineer Description](/jobs/apply/senior-security-engineer-security-research-4187225002/)

# Security Operations

**Team Size:** 13 (as of Jun. 2019)
**Projected Team Size:** 15 (by the end of Jan. 2020)
**Leadership Team:**
Paul Harrison
Jan Urbanc
**How the team works:** Security Operations specialists respond to incidents. This is often a fast-paced and stressful environment, where responding quickly and maintaining one's composure is critical. 
**Links to relevant handbook pages:**
[Security Handbook](/handbook/engineering/security/)
[Security Engineer Job Family](/job-families/engineering/security-engineer/)
[Security Operations Description](/jobs/apply/senior-security-engineer-security-operations-4055719002/)


# Compliance 

**Team Size:** 7 (as of Jun. 2019)
**Projected Team Size:** 10 (by the end of Jan. 2020)
**Leadership Team**
Jeff Burrows
**How the team works:** Compliance specialists enables Sales by achieving standard as required by our customers. This includes SaaS, on-prem, and open source instances.
**Links to relevant handbook pages:**
[Security Handbook](/handbook/engineering/security/)
[Security Engineer Job Family](/job-families/engineering/security-engineer/)
[Compliance Analyst Description](/jobs/apply/senior-security-analyst-compliance-4055714002/)


## Quality 

Team Size: 15 (as of Jun. 2019)
Projected Team Size: 24 (by the end of Jan. 2020)
Hiring Managers:
Mek Stritti, Director of Quality Engineering
Ramya Authappan, Quality Engineering Manager, Dev
Tanya Pazitny Quality Engineering Manager, Secure & Enablement
Useful links:
[Quality Engineering handbook](/handbook/engineering/quality/)
[Test orchestration tool](https://gitlab.com/gitlab-org/gitlab-qa) 
[Dev Quality team overview](/handbook/engineering/quality/dev-qe-team/)
[Ops Quality team overview](/handbook/engineering/quality/ops-qe-team/) 
[Security and Ops Quality team overview](/handbook/engineering/quality/secure-enablement-qe-team/)
[Engineering Productivity team overview](/handbook/engineering/quality/engineering-productivity-team/)


**Team Vision:** Ensure that GitLab consistently releases high-quality software across the growing suite of products, developing software at scale without sacrificing quality, stability or velocity. The quality of the product is our collective responsibility. The quality department makes sure everyone is aware of what the quality of the product is, empirically.


## UX 

**Team Size:** 24 in UX, (as of Jun. 2019)
**Projected Team Size:** 37 in UX (by the end of Jan. 2020)
**Leadership Team:**
Christie Lenneville, UX Director
Valerie Karnes, UX Manager CI/CD
Sarah O’Donnell, UX Manager Research
Jacki Bauer, UX Manager Enablement and Growth

**Useful links:**
[UX Handbook}(/handbook/engineering/ux/)
[Pajamas Design System](https://design.gitlab.com/)
[Product Designer job family](/job-families/engineering/product-designer/)
[UX Researcher job family](/job-families/engineering/ux-researcher/)
[UX Management job family](/job-families/engineering/ux-management/)
[UX Research issue tracker](https://gitlab.com/gitlab-org/ux-research/issues)
[Participant program](/community/gitlab-first-look/)

**Team vision:** Our principles are that GitLab should be [productive](/handbook/engineering/ux/#productive), [minimal](/handbook/engineering/ux/#minimal), and [human](/handbook/engineering/ux/#human). We want to [design](https://library.gv.com/everyone-is-a-designer-get-over-it-501cc9a2f434) the most complete, uncomplicated, and adaptable DevOps tool on the market, enabling our users to spend more time adding business value in their own jobs. 


## Technical Writing

**Team Size:** 7 (as of Jun. 2019)
**Projected Team Size:** 12 (by the end of Jan. 2020)
**Leadership Team:**
Mike Lewis, Technical Writing Manager
**Useful links:**
[Docs site](https://docs.gitlab.com/)
[Technical Writing handbook](/handbook/engineering/ux/technical-writing/)
[Technical Writer job family](/job-families/engineering/technical-writer/)
[Technical Writing Manager job family](/job-families/engineering/technical-writing-manager/)
[Technical Writing workflow](/handbook/engineering/ux/technical-writing/workflow/)

Team vision: The primary goal of the Technical Writing team is to continuously develop GitLab's product documentation content to meet the evolving needs of all users and administrators.
Documentation is intended to educate readers about features and best practices, and to enable them to efficiently configure, use, and troubleshoot GitLab. To this end, the team also manages the docs.gitlab.com site and related process and tooling.


## Product 

**Team Size:** 21 (as of Jun. 2019)
**Projected Team Size:** 45 (by the end of Jan. 2020)
**Leadership Team:**
Scott Williamson, VP of Product
Kenny Johnston, Director of Product - Ops
Eric Brinkman, Director of Product - Dev
Jason Yavorska, Director of Product - CI/CD
**Useful links:**
[Product handbook](/handbook/product/)
[Product Manager job family](/handbook/product/product-manager-role/)
[Product process](/handbook/product/product-processes/)
[Product categories](/handbook/product/categories)

**Team vision:** The primary goal of the Product team is to consistently create products and experiences that customers love and value. Our vision is to replace disparate DevOps toolchains with a single application that is pre-configured to work by default across the entire DevOps lifecycle.

We aim to make it faster and easier for groups of contributors to deliver value to their users, and we achieve this by enabling:

Faster cycle time, driving an improved time to innovation
Easier workflows, driving increased collaboration and productivity

Our solution plays well with others, works for teams of any size and composition and for any kind of project, and provides ongoing actionable feedback for continuous improvement.


## Infrastruture (Reliability)

**Team Size:** 23 (as of Jun. 2019)
**Projected Team Size:** 24 (by the end of Jan. 2020)
**Leadership Team:**
Gerardo Lopez-Fernandez, Director of Engineering - Infrastructure
David Smith, Engineering Manager - Reliability Engineering, CI/CD and Enablement
Anthony Sandoval, Engineering Manager - Reliability Engineering, Secure and Protect
Jose Finotto, Engineering Manager - Reliability Engineering, Dev and Ops
**Useful links:**
[Infrastructure handbook](/handbook/engineering/infrastructure/team/)
[Site Reliability Engineer job family](/job-families/engineering/infrastructure/site-reliability-engineer/)
[Database Reliability Engineer job family](/job-families/engineering/infrastructure/database-reliability-engineer/)
[SRE stable counterparts and areas of ownership](/handbook/engineering/infrastructure/#sre-stable-counterparts-and-areas-of-ownership)

Team vision: We are a blend of operations gearheads and software crafters that apply sound engineering principles, operational discipline and mature automation to make GitLab.com ready for mission-critical customer workloads. We strive for excellence every day by living and breathing GitLab's values as our guiding operating principles in every decision we make and every action we take. 

Reliability: Reliability Engineering team is composed of DBREs and SREs. As the role titles indicate, they have different areas of specialty but focus on the reliability of the environment as the unifying goal.
Reliability Engineering teams own the following operational processes:

Change management
Incident management
Delta management

The teams' overarching goal with respect to these processes is to outdate them through automation.

