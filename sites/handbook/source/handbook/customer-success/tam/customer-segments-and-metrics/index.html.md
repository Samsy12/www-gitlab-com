---
layout: handbook-page-toc
title: "Customer Segments & Associated Metrics"
---

## Overview

The following outlines the customer segments supported by the Technical Account Management organization and the metrics for success for each segment:

## TAM-Assigned Segment

These accounts are those that pay >50k ARR or are >5k and have high [LAM](/handbook/sales/sales-term-glossary/#landed-addressable-market-lam)

#### Align

Success plans with milestones/key objectives in all Pr1 accounts.  These accounts are those that have either near-term growth opportunities or potential risk.  The success plans ensure that we are engaging around value and not features & functions.  This focus ensures that we are spending time on ensuring IACV growth, and defending gross retention.
Completed EBRs in minimum 75% of PR 1 accounts.  These strategic touchpoints ensure that progress against desired business outcomes is being made and communicated back to both the team that we work with and key influencers/decision makers.  It also creates a vehicle for introducing senior leadership into the customer account, enabling more strategic touchpoints.

1. Metric: Management-qualified Success Plans in ALL PR 1 accounts
1. Metric: EBRs completed in 75% of PR1 accounts
1. Metric: IACV on-target growth
1. Metric: Gross retention

#### Enable

The primary objective in enabling our customers is to get to value quickly, and to ensure that any roadblocks to adoption are removed for our customers.  These roadblocks can best be seen in our Time to 1st Value metric, whereby customers are delayed in inviting users into their instance if stuck on setup and rollout issues.  As product analytics become available within Gainsight, these metrics will mature into being initial onboarding and stage maturity.

1. Metric: [Time to first engage](/handbook/customer-success/tam/onboarding/#time-to-engage) (14 days, moving to 10 days in FY22): Requires a smooth transition from pre to post sales, ensuring that the TAM is set up to quickly move the customer from pre-sales POV state to post-sales full instance established
1. Metric: [Time to 1st Value](/handbook/customer-success/tam/onboarding/#time-to-first-value) (30 days) Indicates challenges in the account if not met in regard to initial roll-out; speaks to the success of the TAM in ensuring initial adoption roadblocks are removed
1. Metric: [Time to Onboard](/handbook/customer-success/tam/onboarding/#time-to-onboard) (45 days) Indicates when a customer is enablement-ready, and prepared to move to a more consistent cadence of check-ins as the customer continues to roll out their build.  This metric also speaks to the efficacy of the TAM engagement in the early weeks
1. FY22 Metric: Use Case/Stage Adoption Maturity Score: As we begin to have product analytics, we can score the platform adoption per use cases against our best practice benchmarks for mature product adoption, and more proactively address areas for improvement.  This today is done through cadence calls and qualitative conversations in the absence of analytics.

#### Expand

This phase is about going beyond a customer’s existing use cases, into additional stage adoption and tier upgrades.  In ensuring a customer gets to value quickly, in understanding a customer’s desired business outcomes, and in engaging strategically through touchpoints such as EBRs in addition to cadence calls, a TAM is ‘given permission’ as a trusted advisor to introduce and advocate for the idea of growth.

1. Metric: Stage Adoption Playbooks Completed
1. Metric: Days per Playbook Completed per Stage

## Onboarding Segment

These accounts are 20-50k in ARR. The focus of this program is ensuring these customers quickly move through initial adoption roadblocks to enable the path to IACV growth

#### Align

One of the key outputs of this program is a Success Plan that goes beyond feature adoption, enabling a SAL or AE to engage with the customer in their post-onboarding checkpoints around desired business outcomes.

1. Metric: Completed Success Plan per onboarding-completed customer to drive SAL/AE account planning and growth initiatives

#### Enable

This program's primary focus is to improve Time to 1st Value, removing initial roadblocks to adoption and avoiding poor initial set-up scenarios where use cases then fail and the GitLab platform is blamed. In the onboarding hand-off, the TAM ensures that the customer knows where to find what they need to be successful (Docs, HB, Support, Sales) and that the SAL or AE is given a strategic success plan to drive their future touchpoints.  This program is positioned as a weekly touchpoint (call) for six weeks, ensuring that the customer can quickly move to full-use case adoption.

1. Metric (leading): [Time to First Value](/handbook/customer-success/tam/onboarding/#time-to-first-value) as articulated above
1. Metric (lagging): Gross Retention (ensured through successful set-up and activation)

#### Expand

The expand motion for these customers is about ensuring 1) we have documented desired business outcomes and 2) roadblocks are removed so that the SAL and AE teams can drive expansion more quickly in these accounts, ensuring scenarios such as poor set up (leading to failing features) and ‘analysis paralysis’ do not occur, slowing down or impeding adoption.

1. Metric: Faster time to IACV growth in year 1 compared to the FY21 cohort

## Digital
These accounts are 5-20k ARR for onboarding, or 5-50k ARR for stage & use-case enablement.  The focus of this program is ensuring successful set-up, scale and expansion through digital enablement campaigns.

#### Align

We will engage through carefully placed surveys to garner key information such as desired use cases and outcomes,  customer satisfaction half-way through contract period (NPS, CSAT) and likelihood to renew.

1. Metric: 6-month NPS and CSAT results

#### Enable

We will seek to remove adoption roadblocks and poor setup scenarios (frequent issue) and enable use-case/stage adoption maturity through admin enablement via email.  In FY22 we will augment this email program with other vehicles such as downloadable issue boards, guided video paths, guided blog series, and Slack Bots.

1. Metric: Open Rate
1. Metric: Click-Through Rate
1. Metric: [Time to 1st Value](/handbook/customer-success/tam/onboarding/#time-to-first-value)
1. Metric: End of Onboarding NPS & CSAT results

### Expand

We have legal permission to email users for enablement only, so will seek to drive expansion through suggested next-steps via the FY22 vehicles such as issue boards with task lists, while continuing to partner with other teams such as the Marketing and Growth teams on use case adoption and maturity with key next steps into new stages via key points such as introducing SAST/DAST scanning (leading to Secure, Ultimate).

1. Metric: Faster time to IACV growth against FY21 cohort
