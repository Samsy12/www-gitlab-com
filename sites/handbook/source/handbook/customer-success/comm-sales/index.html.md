---
layout: handbook-page-toc
title: "Commercial Sales - Customer Success"
description: "The Commercial Sales segment consists of two sales teams, Small Business (SMB) and Mid-Market (MM)"
---

# Commercial Sales - Customer Success Handbook
{:.no_toc}

GitLab defines Commercial Sales as worldwide sales for the mid-market and small/medium business segments. [Sales segmentation](/handbook/sales/field-operations/gtm-resources/#segmentation) is defined by the total employee count of the global account. The Commercial Sales segment consists of two sales teams, Small Business (SMB) and Mid-Market (MM). The Commercial Sales segment is supported by a dedicated team of Solutions Architects (SA) and Technical Account Managers (TAM).

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Role & Responsibilities

### Solutions Architects

Solutions Architects are aligned to the Commercial Sales Account Executives by a pooled model. Requests for an SA will be pulled from the triage board by an SA based on multiple factors including availability, applicable subject matter expertise, and current workload.  

* [Engage a Commercial Solutions Architect](/handbook/customer-success/solutions-architects/processes/commercial/)
* [Solutions Architect role description](/job-families/sales/solutions-architect/)
* [Solutions Architect overview](/handbook/customer-success/solutions-architects/)

### Technical Account Managers

Technical Account Managers that support Commercial Sales are aligned by region (Americas East, Americas West, EMEA, and APAC). Not all accounts will have a dedicated TAM. Account qualification is required.

* [Technical Account Manager role description](/job-families/sales/technical-account-manager/)
* [Technical Account Manager overview](/handbook/customer-success/tam/)
* [When and how a TAM is engaged](/handbook/customer-success/tam/engagement/)

## Sales Engagement Guidelines

Sales engagement for all Commercial Sales is documented in the [Commercial Process](/handbook/customer-success/solutions-architects/processes/commercial/) page.

## Customer Engagement Guidelines

### Ongoing Customer Communication

For commercial accounts, we do not currently offer our customers Slack channels unless they meet specific criteria and it would be highly beneficial to both the customer and GitLab teams. Prior to creating the channel and provisioning access, the TAM for the account must approve its creation and use. To qualify, the customer should be at minimum:
- Premium/Silver tier or above
- 100k+ IACV

[Collaborative projects](/handbook/customer-success/tam/engagement/#managing-the-customer-engagement) may be suggested with few possible exceptions (such as reference customers, early adopters or strategic partnerships).  If the customer is evaluating GitLab or doing a POV for an upgrade, SAs or TAMs can request an ephemeral Slack channel, but the channel should be closed within 90 days if it does not meet the criteria shown above. Slack channels are simply too intensive to scale for Mid-Market and SMB.

It is also not scalable for every customer to have a collaboration project, and the CS team will determine the need for a project on a per-customer basis.

## Seamless Customer Journey

A seamless customer journey requires a continuous flow of relevant information between the roles at GitLab with customer outcomes in focus. Below are some examples of the transfer of information between roles that may be required.

### TAM to TAM (existing accounts)

* Ensure all applicable accounts have a GitLab project [here](https://gitlab.com/gitlab-com/account-management/commercial)
* Update TAM name on account team in Salesforce
* Share any Outreach sequences or templates currently in use
* The new TAM should be introduced live on a client call whenever possible
* Ensure any current action items are identified via an issue on the [Commercial TAM Triage board](https://gitlab.com/gitlab-com/account-management/commercial/triage/boards/1139879?&label_name[]=Status%3A%3ANew)
* Evaluate the state of the account's license including an assessment of active users and if a true-up is necessary
* Evaluate the state of tickets to ensure any open critical issues are addressed
* Retrieve any architectural diagrams, machine specifications, and gitlab configuration files to assess current environment
* Review the [Account Handoff](/handbook/customer-success/tam/account-handoff/) checklist for additional info

### Account Executive to TAM (existing accounts without a TAM)

* AE or SA qualifies the account [meets the threshold for TAM services](/handbook/customer-success/tam/tam-manager/#account-assignment) and raises the account requiring a TAM in the SAL-SA-TAM meeting. TAM managers also review their 'accounts requiring TAM' dashboard in Gainsight on a weekly basis as a fail-safe.
* SA or TAM creates a GitLab project [here](https://gitlab.com/gitlab-com/account-management/commercial) if applicable
* Add TAM name to account team in Salesforce
* Identify any relevant Outreach sequences or templates
* The new TAM should be introduced live on a client call whenever possible
* Ensure any current action items are identified via an issue on the [Commercial TAM Triage board](https://gitlab.com/gitlab-com/account-management/commercial/triage/boards/1139879?&label_name[]=Status%3A%3ANew)

### SA to TAM (new accounts)

* SA completes the [New Customer Transition Template](https://docs.google.com/document/d/1Zl8fho1PYyQ-jdpKrG-lM2bRp_R2oMu4SY8anSNLxUE/edit#heading=h.g51kgr7ordew) (available only to GitLab team members) and stores it in the
* Ensure any account notes, [tech stack discoveries](https://docs.google.com/spreadsheets/d/1sOeluQhMO4W0wWIC6rbSE_E1NzTj7eTaR-FDKLYlLb4/edit#gid=912439232), [technical briefs](https://gitlab.com/gitlab-com/customer-success/tko/technical-followup-briefs/-/tree/master) or running call notes are linked to Salesforce and shared with the TAM
* SA to clearly outline to TAM how far the customer is in their adoption of GitLab
* TAM begins onboarding process for new customers
* Add TAM name to account team in Salesforce, ensure SA name is already present
* Ensure any urgent action items are identified via an issue on the [Commercial TAM Triage board](https://gitlab.com/gitlab-com/account-management/commercial/triage/boards/1139879?&label_name[]=Status%3A%3ANew)

### SA to SA (new accounts)

* Update SA name on account team in Salesforce
* Ensure any account notes, [tech stack discoveries](https://docs.google.com/spreadsheets/d/1sOeluQhMO4W0wWIC6rbSE_E1NzTj7eTaR-FDKLYlLb4/edit#gid=912439232), [technical briefs](https://gitlab.com/gitlab-com/customer-success/tko/technical-followup-briefs/-/tree/master) or running call notes are linked to Salesforce and shared with the new SA
* Introduce new SA live on a client call
* If a [POV](/handbook/sales/POV/) is pending or active, update the POV record in Salesforce as required
* Ensure any current action items are identified via an issue on the [Commercial SA Triage board](https://gitlab.com/gitlab-com/customer-success/sa-triage-boards/commercial-triage/boards/1006966)
