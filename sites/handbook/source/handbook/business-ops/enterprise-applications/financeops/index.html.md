---
layout: handbook-page-toc
title: "Finance Systems"
description: "Finance Systems Operations"
---

<link rel="stylesheet" type="text/css" href="/stylesheets/biztech.css" />

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## <i class="fas fa-hand-holding-usd" id="biz-tech-icons"></i> Finance Operations

### Who we are

**Alex Westbrook - Senior Finance Systems Admin**  
GitLab handle: [@awestbrook](https://gitlab.com/awestbrook)  
Slack handle: @Alex_W  
Job Family: [Senior Finance Systems Admin](/job-families/finance/finance-systems-administrator/#senior-finance-systems-administrator)

**Mark Quitevis - Finance Systems Admin**  
GitLab handle: [@mquitevis](https://gitlab.com/mquitevis)  
Slack handle: @Mark Quitevis  
Job Family: [Finance Systems Admin](/job-families/finance/finance-systems-administrator/#finance-system-administrator)

### Contacting us
Slack: `#financesystems_help`  
GitLab: [Create an issue](TBD)

### Our mission

TBD

### Our priorities

- Technical and Operational owner of the finance application ecosystem partnering with Finance and Accounting
- Maintains and optimizes the integrations of the ecosystem
- Partner to Sales Ops, Sales Systems, Growth Teams and other departments where the integrations intersect and the data passes from one system into another


##### What we are working on?

<a href="https://gitlab.com/groups/gitlab-com/-/boards/1722830?assignee_username=awestbrook&" class="btn btn-purple">Work Management Board</a>

- Backlog and In progress issues related to all [finance systems](/handbook/business-ops/enterprise-applications/#finance-systems-covered)

##### Types of Support

1. Access Request or change in access: [Queue](https://gitlab.com/groups/gitlab-com/-/boards/1765444?&label_name[]=FinSys%20-%20Access%20Request).
    Submit [issue](handbook/business-ops/employee-enablement/onboarding-access-requests/access-requests/).
1. Breaks, bugs and incidents related to a system.
    Submit [issue](https://gitlab.com/gitlab-com/business-ops/financeops/finance-systems/-/issues/new).
1. Enhancement Request for a system.
    Submit [issue](https://gitlab.com/gitlab-com/business-ops/financeops/finance-systems/-/issues/new).
1. Other and questions.
    Submit [issue](https://gitlab.com/gitlab-com/business-ops/financeops/finance-systems/-/issues/new).

##### Finance Systems Covered

1. Zuora: [Board](https://gitlab.com/groups/gitlab-com/-/boards/1723367?label_name[]=FinSys%20-%20Zuora) with everything slated to be done.
1. Netsuite
1. Tipalti
1. Expensify
1. Stripe
1. TripActions
1. Avalara
1. CaptivateIQ
1. Workiva
1. FloQast
1. Adaptive Planning

##### _Coming Soon_

1. Z-Revenue (RevPro)
1. Xactly
1. Mavenlink 
1. EdCast

#### What's the status of my request?

- Every issue will have a tag of either
- ~"BT PS:: Backlog" > Unless a due date is indicated or urgency specified, non-access related issues will go into the backlog and prioritized bi-weekly
- ~"BT PS::To Do" > Team will look at the issue within a week of submitting
- ~"BT PS::In Progress" > Team is currently actively workiing on scoping out and gathering requirements
- ~"BT PS::Done"

##### Change Process

1. [Issue](https://gitlab.com/gitlab-com/business-ops/financeops/finance-systems/-/issues/new) submitted with request
1. Request is approved by technical owner and business owner (as necessary).
    ([Approvals Queue](https://gitlab.com/groups/gitlab-com/-/boards/1774935))
1. Change pushed to sandbox/dev environment (as necessary)
1. Change validated
1. Change deployed to production environment
