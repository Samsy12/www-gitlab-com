---
layout: handbook-page-toc
title: "xMAU Analysis"
---

## On this page
{:.no_toc}

- TOC
{:toc}

---

## xMAU Analysis

xMAU is a single term to capture the various levels at which we capture Monthly Active Usage (MAU), encompassing Action (AMAU), Group (GMAU), Stage (SMAU), and Total (TMAU). In order to provide a useful single metric for product groups which maps well to product-wide Key Performance indicators, each xMAU metric cascades upwards in the order noted above. 

This Analytics Workflow enables the analysis of each level of xMAU metric across various segments of customers sets the foundation for reporting on [Reported, Estimated, and Projected](https://about.gitlab.com/handbook/product/performance-indicators/#three-versions-of-xMAU) metrics.

### Knowledge Assessment & Certification

`Coming Soon`

### Data Classification

Some data supporting xMAU Analysis is classified as [Orange](/handbook/engineering/security/data-classification-standard.html#orange) or [Yellow](/handbook/engineering/security/data-classification-standard.html#yellow). This includes ORANGE customer metadata from the account, contact data from Salesforce and Zuora and GitLab's Non public financial information, all of which shouldn't be publicly available. Care should be taken when sharing data from this dashboard to ensure that the detail stays within GitLab as an organization and that appropriate approvals are given for any external sharing. In addition, when working with row or record level customer metadata care should always be taken to avoid saving any data on personal devices or laptops. This data should remain in [Snowflake](/handbook/business-ops/data-team/platform/#data-warehouse) and [Sisense](/handbook/business-ops/data-team/platform/periscope/) and should ideally be shared only through those applications unless otherwise approved. 

**ORANGE**

- Description: Customer and Personal data at the row or record level.
- Objects:
  - `dim_billing_accounts`
  - `dim_crm_accounts`
  - `usage_ping_mart`

### Solution Ownership

- Source System Owner:
    - Versions: `@jeromezng`
    - Gitlab.com: `TBD`
    - Salesforce: `@jbrennan1`
    - Zuora: `@andrew_murray`
- Source System Subject Matter Expert:
    - Versions: `@jeromezng`
    - Gitlab.com: `TBD`
    - Salesforce: `@jbrennan1`
    - Zuora: `@andrew_murray`
- Data Team Subject Matter Expert: `@mpeychet_` `@m_walker`

### Key Terms

- [Account](/handbook/sales/sales-term-glossary)
- Account Instances - the total number of reported instances that can be mapped to an account
- [Host](/handbook/product/product-intelligence-guide/#event-dictionary)
- [Instance](/handbook/product/product-intelligence-guide/#event-dictionary)
- Instance User Count - the total number of users on an instance
- [Paid User](/handbook/product/performance-indicators/#paid-user)
- [Product Tier](/handbook/marketing/strategic-marketing/tiers/#overview)
- [Usage Ping](/handbook/product/product-intelligence-guide/#usage-ping)
- [Version](/handbook/sales/process/version-check/#what-is-the-functionality-of-the-gitlab-version-check)

### Key Metrics, KPIs, and PIs

Explanations for the metrics below can be found on [the Product Team Performance Indicator page](/handbook/product/performance-indicators/#structure):
- [Action Monthly Active Users (AMAU)](/handbook/product/performance-indicators/#action-monthly-active-users-amau)
- [Stage Monthly Active Users (SMAU)](/handbook/product/performance-indicators/#stage-monthly-active-users-smau)
- [Section Monthly Active Users (Section MAU)](/handbook/product/performance-indicators/#structure)
- [Section Total Monthly Active Users (Section TMAU)](https://about.gitlab.com/handbook/product/performance-indicators/#structure)
- [Total Monthly Active Users (TMAU)](/handbook/product/performance-indicators/#structure)

Each metric has three different versions (Recorded, Estimated, Projected), explained on
  - [the Product Team Performance Indicator page](/handbook/product/performance-indicators/#three-versions-of-xmau) 
  - [the Sisense Style Guide](/handbook/business-ops/data-team/platform/sisense-style-guide/#recorded-and-calculated-data)
Currently, recorded metrics that have identified usage ping metrics have been charted on the Centralized Dashboard, but we are working on our first version of Estimated values [in this issue](https://gitlab.com/gitlab-data/analytics/-/issues/6547#note_429610192).

#### Difference between xMAU and Paid xMAU

##### Paid xMAU Definition

Each of these above metrics will be calculated for xMAU and paid xMAU. Paid xMAU is currently defined as Monthly Active Users who "roll up to a paid instance for Self-Managed via Usage Ping data _or_ a paid namespace for SaaS via GitLab.com Postgres Database Imports in a 28 day rolling period". (See [Paid Stage Monthly Active Users - Paid SMAU](https://about.gitlab.com/handbook/product/performance-indicators/#paid-stage-monthly-active-users-paid-smau) as an example.) 

Since GitLab Business functions currently do not have a standardized way to identified which namespaces or instances belong to an OSS, EDU, internal projects, or other subscriptions that have a paid plan type but do not contribute ARR, the current implementation of xMAU include users associated with these subscriptions as "paid". 

##### Calculation of xMAU and Paid xMAU

We have 2 main data sources to calculate xMAU and paid xMAU, the Version App and the Gitlab.com Postgres database. The table below summarises which data source is used for those calculations.

|   Delivery   | xMAU         | Paid xMAU                   |
|--------------|--------------|-----------------------------|
| SaaS         | version app* | Gitlab.com Postgres Table** |
| Self-Managed | version app  | version app***              |

_Notes_:

\*: For SaaS xMAU, we use the payloads generated for gitlab.com. These payloads are easily identifiable since they are linked to an instance with uuid = `ea8bf810-1d6f-4a6a-b4fd-93e8cbd8b57f`

\*\*: For SaaS paid XMAU, we need to use Gitlab.com postgres table. The Usage Ping payloads generated from our Gitlab.com Instance gives us high-level statistics at an instance level. This is an aggregated number which can't be further broken down, for example by product tier, plan type, or namespace. 
  * To be able to generate SaaS paid xMAU we need to replicate the xMAU counters with the replica of the Gitlab.com database which is stored in our data warehouse. 
    * [Batch counters](https://docs.gitlab.com/ee/development/product_analytics/usage_ping.html#ordinary-redis-counters) are simple SQL-generated counters. The SQL query used to generate the counters are accessible and easily recreated. 
    * Unfortunately, this is not doable for every single counter. [Redis counters](https://docs.gitlab.com/ee/development/product_analytics/usage_ping.html#redis-counters) are NOT SQL-generated counters. They also track actions that are not in the Postgres DB such as pageviews, or frontend interactions. 
  * Therefore, only some metrics can be recreated using the Gitlab.com Postgres Replica. That means that for now, we are not able to calculate some of the SaaS Paid xMAU metrics like the Monitor Stage.

\*\*\*: To calculate paid xMAU on Self-Managed we use the `edition` field in the [Usage Ping Payload](https://docs.gitlab.com/ee/development/product_analytics/usage_ping.html#example-usage-ping-payload), selecting only usage pings with `EEP`, `EES` and `EEU` edition. The edition value is derived from the [plan column in the license table in the licenseDot database at the time the license was generated](https://gitlab.com/gitlab-data/analytics/-/issues/7257#note_464118474)

### Key Dimensions

The models have the following dimensions on which users can segment the data by:

- [delivery](https://about.gitlab.com/handbook/marketing/strategic-marketing/tiers/#delivery)
- [product_tier](https://about.gitlab.com/handbook/marketing/strategic-marketing/tiers/#definitions)
- [distribution](https://about.gitlab.com/handbook/marketing/strategic-marketing/tiers/#definitions)

The models will be expanded to include the following dimensions:

- is_paid_instances
- country
- is_paid_subscription
- is_edu_oss_subscription
- is_trial

We are currently coducting [a survey](https://gitlab.com/gitlab-data/analytics/-/issues/6936) to identify more dimensions the Product Team could be interested in.

### Going from Recorded to Estimated xMAU

For the Self-Managed instance that hosts our SaaS GitLab.com product, we get the accurate value via Usage Pings. For other Self-Managed instances, customers have the option to turn disable Usage Ping; therefore, we would not receive any usage statistics for that instance. In order to calculate estimated usage values across our Self Managed customers, we need to develop an algorithm to predict usage across instances who have disabled their Usage Ping.

This section explains our first attempt in these xMAU estimations.

#### Current Methodology

##### What we know

* The number of Active subscriptions per month
* The number of Active subscriptions that send us a Usage Ping payload per month broken down by version
* The release date (therefore month) of a specific xMAU counter in Usage ping. For example we know that the `merge_requests_users` which is the GMAU of the Create - Source Code Group was released in version 12.9

##### What we don't know

* Number of Active Free Instances 
  * The number of Active CE Free Instances 
  * The number of Active EE Free Instances 

##### What we can do 

**General Methodology** 

* Calculate per month, the paid opt-in rate: 
  * For October, we can match 3500 self-managed subscriptions to at least one Usage Ping Payload out of 5000 subscriptions
  * Among them we split between subscriptions that are on a version 13.3 or above and the ones which have 13.2 or below and the ones which don't send us any payload.
  * [This SiSense chart saw the split per month](https://app.periscopedata.com/app/gitlab/602123/Data-For-Product-Managers:-Supporting-Dashboard?widget=10065654&udv=953103)
  * We get from the chart above a % of subscriptions that send us payloads and that are on 13.4 or above out of all Active Subscriptions
* Calculate recorded GMAU split by delivery
* We then deduce that Estimated Self-Mangaged SMAU = Recorded Self-Managed SMAU / % on 13.3
* Then we calculate the Estimated SMAU as Recorded SaaS SMAU + Estimated Self-Managed SMAU

**Example** 

Let's discuss a real-life example! The counter used as SMAU for Dev:Plan stage is `usage_activity_by_stage.plan.issues`. It was released on version 12.9 for EE and version 13.3 for CE. To calculate the Estimated SMAU, we follow this process: 

1. Identify the version in which the counter was first released on CE and EE. [Available in this chart](https://app.periscopedata.com/app/gitlab/789044/Estimation-Methodology-Experimentation-Dashboard?widget=10472805&udv=0)
1. Calculate 2 opt-in rates proxies for CE and EE by calculating the ratio of paid of subscriptions that can be tied to an instance that on a given month: 
  (a) have an instance version greater or equal to the version that the metric was released (for both CE and EE)  
  (b) reported a value for the metric `usage_activity_by_stage.plan.issues` 
  ![percent_subscription_match_action_monthly_active_users_project_repo](/handbook/business-ops/data-team/data-catalog/xmau-analysis/images/percent_subscription_match_action_monthly_active_users_project_repo.png)
1. Calculate the recorded GMAU split by delivery (SaaS, CE/EE)
1. Calculate the estimated Estimated Self-Mangaged SMAU by Self-Managed Edition (CE/EE).
  ![estimated_recorded_values_for_action_monthly_active_users_project_repo](/handbook/business-ops/data-team/data-catalog/xmau-analysis/images/estimated_recorded_values_for_action_monthly_active_users_project_repo.png)
1. Calculate the Estimated SMAU = Recorded SaaS SMAU + Recorded Self-Managed SMAU + Estimated Self-Managed SMAU Uplift
  - Estimated Self-Managed SMAU Uplift = Estimated Self-Managed SMAU - Recorded Self-Managed SMAU because we want to know what the lift is or the additional add by estimating. 
  ![estimated_smau_split](/handbook/business-ops/data-team/data-catalog/xmau-analysis/images/estimated_smau_split.png)

* [We created a table that shows the different ratios used to calculate SMAU Estimated Value broken down by stage and edition.](https://app.periscopedata.com/app/gitlab/789044/Estimation-Methodology-Experimentation-Dashboard?widget=10472805&udv=0)
* Over time, more instances will logically upgrade to newer versions, therefore, Estimated Uplift will tend to shrink while Recorded will keep on growing.


**Additional Notes** 

This [issue](https://gitlab.com/gitlab-data/analytics/-/issues/7020) walks through all the data models that were used to create this methodology. 

Since counters are released on different GitLab version, the estimation is customised form one counter to another. 

This number is actually a quite accurate number to estimate paid XMAU but can be used at first for calculating estimated xMAU. Though we can imagine several improvements in order to make this estimator more robust:

* The first improvement would be to use the sum of the seat quantity ordered instead of the number of subscriptions.
* We could also break down the estimator per plan (current problem: a subscription can have 2 instances from 2 different product tiers)
* A major area of improvement is regarding our Core estimation. We currently assume that we have the same patterns between paid and free in terms of version upgrade and usage ping opt-in. While usage ping opt-in rate estimation seems very complicated to improve an critical area of improvement would be around version upgrade and usage data trend for Core Instances

## Self-Service Data Solution

### Self-Service Dashboard Viewer

| Dashboard                                                                                                    | Purpose |
| ------------------------------------------------------------------------------------------------------------ | ------- |
| Executive Overview - TBD | This dashboard presents an executive overview of the current status of all metrics. |
| Dev Section Analysis - TBD | This dashbaord presents a section overview of the current status of related metrics. |
| [DRAFT: Centralized Dashboard for Handbook Updates](https://app.periscopedata.com/app/gitlab/758607/WIP-SMAU-GMAU-Mathieu) | The charts on this dashboard are used for handbook embeds. |

### Self-Service Dashboard Developer

A great way to get started building charts in Sisense is to watch this 10 minute [Data Onboarding Video](https://www.youtube.com/watch?v=F4FwRcKb95w&feature=youtu.be) from Sisense. After you have built your dashboard, you will want to be able to easily find it again. Topics are a great way to organize dashboards in one place and find them easily. You can add a topic by clicking the `add to topics` icon in the top right of the dashboard. A dashboard can be added to more than one topic that it is relevant for. Some topics include Finance, Marketing, Sales, Product, Engineering, and Growth to name a few. 

#### Self-Service Dashboard Developer Certificate

`Coming Soon`

### Self-Service SQL Developer

#### Key Fields and Business Logic

`Coming Soon`

#### Self-Service SQL Developer Certificate

`Coming Soon`

#### Snippets

We built several snippets to make it easy for you to chart the different xMAU relative to your section, stage or groups.

##### [reported_section_tmau(section)]

This snippet will retrieve Reported Total Monthly Active Users at the section level. This snippet is using usage ping as the only source of truth.

**Parameters:**

- section: the name of the section (not capitalized, so the accepted values are `dev`, ). You can also use `All` as a value for this parameter. It will then retrieve ALL sections.

**Dimensions:**

- section_name
- delivery

Example SQL Chart in Periscope (you can copy and paste this SQL reference in a New chart Editor in SiSense):

```
[reported_section_tmau('dev')]
```

[Example chart](https://app.periscopedata.com/app/gitlab/758607/WIP-SMAU-GMAU-Mathieu?widget=9982128&udv=1146726)

##### [reported_smau(section, stage, target)]

This snippet will retrieve Reported Stage Monthly Active Users at the stage level. This snippet is using usage ping as the only source of truth.

**Parameters:**

- section: the name of the section (not capitalized, so the accepted values are `dev`, ).
- stage: the name of the section (not capitalized, so the accepted values are `dev`, ) in camel case. You can also use `All` as a value for this parameter. It will then retrieve ALL sections.
- target: to get a target line added to the charts. If you don't have a target, you can simply set this to 0 and it won't show.

**Dimensions:**

- section_name
- stage_name
- delivery

Example SQL Charts in Periscope (you can copy and paste this SQL reference in a New chart Editor in SiSense):

```
[reported_smau('dev', 'create', 10000000)]
```

And without target:

```
[reported_smau('dev', 'create', 0)]
```

[Example chart 1](https://app.periscopedata.com/app/gitlab/758607/WIP-SMAU-GMAU-Mathieu?widget=9967642&udv=1146726)
[Example chart 2](https://app.periscopedata.com/app/gitlab/758607/WIP-SMAU-GMAU-Mathieu?widget=9992267&udv=1146726)

##### [reported_gmau(stage, group, 0)]

This snippet will retrieve Reported Group Monthly Active Users at the group level. This snippet is using usage ping as the only source of truth.

**Parameters:**

- stage: camel-case'd name of the stage (not capitalized, so the accepted values are `dev`, ). You can also use `All` as a value for this parameter. It will then retrieve ALL sections.
- group: camel_case'd name of the group
- target: to get a target line added to the charts. If you don't have a target, you can simply set this to 0 and it won't show.


**Dimensions:**

- stage_name
- group_name
- delivery

Example SQL Charts in Periscope (you can copy and paste this SQL reference in a New chart Editor in SiSense):

```
[reported_smau('create', 'gitaly', 10000000)]
```

And without target:

```
[reported_smau('monitor', 'monitor', 0)]
```

#### How to update targets for a specific xMAU chart using these snippets ?

For all embedded PI/xMAU charts using our standardized snippets and visualisation, updating the target is a very easy 4-step process:

1. Go to the PI of interest and click on the sisense link  
![pi_page](/handbook/business-ops/data-team/data-catalog/xmau-analysis/images/pi_page.png)
1. Click on Edit Chart  
![edit](/handbook/business-ops/data-team/data-catalog/xmau-analysis/images/edit_chart.png)
1. Change the last value (that's the target value - if you do a decimal it will increase by a certain percentage. If you type in a full number, that number will be the target line.)  
![target](/handbook/business-ops/data-team/data-catalog/xmau-analysis/images/target_change.png)
1. Click the Save Button  
![save](/handbook/business-ops/data-team/data-catalog/xmau-analysis/images/save_chart.png)

#### Reference SQL

`Coming Soon`

#### Data Discovery Function in Sisense

If you are not familiar with SQL, there is the Data Discovery function in Sisense wherein you can create charts through a drag-and-drop interface and no SQL query is needed.

##### How to work with the Data Discovery Function

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/h_b9A8F7Ic8" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

More information here on the [Data Discovery page in Sisense](https://dtdocs.sisense.com/article/data-discovery).

#### Entity Relationship Diagrams

| Diagram/Entity                                 | Grain                                               | Purpose                                                                              |
|------------------------------------------------|-----------------------------------------------------|--------------------------------------------------------------------------------------|
| [Fact Monthly Usage Ping Data ERD and Data Flow](https://app.lucidchart.com/lucidchart/invitations/accept/b1a46304-f878-4c90-9252-d76ce4015b7a) | host_id, instance_id, reporting_month, metrics_path | Provides insights into Feature Usage by various instance and subscription dimensions |

## Data Platform Solution

### Data Lineage

`Coming Soon`

### DBT Solution

`Coming Soon``

## Trusted Data Solution

[Trusted Data Framework](https://about.gitlab.com/handbook/business-ops/data-team/direction/trusted-data/)

### EDM Enterprise Dimensional Model Validations

`Coming Soon`

### RAW Source Data Pipeline validations

`Coming Soon`

### Manual Data Validations

* [Manual Usage Ping Validation Dashboard](https://app.periscopedata.com/app/gitlab/762611/Manual-Usage-Ping-Validation)
