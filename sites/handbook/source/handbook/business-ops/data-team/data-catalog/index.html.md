---
layout: handbook-page-toc
title: "Data Catalog"
---
{::options parse_block_html="true" /}

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .toc-list-icons .hidden-md .hidden-lg}

---

## Introduction

This page is the list of GitLab Self-Service [Analytics Workflows](/handbook/business-ops/data-team/data-catalog/#analytics-workflow) and [Data Definitions](/handbook/business-ops/data-team/data-catalog/#data-definition), collectively known as **The Data Catalog**. To maintain a Single Source of Truth for Trusted [Self-Service Data](/handbook/business-ops/data-team/direction/self-service/), content is organized by function and maintained within the Data Team handbook. In cases where a Data Subject (such as Product SKU) crosses function boundaries, content will reside alongside the function that owns the source system of record (e.g. Product SKU will reside in Finance because Zuora is the SSOT for SKUs).

### Analytics Workflow

An _Analytics Workflow_ is a routine activity undertaken to analyze the performance of a well-defined business process. A given business process will likely have one or more supporting Analytics Workflows. As an example, the Marketing Lead Generation business process might include analytics workflows for Email Campaign Performance Analytics, Lead Qualification Analytics, and Pipeline Generation Analysis. Analytics Workflows are purposefully designed with established business processes in mind.

```mermaid
graph TD
  subgraph Business Process
    subgraph Analytics Workflows 1..n
      KPIs
      Dashboards
      Ad-Hoc-SQL
    end
  end    
```

#### Analytics Workflow Development

1. Create a merge request to add the Analytics Workflow definition to the the appropriate section of the Data Catalog.
1. Define the Analytics Workflow page as completely as possible with all of its parts based on the [Level 2 Reference](/handbook/business-ops/data-team/direction/reference/).
1. Self-Service Development is considered complete once all key parts are operationalized, a Knowledge Assessment is published, and the Self-Service entry on this page is tagged with the appropriate [operational status](/handbook/business-ops/data-team/data-catalog/#legend).

### Data Definition

The Data Definition is a standard definition and set of use cases used as part of an Analytics Workflow.

#### Data Definition Development Workflow

1. Create a merge request to add the Data Definition to the appropriate section of the Data Catalog. If a Data Definition is part of an existing Subject Area page, only add a link to it from the Data Catalog Index and do not create a new stand-alone page with the Definition.
1. Define the Data Definition page as completely as possible with all of its parts, including a business summary and links to source systems of record and dashboards.
1. Data Definition is considered complete once all key parts are defined and the entry on this page is tagged with the appropriate [operational status](/handbook/business-ops/data-team/data-catalog/#legend).

### Data Catalog 

<style> #headerformat {
background-color: #554488; color: white; padding: 5px; text-align: center;}
</style>

<h1 id="headerformat">Finance </h1>

  * <a href="https://about.gitlab.com/handbook/business-ops/data-team/data-catalog/finance-arr/" > 🚧 WIP Self-Service Analytics Workflow: Finance ARR</a>

<style> #headerformat {
background-color: #554488; color: white; padding: 5px; text-align: center;}
</style>

<h1 id="headerformat">Marketing </h1>

  * Coming Soon!

<style> #headerformat {
background-color: #554488; color: white; padding: 5px; text-align: center;}
</style>

<h1 id="headerformat">People </h1>

[People Metrics Overview](https://about.gitlab.com/handbook/business-ops/data-team/data-catalog/sites/handbook/source/handbook/business-ops/data-team/data-catalog/people-analytics/index.html.md)

<details>
<summary markdown='span'>
  Data Models
</summary>  

  * <a href="#" class="https://about.gitlab.com//handbook/business-ops/data-team/data-catalog/people-analytics/pto/pto.html.md">PTO By Roots (Slack)</a>
  
</details>

<details>
<summary markdown='span'>
  Data Solutions
</summary>  
  * Coming Soon
</details>

<style> #headerformat {
background-color: #554488; color: white; padding: 5px; text-align: center;
}
</style>
<h1 id="headerformat">Product </h1>

<details>
<summary markdown='span'>
  Data Solutions
</summary>  
  * <a href="https://about.gitlab.com/handbook/business-ops/data-team/data-catalog/product-geolocation/" class="">📊 Self-Service Analytics Workflow: Product Geolocation Analysis</a><br><br>
  * <a href="https://about.gitlab.com/handbook/business-ops/data-team/data-catalog/pricing/" class="">🚧 WIP Self-Service Analytics Workflow: Pricing Analysis</a><br><br>
  * <a href="https://about.gitlab.com/handbook/business-ops/data-team/data-catalog/xmau-analysis/" class="">🚧 WIP Self-Service Analytics Workflow: XMAU Analysis</a><br><br>
</details>


<details>
<summary markdown='span'>
  Data Definitions
</summary>  
  * <a href="https://about.gitlab.com//handbook/product/performance-indicators/#structure/" class="">Data Definition: XMAU</a>
  * <a href="https://about.gitlab.com/handbook/product/product-categories/#devops-stages/" class="">Data Definition: Product Stage</a>
</details>


### Legend

📊 indicates that the solution is operational and is embedded in the handbook.

🚧 indicates that the solution is in a `Work In Progress` and is actively being developed. When using this indicator, an issue should also be linked from this page.

🐔 indicates that the solution is unlikely to be operationalized in the near term.
