---
layout: handbook-page-toc
title: "Yorick Peterse's README"
job: "Staff Backend Engineer, Delivery"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Introduction

My name is Yorick Peterse and I am a Staff Backend Engineer as part of the
[Delivery team][delivery], and I live in The Netherlands.

## Links

* [GitLab.com account](https://gitlab.com/yorickpeterse)
* [dev.gitlab.org account](https://dev.gitlab.org/yorickpeterse)
* [ops.gitlab.net account](https://ops.gitlab.net/yorickpeterse)
* [Team page](/company/team/#yorickpeterse)

## Company History

I joined GitLab in October 2015 as a "Performance Specialist", with my goal
being to improve performance of GitLab as a whole. At the time GitLab only had
around 40 employees, whereas today we have roughly ten times the number of
employees.

After working as a Performance Specialist for a while, I transitioned into a
"Database Specialist" position, which was similar but focused exclusively on
database performance. After working on performance for almost three years, I
joined the newly formed [Delivery team][delivery] team.

## Personality

I take great pride in the work I do, have a strong sense of justice, and can be
very direct at times; bordering on the edge of being too harsh. While I am
actively working on keeping this at reasonable levels, it's important to keep in
mind that I never intend to make anybody uncomfortable through my words or
actions. If this is the case, by all means let me know.

I tend to express frustration rather vocally, especially when encountering
problems we encountered (many times) in the past. It's important to keep in mind
that this frustration is never directed at the people to triggered the problem,
but instead is directed towards the problem itself and it not being solved yet.

## Focus

These days I focus on two goals:

1. Improving the deployment pipeline for GitLab.com. This would be done by
   simplifying the process to deploy changes, building the tools necessary to
   achieve this, and so forth.
1. Making it easier for developers to develop their code, thereby making it also
   easier to ship it. This goal focuses more on code quality, development
   workflows, and basically everything that happens before we begin deploying
   it.

Occasionally I may submit some performance improvements, but this is no longer
my primary focus.

## Work Schedule & Meetings

My work schedule is largely dominated by my inconsistent sleeping schedule, a
problem I have struggled with for almost my entire life. This means one day I
may start working at 10:00 in the morning, while on another day I start at
12:00.

In general, I am available between 11:00 and 18:00 UTC for meetings. When
scheduling one, please make sure to schedule it _at least_ 24 hours in advance.
I try to limit the number of meetings to at most two, _maybe_ three meetings a
week.

It's also worth mentioning that I tend to have Slack closed for most of the day,
only checking it periodically (every hour or so). This means that it may take a
little while for me to reply to any direct messages or @ mentions.

## Work Environment

I work exclusively from home, as anything but [the finest of chairs][aeron] will
hurt my back and neck way too much. I use Linux exclusively for work using a
combination of a Linux desktop and laptop (an X1 Carbon). My editor of choice is
(Neo)Vim, and has been for almost 10 years. I recently learned how to exit Vim
by typing `:q10283asldkajsdlk123890!!!!!` and hoping for the best.

My keyboard is an [Ergodox EZ][ergodox], using a customised Colemak layout.  My
mouse is a [Logitech MX Ergo][mx-ergo] trackball. My display is a [Dell
P2715Q][display].

I have a standing desk (a Galant 13662, which are no longer sold), and I
alternate between standing and sitting; though I sit more often than I stand.

For keeping track of notes and TODOs I use [a fountain pen][pen] and paper. I
find this works much better than using a computer.

## Trivia

* The town I live in has a monument that is [literally just a rock][keigoed].
* Over the course of the first three years I've had a total of 10 different
  managers.
* I once deleted GitLab.com's Production database by accident, only to find out
  all our backups had been broken for a long time. Had I not made some database
  snapshots six hours before, GitLab may not have been around today.
* My squatting record is 110KG. Sadly I have gotten a bit lazy since, so it will
  take me a while to get anywhere close to this record again.

[delivery]: /handbook/engineering/infrastructure/team/delivery/
[keigoed]: https://nl.wikipedia.org/wiki/Kei_van_Hilversum
[aeron]: https://en.wikipedia.org/wiki/Aeron_chair
[pen]: https://www.waterman.com/en/59-carene
[ergodox]: https://ergodox-ez.com/
[mx-ergo]: https://www.logitech.com/en-us/product/mx-ergo-wireless-trackball-mouse
[display]: https://www.dell.com/ed/business/p/dell-p2715q-monitor/pd?&l=en
