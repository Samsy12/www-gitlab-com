---
layout: handbook-page-toc
title: "Security Research"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## OKRs

The OKRs for the security research team should fall into the two following categories in order to align with the [role description](/handbook/engineering/security/#security-research):

* Secure the product
* Raise awareness

### Secure the product

The Security Research team is part of the Security Engineering & Research sub-department. In order to secure GitLab the product, and in addition to this also protect
GitLab the company, the security research team will conduct research in areas
such as:

* GitLab's own codes bases
* Dependencies of the GitLab code base
* Software, hardware and services used by the company

The specific topic is chosen individually per quarter to set as an OKR. As the
security research OKRs are twofold any research steps and outcomes should be
documented in detail such that the other OKR category “raise awareness” can
succeed based on the actual research steps.

### Raise awareness

Typically “raising awareness” is a matter of presenting concrete research
results in a suitable way. This might include:

* Handbook updates
* GitLab documentation updates
* Blog posts
* Conference presentations
* Papers or other forms of write ups

Even if the outcome of the research is not yielding any spectacular
vulnerabilities the approaches and procedures conducted during the research
phase should be documented and published. Depending on the impact or importance
of the published research the [external communication team](/handbook/engineering/security/#security-external-communications) might be involved
within the publication.

## Bug bounties and speaker fees

As a result of Security Research it might occur that a team member gets offered
a bug bounty for some reported vulnerability which has been identified during
their working hours at GitLab. In such a case the payment of the bounty should
be instead donated to a charity by the vendor.

The same should be done for potential speaker fees which might be offered for
conference presentations which are held on behalf of GitLab. 

Any kind of bug bounties/fees being offered based on privately conducted
research and speaking engagements are of course not required to be donated to
charity.
