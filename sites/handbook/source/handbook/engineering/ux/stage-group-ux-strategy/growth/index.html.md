---
layout: handbook-page-toc
title: Growth UX Team
description: "The Growth UX team helps create experiences that unlock additional product value to increase usage and paying customers."
---
## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview

The Growth section is made up of the Product Intelligence, Adoption, Activation, Conversion and Expansion groups. The Growth section doesn't own a product. Instead, they unlock additional value that the product provides to increase usage and paying customers. GitLab believes that **everyone can contribute**, and this is central to our strategy.

- [Growth Direction Pages](https://about.gitlab.com/direction/growth/)
- [Growth Engineering Pages](/handbook/engineering/development/growth/)
- [Growth Product Page](https://about.gitlab.com/handbook/product/growth/)

## UX team members

- [Jacki Bauer](/company/team/#jackib) ([Jacki's ReadMe](https://gitlab.com/jackib/jacki-bauer/blob/master/README.md)) - UX Manager
- [Matej Latin](/company/team/#matejlatin) ([Matej's ReadMe](https://gitlab.com/matejlatin/focus/blob/master/README.md)) - Senior Product Designer, Adoption and Expansion.
- [Kevin Comoli](/company/team/#kcomoli) - Product Designer, Activation and Conversion.
- [Jeff Crow](/company/team/#jeffcrow) - Senior UX Researcher, Growth

Not sure which designer to talk to about a particular issue? Create your issue and tag the UX Manager. You can also reach out to the Growth Slack channel (#s_growth) or mention the product designers in issues @gitlab-com/gitlab-ux/growth-ux.

## How We Work

We follow the [Product Designer workflows](/handbook/engineering/ux/ux-designer/) and [UX Researcher workflows](/handbook/engineering/ux/ux-research/) described in the [UX section](/handbook/engineering/ux/) of the handbook. As Growth designers, we relentlessy measure the impact of our design changes following the [experimentation workflow](https://about.gitlab.com/handbook/engineering/development/growth/#running-experiments). In addition:

- we have issue boards so we can see what everyone is up to.
    - [by group](https://gitlab.com/groups/gitlab-org/-/boards/1334665?&label_name%5B%5D=UX&label_name%5B%5D=devops%3A%3Agrowth)
    - [by workflow](https://gitlab.com/groups/gitlab-org/-/boards/1346572)
- we **label** our issues with `UX`, `devops::growth` and `group::`.
- we use the [workflow labels](https://gitlab.com/groups/gitlab-org/-/labels?utf8=%E2%9C%93&subscribed=&search=workflow%3A%3A) for regular issues and [experiment workflow labels](/handbook/engineering/development/growth/#experiment-workflow-labels) for experiment issues.
- we use **milestones** to aid in planning and prioritizing the four growth groups of Acquisition, Conversion, Expansion and Retention.
    - PMs provide an [ICE score for experiments](https://docs.google.com/spreadsheets/d/1yvLW0qM0FpvcBzvtnyFrH6O5kAlV1TEFn0TB8KM-Y1s/edit#gid=0) and by using [priority labels](https://docs.gitlab.com/ee/development/contributing/issue_workflow.html#priority-labels) for other issues.
    - The Product Designer applies the milestone in which they plan to deliver the work (1-2 milestones in advance, or backlog for items that are several months out. For example, if an issue is not doable for a designer in the current milestone, they can add the next milestone to the issue, which will communicate to the PM when the work will be delivered.
    - If the PM has any concern about the planned milestone, they will discuss trade-offs with the Product Designer and other Growth PMs.
- we use [UX issue weights](https://about.gitlab.com/handbook/engineering/ux/ux-designer/#ux-issue-weights) in order to better estimate capacity, realistically break down our work, and give PMs a little insight into how much work we can take on in a milestone.
    - Label the issue for UX work with `UX` and assess the issue weight. Issues larger than an **8** should be broken down further.
    - When the UX work is ready to be transitioned into Engineering, apply the workflow lable `workflow::planning breakdown`.
    - The Engineering team can create a new issue or issues with the broken down work, and apply issue weights. These issues should be labeled `Engineering`.
    - Label issues with `UX` or `Engineering` but not both. This will make sure there is no confusion as to what the weight is for.

#### UX Definition of Done (DoD)

Together with the Product Manager, the Product Designer applies the DoD to epics in order to better break down design work and give counterparts better insight into which steps in the design workflow need to be completed before the MVC can move to the development phase.

In addition to the [Validation Phase Outcomes](https://about.gitlab.com/handbook/product-development-flow/#validation-goals--outcomes) listed in the Product Development Flow, we also make sure that:

- [ ] For an experiment, experiment issues have been created with a hypothesis and experiment plan, and experiment labels have been applied
- [ ] Cross-team dependencies and have been identified and those teams notified and their feedback received
- [ ] Prototypes or mocks for possible solution(s) have been completed and the selected solution is clearly identified
- [ ] Previous versions and discussions of the solution are clearly labeled as such, and ideally are moved out of issue descriptions
- [ ] If changes involve copy, ~"Technical Writing" and ~"UI text" labels have been added
- [ ] Design spec has been added to the designs, including redlines and annotations regarding any non-obvious interactions or design patterns that aren't documented in Pajamas
- [ ] Follow up issues have been created in the design system project for any new design patterns or documentation updates 
- [ ] UX issues are closed and the SSOT (epic) is updated
- [ ] The MVC engineering issue has been updated and labeled ~"workflow::planning breakdown"

#### Collaborating with Other Stages
Since Growth works on a lot of different parts of the app, we will work on user flows and design new patterns that overlap with the work other product designers are doing in their stage groups. For example, our current focus will result in overlap with Create and Verify.

To promote good collaboration and decrease the risk of duplicate or conflicting work, the Growth UX team will use this process for issues that overlap with other stage groups:
- The Growth Product Designer will mention the stage group Product Designer when starting work on a design issue for awareness and to gather background information such as existing constraints, usability problems and existing research. (Growth PMs will communicate in a similar way to Stage PMs)
- The Growth Product Designer will add the stage group Product Designer to design reviews to get feedback and ensure consistency. Design reviews can be sync or async.
- The Growth Product Designer will establish foundations for a good collaborative relationship with Product Designers from other stages by either one or both of the following:
  - Scheduling a recurring monthly sync call.
  - Creating a document for async catching up ([template](https://docs.google.com/document/d/1r83O5AAo_o46ics-BF6NV_eIr1yR7KkKCarfc8kbFgM/edit?usp=sharing)) and a calendar event to remind everyone involved to share what they’re working on. 
- UX Researchers should share research results with stage groups whenever that research is relevant cross-stage.

#### Sharing Designers Across Stage Groups

Two Growth group Product Managers share one Product Designer. To keep things simple and clear, we follow [GitLab internal communication](/handbook/communication/internal-communications) guidelines. In addition the following tips will make it easier to collaborate with Product Designers who span multiple groups:

- Overcommunication is better than undercommunication, especially in this case.
    - Communicate priorities clearly and transparently. Communicate UX priorities via priority labels (ie ~"milestone::p1"), due dates and issue boards instead of in 1:1 docs or Slack channels. The PMs from both covered stage groups, all the product designers and the UX manager need to be able to access those priorities.
    - Feel free to ping designers in more than one channel (GitLab + Slack + email). It can be easy to miss something when covering multiple stage groups.
- On our monthly planning issues, each designer should indicate high priority or time consuming issues from other groups. A link to the other planning issue is fine too, just so it's easy for the UX Manager and Product Managers to navigate to this information.
- Minimize meetings: Designers who span stage groups have 2-3 times more meetings than a designer on 1 stage group, so their teams can help them by ensuring we follow GitLab's async meeting practices to allow them to miss those meetings without negative consequences.

#### Communicating Due Dates

The order in which we approach our work can be complex, as we have priorities and severities, milestone plans, and the product design team has their own [guidance on selecting the next thing to work on](/handbook/engineering/ux/ux-designer/#priority-for-ux-issues). Additionally, some UX issues need to be delivered earlier in the milestone than others. To help with communication you can use the Due Date field. As a PM, if you choose this route, please do the following:

- Continue using priority labels and issue weights. The due date isn't a substitution for these.
- Add an issue comment and suggest the due date. Mention the product designer.
- The product designer should respond within one day as to whether or not the due date is doable. If the due date works, the product designer will add the Due Date. If it doesn't work, they will offer trade-offs for the PM to consider.
- If the due date changes, comment in the issue and @mention people who will be impacted. Note that Due Date changes do not result in a notification.
- When changes are communicated via Slack or in a 1:1, update the issue so everyone else can see the change. Consider sharing in sync meetings.

#### Visual Reviews of MRs

The engineering team applies the `UX` label to any MR that introduces a visual, interaction or flow change. These MRs can be related to new issues, bugs, followups, or any type of MR. If the engineer isn't sure whether the MR needs UX, they should consult the designer who worked on the related issue, and/or the designer assigned to that stage group, or the UX manager.

Visual reviews are required for any MR with the `UX` label. When the MR is in `workflow::In review`, the engineer assigns the MR to the designer for a visual review. This can happen in parallel with the maintainer review, but designers should prioritize these reviews to complete them as quickly as possible.

There are times when it isn't possible or practical for a designer to complete their visual review via Review Apps or GDK. At these times the designer and engineer should coordinate a demo.

## Themes and Focus Areas

In Q4, in addition to KPIs, we have two additional focus areas. These are: 

- **First Mile:** Adoption and Expansion groups will work on this, with Adoption leading. These improvements address a focused experience for a brand new user, from sign-up to the first few important tasks in GitLab.com.
- **Continuous Onboarding:** The Activation and Conversion groups will work on this, with Conversion leading. These improvements address an improved onboarding experience for a user's first few weeks and months with GitLab.

As part of these initiatives, all Growth groups will work on improving the relevant empty states pages. 

We use labels to track UX improvements across a few themes. This allows us to track our work more holistically against big areas we've identified for UX improvement.


For Growth, these themes/labels are:

- [Easy to Trial GitLab](https://gitlab.com/groups/gitlab-org/-/issues?label_name%5B%5D=UX+Team%3A++Easy+to+Trial+GitLab+Theme)
- [Feature Discoverability](https://gitlab.com/groups/gitlab-org/-/issues?label_name%5B%5D=UX+Team%3A++Feature+Discoverability+Theme)
- [Onboarding New Users](https://gitlab.com/groups/gitlab-org/-/issues?label_name%5B%5D=UX+Team%3A+Easy+Onboarding+Theme)
- [Easy Upgrade Flows](https://gitlab.com/groups/gitlab-org/-/issues?label_name%5B%5D=UX+Team%3A+Easy+Upgrades+Theme)


## UX Scorecards

All of the planned, in progress and completed UX Scorecards for Growth can be found in this [epic](https://gitlab.com/groups/gitlab-org/-/epics/2015).
For more information, read about [UX Scorecards](/handbook/engineering/ux/ux-scorecards/).

##### Scorecard: Renew a GitLab Plan

- Job Description: _TBD: When (situation), I want to (motivation), so I can (expected outcome)._
- UX scorecard issue (with walkthrough and video): [113](https://gitlab.com/gitlab-org/growth/product/issues/114) (WIP)
- UX scorecard Score: TBD
- Recommendations Epic: TBD

##### Scorecard: Start a GitLab trial

- Job Description: _When creating a new account, I want to start a trial, So I can test GitLab.com Gold features._
- UX scorecard Epic (with walkthrough and video): [1332](https://gitlab.com/groups/gitlab-org/-/epics/1332)
- UX scorecard Issue: [1355](https://gitlab.com/groups/gitlab-org/-/epics/1355)
- UX scorecard Score: D- (Q2, 2019)
- Recommendations: [1356](https://gitlab.com/groups/gitlab-org/-/epics/1356)

##### Scorecard: User onboarding

- Job description: _When I sign up for a GitLab account, I want to see what are its main features and benefits for my role/team, so I can find out if it’s potentially valuable to our company._
- UX scorecard issue (with walkthrough and video): [170](https://gitlab.com/gitlab-org/growth/product/issues/170)
- UX scorecard Score: C- (Q4, 2019)
- [Recommendations Epic](https://gitlab.com/groups/gitlab-org/growth/-/epics/4)

##### Scorecard: Upgrade a GitLab Plan

- Job Description: _When I realise my team needs a feature from a higher tier than our current paid one, I want to quickly and easily upgrade (to that tier), so that they can benefit from it as soon as possible._
- UX scorecard issue (with walkthrough and video): [113](https://gitlab.com/gitlab-org/growth/product/issues/113)
- UX scorecard Score: C- (Q4, 2019)
- [Recommendations Epic](https://gitlab.com/groups/gitlab-org/growth/-/epics/22)

##### Scorecard: Buy add-on CI minutes

- Job Description: _When I realise that we're running out of CI minutes, I want to quickly and easily buy more, so that our team can continue building and delivering software uninterrupted._
- UX scorecard issue (with walkthrough and video): [531](https://gitlab.com/gitlab-org/gitlab-design/issues/531)
- UX scorecard Score: C (Q4, 2019)
- [Recommendations Epic](https://gitlab.com/groups/gitlab-org/growth/-/epics/26)

##### Scorecard: Upgrade a GitLab.com subscription tier

- Job Description: _When I realise my team needs a feature from a higher plan than our current paid one, I want to quickly and easily upgrade (to that plan), so that they can benefit from it as soon as possible._
- UX scorecard issue (with walkthrough and video): [947](https://gitlab.com/gitlab-org/gitlab-design/-/issues/947)
- UX scorecard Score: C (Q1, 2020)
- [Recommendations Epic](https://gitlab.com/groups/gitlab-org/growth/-/epics/32)

<!--
## Log of major changes

This is a log of major changes introduced by the Growth UX team as part of their work with the [Growth subgroups](/handbook/engineering/development/growth/). It serves as an easy way to track down when and why a major change to a user experience was introduced.

We define "major changes" as:

- Major layout/IA changes on a single screen
- User experience changes across multiple screens (flow changes)
- Increasing/decreasing the number of interactions it takes to complete a task
- Changing the content of something at a critical step for the users (a banner warning for example)
- Changing when and where something that is triggered by the system happens (when and where a banner shows up and what triggers it)

### Introduced the new, simpler free trial signup flow

Feb 18, 2020, [Epic](https://gitlab.com/groups/gitlab-org/-/epics/377) - _Released in 12.4_

Last year we introduced a simpler free trial sign up in which a user could complete the process by interacting with one app only. Before, they had to create a separate account in the CustomersDot app which often led to confusion.

### Provide more context and guidance for true-ups in the renewal flow

Jan 30, 2020 - [Issue](https://gitlab.com/gitlab-org/growth/engineering/issues/40) - _MVC expected in 12.8_

Users didn’t know what number to put into the _Users over license_ field in the renewal flow which resulted in new licenses that threw errors. They also didn’t know what number to put into the _Users_ field so we renamed it so it aligns with the data and labels in the Admin Overview. The MVC will be shipped without the illustrations but is still considered a major improvement. This change as a whole is an intermediate step before we move towards automatically collecting the data.

### Changed the appearance, content and behavior of renewal and auto-renewal banners

Jan 30, 2020 - [Issue 1](https://gitlab.com/gitlab-org/growth/product/issues/102), [Issue 2](https://gitlab.com/gitlab-org/growth/product/issues/143) - _Expected to be delivered in 12.8_

Existing banners were confusing the users because they lacked contextual information. Auto-renewal banners, for example, didn’t make it clear that the subscription will automatically renew. Banners were also non-dismissable and shown to all users instead of just the instance admins. This change introduced a new appearance, new behaviour (who they’re shown to and when) and more contextual content.
-->
 
## A/B test resources  
The Growth UX team is using A/B testing as a solution validation method. Below are resources to better understand what is A/B testing and how it can be implemented in the design process. 

* [Optimizely Glossary - A/B testing](https://www.optimizely.com/nl/optimization-glossary/ab-testing/)
* [How to use A/B testing for better product design](https://andrewchen.co/how-to-use-ab-testing-for-better-product-design/)
* [Define Stronger A/B Test Variations Through UX Research](https://www.nngroup.com/articles/ab-testing-and-ux-research/)
* [Don't A/B Test Yourself Off a Cliff](https://www.youtube.com/watch?v=TYrWpZdcujU)

<!-- Template
### Short, descriptive title for the change

Timestamp (MMM DD, YYYY) - [Issue](link_to_issue) - *When was it delivered (milestone)*

Short description of the change and why it was made. 
-->
