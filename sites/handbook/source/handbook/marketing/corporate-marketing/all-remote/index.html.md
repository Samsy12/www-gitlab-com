---
layout: handbook-page-toc
title: "All-Remote Marketing Handbook"
description: Strategy, Workflows, and Vision for All-Remote Marketing at GitLab
twitter_image: "/images/opengraph/all-remote.jpg"
twitter_image_alt: "GitLab's All-Remote Marketing branded image"
twitter_site: "@gitlab"
twitter_creator: "@gitlab"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Welcome to the All-Remote Marketing Handbook

![GitLab all-remote team](/images/all-remote/gitlab-com-all-remote-1280x270.png){: .shadow.medium.center}

The all-remote marketing team is responsible for the creation, curation, and continued iteration of [GitLab's guide to all-remote](/company/culture/all-remote/guide/), a deep library of guides that covers every facet of how GitLab functions as a remote team. 

**This page is the single source of truth for all-remote positioning, evangelism, approvals, vision, and strategy**. 

## Mission Statement

<!-- blank line -->
<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/GKMUs7WXm-E" frameborder="0" allowfullscreen="true"> </iframe>
</figure>
<!-- blank line -->

The mission of GitLab’s [All-Remote Marketing](/job-families/marketing/all-remote-marketing/) team is to champion the company’s [all-remote culture](/company/culture/all-remote/) and initiatives. 

This involves close collaboration with corporate marketing (PR, corporate events), people group ([employment branding](/handbook/people-group/employment-branding/)) and [Diversity, Inclusion & Belonging ](/company/culture/inclusion/).

## Vision

GitLab is an influencer and educator in remote work. It serves the community by creating valuable content that furthers the proliferation and ubiquity of [remote-first](/company/culture/all-remote/how-to-work-remote-first/) and [all-remote](/company/culture/all-remote/terminology/) organizations, while enhancing the operations of colocated and [hybid-remote](/company/culture/all-remote/hybrid-remote/) companies by sharing implementable remote-first practices. 

We believe that the remote principles relied on by GitLab are applicable even to colocated companies, and educating on pillars such as [asynchronous workflows](/company/culture/all-remote/asynchronous/) and [informal communication](/company/culture/all-remote/informal-communication/) can benefit all organizations.

## Evangelism materials

![GitLab all-remote team](/images/all-remote/gitlab-com-all-remote-v3-dark-1280x270.png){: .shadow.medium.center}

If you are asked by peers for more information on how GitLab thrives as an [all-remote](/company/culture/all-remote/terminology/) team, please see the below.

### GitLab's Guide to Remote Work

If you're wondering where to start, direct people to a blog post — [Resources for companies embracing remote work](/blog/2020/03/06/resources-for-companies-embracing-remote-work/) — which gives background and context, and lays out a logical flow of links for leaders and workers to follow as they learn. 

We've built The Remote Playbook, a curated eBook with GitLab's top advice for companies transitioning to remote. This can be downloaded via a prompt on the all-remote homepage, accessible at [http://allremote.info/](http://allremote.info/)

In scenarios where you need a quick link to vocalize, tweet, email, or otherwise share, we have established a memorable redirect: http://allremote.info/ ("*All Remote Dot Info*")

### Why remote?

GitLab's overview video on its [all-remote culture](/company/culture/all-remote/vision/) can be [viewed here](https://youtu.be/GKMUs7WXm-E). 

### Presentations (slide deck)

You may be asked to give a presentation on how GitLab works as an all-remote team, including requests that are specific to your role (sales, engineering, finance, people, etc.). 

In these instances, you're welcome to use [this Google Slides presentation](https://docs.google.com/presentation/d/1BdfCVmjqEKiKgvSAElha82_CnHkQT6lSPClGEaKjXn4/edit?usp=sharing), `Thriving as a Remote Team: A foundational toolkit`. (This link is only viewable by GitLab team members.) A [suggested script](https://docs.google.com/document/d/16viV_819RhRtCb_SOJami3ctw7gcZoBMzT8ICIU_wX0/edit?usp=sharing) following this presentation is available for your use.

For a video of GitLab's Head of Remote walking through this presentation to a group of founders, [click here](https://youtu.be/MVTvm4awuJ0). This presentation will serve as a guide to narrating the slides and connecting GitLab's approach to remote with the current reality of your audience. 

Other slide decks are below.
* [GitLab remote work starter guide — adjusting to work-from-home for workers](https://docs.google.com/presentation/d/1n6y8sEUKGLdRuPAVN3cTBI3XnKPY_mFnEAi6E08f4qI/edit?usp=sharing) with [suggested script](https://docs.google.com/document/d/1vUJ_Lks1O2kZuekPQMhadd2LjynH1n7b87A2FJmfcrA/edit?usp=sharing)
* [GitLab remote work company emergency plan — what to do and where to start for team leaders](https://docs.google.com/presentation/d/1XC0eysmuImfvdTy81IVaZ98mRq_rX2IGX3j_CISWc04/edit#slide=id.g8164dec4ba_0_215) with [suggested script](https://docs.google.com/document/d/1RgHTZ_eDEQrcMA_w8mkuxhjjslFH5gnGwbzccXX2S2Q/edit?usp=sharing)
* [Implementing and reinforcing remote-first practices — shared at GitLab Culture Open House 2020](https://drive.google.com/file/d/1-hYeifS-Z10iUzp_17QP1UxOxGXVPARk/view)

**Usage guidelines**:

1. Please make a copy of the presentation and script instead of overwriting the template.
2. Swap out existing names/headshots for your own.
3. Feel welcome to add a slide or two related to the specifics of the request. 

### Teaching other companies how to go remote

GitLab is unique in that every single team member is remote. All-remote is the common thread that we all share, regardless of what department we serve. This means that each GitLab team member is uniquely equipped to share best practices with other leaders and companies. 

The requests may be varied, from small firms looking to [unwind](https://youtu.be/MSj6-wC4f9w) their office strategy and go [all-remote](/company/culture/all-remote/terminology/), to multi-nationals which are looking to implement [remote-first](/company/culture/all-remote/how-to-work-remote-first/) best practices to account for more of their team working outside of the office (or in different offices). 

### Top questions from suddenly or newly-remote companies

It is useful to empathize with those you speak with. GitLab is a very advanced remote work environment, making it all the more important for team members to understand alternative baselines and speak to other realities, all while forecasting what's possible if an organization invests in remote-first principles today. 

Below are the most common questions asked by suddenly or newly-remote companies, linked to relevant handbook pages that you can study prior to presenting. These shed light on their challenges, and will help you proactively speak to common needs, misconceptions, and struggles. 

1. How do you [maintain and build company culture](/company/culture/all-remote/building-culture/) in a remote work environment? 
1. How do we [maintain and build new work relationships](/company/culture/all-remote/informal-communication/) without seeing each other in-person on a regular basis?
1. How do we [prevent burnout, isolation, and mental health crises](/company/culture/all-remote/mental-health/)?
1. How do we [combat Zoom fatigue](/company/culture/all-remote/meetings/) (e.g. exhaustion associated with nonstop videocalls)?
1. How we do handle [compensation changes](/handbook/total-rewards/compensation/) if people permanently relocate to work remotely?
1. How do we ensure that [employees are productive](/handbook/leadership/) when we cannot physically see them?
1. How does one [become a great remote manager](/company/culture/all-remote/being-a-great-remote-manager/)?
1. How do you [effectively lead remote teams](https://www.coursera.org/learn/remote-team-management)?
1. How do you [onboarding](/company/culture/all-remote/onboarding/) and [train/educate](train/educate) remotely?
1. How do you [collaborate and whiteboard](/company/culture/all-remote/collaboration-and-whiteboarding/) remotely?

### 'How to Manage a Remote Team' course on Coursera 

Mention in panels and consultations that GitLab's expertise in managing a remote team can be digested as a free course on Coursera. 

The course, titled "[How to Manage a Remote Team](https://www.coursera.org/learn/remote-team-management)," provides a holistic, in-depth analysis of remote team structures, phases of adaptation, and best practices for managers, leaders, and human resources professionals. It is being offered free of charge, with an optional paid certificate available.

### Foundational presentation areas

Regardless of the nuance in the request, here are the foundational areas that should be covered. Be sure to describe how GitLab implements these tactics using [low-context communication](/company/culture/all-remote/effective-communication/#understanding-low-context-communication), leaning on examples and detail such that a non-GitLab company can envision how such a tactic could be useful in their own organization. 

1. **Forced work-from-home isn't remote work**. In speaking with suddenly or newly-remote companies, preface everything with the following sentiment: Forced work-from-home, triggered by external factors such as a global pandemic, is not the same as *intentionally designed remote work*. It is vital to disentangle the two concepts. The stress one feels from working at home with no preparation — possibly with a significant other also working from home while juggling childcare or virtual schooling — is more closely linked to life during a pandemic than it is remote work.
1. **Set the stage**. GitLab is the [world's largest all-remote company](/company/history/#how-did-gitlab-become-an-all-remote-company), which is why our advice matters.
1. **Remote requires a different mindset**. All the tools and processes in the world will falter if leadership doesn't lead with **trust and transparency** rather than micromanagement and fear.
1. **Lead with data**. GitLab's [Remote Work Report](/remote-work-report/) surveys thousands of global remote workers. As leaders and team members grapple with going remote, this report provides insights on what matters to those who adopt this way of working, charting a path for building culture around autonomy and flexibility. 
1. **Remote-first practices aren't just for remote companies.** Empathize with [challenges](/company/culture/all-remote/how-to-work-remote-first/). Offer up common issues that teams who are going remote will face. Although GitLab is [all-remote](/company/culture/all-remote/terminology/), we should make clear that our advice applies to colocated companies and [hybrid-remote](/company/culture/all-remote/hybrid-remote/) companies as well. 
1. **How do I manage a remote team?**
   * What tools do we need?
   * How much communication is too much?
   * What is in charge of our remote transition?
1. **Guidance is about the here-and-now, but approach this for the long-term.**
   * Recognize that companies forced into work-from-home need communication gaps filled *now*.
   * The reason to do this with intention is that it will become a core part of a company's talent and operational strategy.
   * Remote de-risks a company, making it less susceptiable to socioeconomic swings and crises. 
1. **Three biggest challenges!**
   * Workspace challenges and work/life separation.
   * Communications in a remote world, and keeping everyone engaged/informed.
   * Mindset and culture, leaning into the reality that change takes time, and a focus on iteration. 
1. **How does GitLab do it?!**
   * This is your moment to showcase specific examples of how GitLab does things differently. If you are addressing an department-specific audience (sales, engineering, HR, finance, etc.), surface examples germane to that audience.
   * Feel welcome to share that [GitLab uses GitLab](/solutions/gitlab-for-remote/). It's a tool built by an all-remote team to enable remote practices such as asynchronous workflows and prevent typical challenges such as communication silos.
   * Share our [GitLab for remote teams solutions page](/solutions/gitlab-for-remote/).
   * Prepare for minds to be blown. These things feel like second nature to GitLab team members, but are revolutionary to most. 
1. **You must have a single source of truth**
   * It's not about blanket documentation. It's about working [handbook-first](/company/culture/all-remote/handbook-first-documentation/). 
   * Start now! Designate a scribe if you have to. Start small, as an FAQ, and build it out. 
   * Show an example of a handbook page — a great example is our [Communication page](/handbook/communication/).
1. **Asynchronous over synchronous** 
   * Explain how GitLab requires each meeting to have an agenda and someone [documenting](/company/culture/all-remote/meetings/#document-everything-live-yes-everything).
   * Explain how meeting takeaways then need to be contextualized and [added to relevant handbook pages](/company/culture/all-remote/self-service/#paying-it-forward).
   * Explain how this added burden on meeting is a [forcing-function](/company/culture/all-remote/how-to-work-remote-first/) to work first in GitLab, and rely on a meeting as a last resort. 
1. **"OK, but where do we start?"**
   * Start small, don't be overwhelmed. Get your executives out of the office and have EA's document the communication gaps that emerge. (Hint: *That's your priority list of what voids to fill*.)
   * Establish a team responsible for communication. Everything that comes next requires clear, frequent, transparent communication and an understanding of where to communicate and who are the DRIs for various functions.
   * Provide a feedback mechanism. [It's impossible to know everything](/handbook/values/#its-impossible-to-know-everything), so ask your team members what's missing in their remote approach. Prioritize those asks as you see themes forming. 
   * Remind people that GitLab has two Getting Started guides: one for [leaders/companies](/company/culture/all-remote/remote-work-emergency-plan/), another for [workers](/company/culture/all-remote/remote-work-starter-guide/). 
1. **Minimize your tool Stack**. The fewer moving pieces when transitioning to remote, the better. GitLab (the product) is at the heart of our workflows.
1. **"But wait, I still need help!"** Fret not! GitLab's entire library of remote guides are available at http://allremote.info/ ("*All Remote Dot Info*")

In case you as a subject matter expert are invited to write an Unfiltered blog post or create other written content, please feel free to make a copy of the ["Going remote in ____" blog post template](https://docs.google.com/document/d/199fWehOHX2tPUZgOLDIJUSjpTUOezqQXCtIFXo2uEng/edit?usp=sharing) and tailor based on your audience.

### GitLab All-Remote Certification

Anyone in the world (yes, this includes those who are not employed by GitLab) may take the [GitLab All-Remote Certification](/company/culture/all-remote/remote-certification/) to improve their remote fluency.

### How do I talk about remote?

If you're looking for examples of the GitLab team describing our experience with remote work, have a listen at the podcasts below.

* SafetyWing Podcast — [How to transition to remote](http://buildingremotely.com/episode/2)
* The Recruitment Network Podcast — [The Future of Remote Working with Darren Murph](https://open.spotify.com/show/5Z3HMCHlBOLuYUWvczJjxj?si=giLfsfd-TZOB1hGkFiNeTw)
* Wise Up with Cristina Podcast - [Darren Murph, Remote Rebel, on Opportunities Beyond Work](https://cristinadigiacomo.com/wise-up-podcast/wise-up-episode14-darrenmurph)
* Customer Centric Podcast — [Darren Murph: home is where the office is](https://customercentric.unbabel.com/podcast/darren-murph-gitlab)
* Changelog Podcast — [Creating GitLab's remote playbook](https://changelog.com/podcast/397)
* OpenFin MVP Podcast — [All-remote before it was mandatory with GitLab's Brendan O'Leary](https://openfin-mvp.simplecast.com/episodes/all-remote-before-it-was-mandatory-with-gitlabs-brendan-oleary)
* Skills for Mars — [Darren Murph On Transparency, Values & Leadership when All-Remote](https://youtu.be/g78mzVYjDY0)
* Bright & Early Podcast — [Darren Murph: Remote Work at GitLab](https://www.brightandearlypodcast.com/33)
* Working Without Borders: The Get on Board Podcast — [Darren Murph on leading a remote culture at GitLab](https://medium.com/getonbrd/working-without-borders-s01e02-darren-murph-head-of-remote-at-gitlab-5567d7c634fd)
* Outside The Valley: [Darren Murph of GitLab - Why Companies Should Go All-Remote](https://arc.dev/blog/podcast-ep23-gitlab-darren-murph-96ggw37q6t)
* Accelerating Support Podcast: [Darren Murph, Head of Remote at GitLab](https://soundcloud.com/acceleratingsupport/accelerating-support-export-v1/s-JylGQhxc7il)

### Social media assets and guidelines

[This is the issue](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/1973) to get everything you need in order to evangelize remote work on your social media accounts. It includes:

* Goals
* Perspective
* Hashtags to use
* Topics to follow
* Tips for writing your own posts
* FAQ

## How does a company create their own handbook?

![GitLab all-remote team](/images/all-remote/gitlab-com-all-remote-v4-dark-1280x270.png){: .shadow.medium.center}

Learn more in GitLab's [Handbook-First Documentation guide](/company/culture/all-remote/handbook-first-documentation/#tools-for-building-a-handbook) about how GitLab (the company) uses GitLab (the product) to build and maintain its handbook, as well as tools and tips for other companies who wish to start their own.

## All-remote guide creation

GitLab's growing [library of remote guides](/company/culture/all-remote/guide/) is designed to be bolstered by new pages. Below is an overview of the process for adding a new guide.

1. Check [this GitLab Issue](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/2131) to ensure that your proposed guide isn't already being scheduled
2. If it's a net-new idea, please put each new guide idea/topic in a new issue within [Corporate Marketing](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/)
3. Put `Proposal: [NEW ALL-REMOTE GUIDE]` as the subject
4. Add the label `mktg-status::triage`
5. Assign to `@dmurph` to evaluate and provide feedback

### What's the difference between an all-remote guide and a traditional GitLab handbook page?

See below for an A/B comparison of how an inward-facing GitLab *handbook* page is written vs. an external-facing *all-remote guide* is written.

- [Handbook Hiring Page](/handbook/hiring/) and [Handbook Learning & Development Page](/handbook/people-group/learning-and-development/) (The audience is clearly GitLab, serving as an internal process guide for team members to follow.)
- [All-Remote Hiring Guide](/company/culture/all-remote/hiring/) and [All-Remote Learning & Development Guide](/company/culture/all-remote/learning-and-development/) (The audience is external readers, written as an instructive guide for external companies.)

## Design and illustration assets

![GitLab all-remote team](/images/all-remote/GitLab-All-Remote-Zoom-Team-Tanuki.jpg){: .shadow.medium.center}

GitLab's [Brand and Digital Design](/handbook/marketing/inbound-marketing/digital-experience/) team are building out images and illustrations to visualize all-remote. 

The Google Drive repository is [here](https://drive.google.com/open?id=1_m0Gg8DjJPo1n5b3IRYl0fB48UyeIX_e), which is home to all-remote photography, presentation decks, animations, banners, illustrations, iconography, e-books, social graphics, and more. (*Note: The Google Drive folder is private to those within the GitLab organization.*)

## Approvals

Any GitLab team member is welcome to offer 1:1 advice or consultations on remote work. If you're asked to give a broader presentation or webinar to a group, private or public, please create a new issue in Corporate Marketing per the instructions in [How To Contribute](/handbook/marketing/corporate-marketing/#how-to-contribute) with details of the request. 

An example of these details in an issue can be found [here](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/2177).

Once created, please tag `@jessicareeder` in a comment with a note that includes `Seeking Approval`. 

The all-remote team will be available to help direct if you feel unprepared, or pair the creator of the issue with someone else on the GitLab team if there's opportunity to add another layer of expertise (e.g. a DevOps expert, an HR expert, a Finance expert) depending on the company that's requesting.

## Other assets

### GitLab Remote Work Report

GitLab's [Remote Work Report](/remote-work-report/) sheds light on the current reality of remote work during a critical time in its global adoption. As leaders and team members grapple with going remote, this report provides insights on what matters to those who adopt this way of working, charting a path for building culture around autonomy and flexibility. 

For example, 86% of respondents believe remote work is the future and 62% of respondents said that they would consider leaving a co-located company for a remote role. Contrary to popular belief, we found that most remote workers aren't digital nomads, and 52% are actually likely to travel less than their office working counterparts.

### Remote Work playlist on GitLab Unfiltered

GitLab is a very [transparent](/handbook/values/#transparency) company. As such, our AMAs, webinars, and other conversations with team members and other companies are uploaded to a dedicated [Remote Work playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0Kq7QUX-Ux5fOunQotqJbECc) on the GitLab Unfiltered YouTube channel.

### Universal Remote webcast playlist on GitLab YouTube channel

[Universal Remote](https://www.youtube.com/playlist?list=PLFGfElNsQthay5Dd5OUC9DsNiIl7tzhMW) is GitLab's weekly web show focused on helping teams transition to a fully remote world. The running playlist of episodes can be found on [GitLab's YouTube channel](https://www.youtube.com/playlist?list=PLFGfElNsQthay5Dd5OUC9DsNiIl7tzhMW). 

### All-Remote on the GitLab blog

* GitLab blog posts [in the `Culture` category](/blog/categories/culture/)
* GitLab blog posts [tagged `remote-work`](/blog/tags.html#remote-work) 

### All-Remote Initiatives Parent Epic

This [parent epic](https://gitlab.com/groups/gitlab-com/marketing/-/epics/334) houses all corporate marketing campaigns, projects, child epics, and issues related to all-remote initiatives. 

## Our audience

The audience we aim to reach with our all-remote initiatives is both internal and external to GitLab. It closely aligns with our employment branding audience, and expands to cover key segments in the investor and business communities.

  * Venture capitalists
  * Entrepreneurs
  * Business founders
  * Talent, recruiting, and HR leads
  * Media (business, lifestyle, workplace, finance)
  * Educators and researchers
  * GitLab team members
  * Job candidates and future team members
  * The broader GitLab community
  * People interested in remote work
  * Executives, Managers and HR that are finding a need for resources to support a remote workforce
  * Educators suddenly needing to support remote teaching
  * Industry analysts
  * People suddenly working remotely 

## Key Messages for All-Remote

![GitLab all-remote computer](/images/all-remote/GitLab-All-Remote-Learning-Computer.jpg){: .shadow.medium.center}

### GitLab: The Remote Strategy

* GitLab is an [all-remote](/company/culture/all-remote/terminology/) company. Hiring managers are able to find candidates not limited to tech hubs like San Francisco, New York or Boston. 
* When you can hire around the world, you can pay market wages and offer people an at-market or above-market wage while still reducing costs for the company. 
* Without office rent, an organization [saves](/company/culture/all-remote/benefits/) a significant amount of money. GitLab, for example, has experienced rapid growth and would've had to move offices seven times in the last few years. We save a significant amount of money on rent, utilities, office equipment, and additional team members to manage the office.
* GitLab has [grown](/company/history/) from 350 employees at the beginning of 2019, to over 1,300 employees across 65+ countries and regions currently.
We chose the all-remote structure so we can hire people irrespective of location and we’re able to find the most talented people in the world rather than within a commutable distance.

### Best practices for managing teams and communications remotely

**Managing your team**

* Prioritize results over hours worked
* Don't require people to have consistent set working hours or say when they're working
* Don't encourage or celebrate working long hours or on weekends
* Encourage teamwork and saying thanks

**Communication**

* Encourage people to write down all information
* Allow everyone in the company to view and edit every document
* Consider every document a draft, don't wait to share until it's done
* Use screenshots in an issue tracker instead of a whiteboard, ensuring that everyone at any time can follow the thought process
* Encourage non-work related communication for relationship building
* Encourage group video calls for bonding
* Encourage one-on-one video calls between people (as part of onboarding)
* Host periodic summits with the whole company to get to know each other in an informal setting

### Connecting to GitLab's values of iteration and transparency

**Iteration**

* We do the smallest thing possible and get it out as quickly as possible. If you make suggestions that can be excluded from the first iteration, turn them into a separate issue that you link. Don't write a large plan, only write the first step. Trust that you'll know better how to proceed after something’s released. You're doing it right if you're slightly embarrassed by the minimal feature set shipped in the first iteration. 
* This value is the one most underestimate when they join GitLab. The impact both on your work process and on how much you achieve is greater than anticipated. In the beginning, it hurts to make decisions fast and to see that things are changed with less consultation. But frequently the simplest version turns out to be the best one.

**Transparency**

* Be open about as many things as possible. By making information public we can reduce the threshold to contribution and make collaboration easier. Use public issue trackers, projects, and repositories when possible.
* An example is the public repository of our website that also contains our company handbook. Everything we do is public by default, for example, the GitLab CE and GitLab EE issue trackers, but also marketing and infrastructure. 
* Transparency creates awareness for GitLab, which allows us to recruit people that care about our values. It gets us more and faster feedback from people outside the company, and makes it easier to collaborate with them. It’s also about sharing great software, documentation, examples, lessons, and processes with the whole community and world in the spirit of open source, which we believe creates more value than it captures.

## Objectives and goals

As detailed in GitLab’s public [CMO OKRs](/company/okrs/), GitLab’s All-Remote Marketing team seeks to elevate the profile of GitLab in the media and investor circles, positioning it as a pioneer of remote work. It will spread the story of GitLab’s global remote employees, remote work processes, transparent culture and the movement to remote work that GitLab has created. It also seeks to position GitLab as an innovator in the eyes of investors, a vital part of GitLab’s [public ambition to become a public company](/company/strategy/).

  * Leverage events to generate business interest and media coverage on GitLab’s all-remote culture
  * Form and foster relationships with other remote companies, creating unity in ramping up mentions and credibility for remote work
  * Position GitLab CEO Sid Sijbrandij as a thought leader in the space, utilizing [interviews, livestreams, podcasts and panels](/company/culture/all-remote/resources/#videos-podcasts-interviews-presentations) to raise visibility
  * Attract new [candidates](/jobs/) that embrace geographic diversity and place a high degree of value on an all-remote culture
  * Maintain and evolve an [all-remote web destination](/company/culture/all-remote/) focused on GitLab’s leadership in remote work culture in the context of the broader movement
  * Work with GitLab team members around the globe, as well as external remote advocates, to highlight remote culture [stories](/company/culture/all-remote/stories/)
  * Employ an ethnographic storytelling approach to document and share authentic, credible stories from the movement offering insights that can be applied to solve problems throughout the organization and also adopted by others outside of GitLab
  * Position GitLab (the product) as a key enabler of remote work
  * Develop strategy for mentoring, advising and consulting within the startup community to foster the creation of more all-remote companies
  * Leverage partners and friendlies in the all-remote space to cross-promote and amplify GitLab’s all-remote messaging across events, web and social media

### Deliverables (OKRs)

- [Q4FY21 All-Remote OKRs (active)](https://gitlab.com/groups/gitlab-com/marketing/-/epics/1644)
- [Q3FY21 All-Remote OKRs](https://gitlab.com/groups/gitlab-com/marketing/-/epics/1312)

## Nurturing all-remote leads

A growing cross-section of audiences — DevOps and remote — is enabling GitLab to nurture interactions which begin on the topic of remote work. While not every individual who engages with GitLab's remote leadership materials will be interested in learning more about project management, collaboration, and/or software development through GitLab ([the product](/solutions/gitlab-for-remote/)), the all-remote team is iterating on a nurture strategy to properly serve those who are. 

The primary nurture tool is [PathFactory](/handbook/marketing/marketing-operations/pathfactory/), which is maintained by colleagues within Marketing Operations. 

An example of how this looks can be seen in an [iteration developed for the Scaleup 360 Event](https://learn.gitlab.com/scaleup360/remote-playbook). This houses a unique PathFactory track specific to the event, which was cloned from a template and customized with relevant information. The track includes key assets — [The Remote Playbook](http://learn.gitlab.com/all-remote/remote-playbook) and [The Guide to Remote DevOps](/resources/ebook-guide-remote-devops/) — along with supplementary materials and guides. 

The DRI ([directly responsible individual](/handbook/people-group/directly-responsible-individuals/)) for all-remote nurture strategy is `@jessicareeder`. 

### Planned iterations

Future adjustments may include:

- Scope down the amount of content surfaced (potentially via an `Explore` page)
- Use the remote form specifically, rather than a more general one
- Implement a follow-up email triggered by individuals filling out the form

### Planned outcomes

Whenever a GitLab team member conducts a talk, session, or presentation on remote work, we can direct audiences to either the main track or a cloned one (for bigger events). This allows us to understand what our audiences are engaging with, and connect our leads to the [main funnel](/handbook/marketing/marketing-operations/#marketing-gearing-ratios).

### Connecting GitLab sellers to individuals 

At times, our remote team members speak with GitLab prospects on joint media panels, interviews, webinars, etc. focused on the topic of remote work. If appropriate, the GitLab team member with the contact should consider introducing the prospect to the GitLab sales member. In order to make this connection to the GitLab seller (if it is not known to the All-Remote team member), the All-Remote team member should open an issue in the [Field Marketing project](https://gitlab.com/gitlab-com/marketing/field-marketing/-/issues/new?issuable_template=) and tag the correct [regional Field Marketing leader](https://gitlab.com/gitlab-com/marketing/field-marketing#fm-managers). 

Field Marketing will look up account ownership in SFDC (Salesforce.com) and make the connection between the GitLab seller and the All-Remote team member so they can establish next steps in connecting to the prospect. The All-Remote team member should also feel comfortable asking about account ownership in the `#fieldmarketing` or `#sales` Slack channels before opening an issue. 

## Channels

![GitLab all-remote illustration](/images/all-remote/gitlab-all-remote-v1-opengraph-social-1200x630.jpg){: .shadow.medium.center}

### Web

The team's primary home for publishing informational guides and content is the [all-remote section of GitLab's handbook](/company/culture/all-remote/). This will be the preeminent home to all-remote content, positioned for consumption by media, investors, prospective customers and candidates.  

### Video

GitLab is a very [transparent](/handbook/values/#transparency) company. As such, our remote-centric AMAs, webinars, and other conversations with team members and other companies are uploaded to a dedicated [Remote Work playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0Kq7QUX-Ux5fOunQotqJbECc) on the GitLab Unfiltered YouTube channel.

### Podcast (audio)

GitLab is [experimenting](/company/culture/all-remote/universal-remote/) with new mediums to share its all-remote messaging. The [Universal Remote podcast](https://open.spotify.com/show/7LwY2YGQWKFHGLMxtpzIcp?autoplay=true) is a foray into this space. (For those who prefer visuals, visit the [Universal Remote YouTube playlist](https://www.youtube.com/playlist?list=PLFGfElNsQthay5Dd5OUC9DsNiIl7tzhMW).)

### Events, panels, keynotes and webinars

All-remote events should elevate GitLab as a thought leader in the remote work space, create new partnerships, generate leads and generate media interest/coverage. We will consider physical events, virtual events and events that combine an in-person presence with a livestream option.

We believe that [all-remote is for everyone](/blog/2019/08/15/all-remote-is-for-everyone/), and that almost every company is [already a remote company](/company/culture/all-remote/scaling/#does-all-remote-work-at-scale). This includes all company sizes, from solo enterprises to multi-nationals, and geographies. Our event strategy should reflect this, offering education, insights, and actionable advice that applies to a wide spectrum of remote companies. 

Events should create an inclusive atmosphere, welcoming and beneficial to those who are not receptive to remote or are working in a company where remote is not feasible/acceptable. 

### Social media

We incorporate all-remote content on GitLab’s [social media](/handbook/marketing/corporate-marketing/social-marketing/) accounts, and are investigating a visual approach to new mediums that are aligned with culture and lifestyle stories.

We are working with employment branding to surface relevant all-remote stories from GitLab team members to recruiting channels and review sites, such as Glassdoor, LinkedIn and Comparably.

There are also a number of videos on GitLab's [YouTube channel](https://www.youtube.com/gitlab) that relate to working here:
- [GitLab Unfiltered Remote Work playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0Kq7QUX-Ux5fOunQotqJbECc)
- [Everyone can contribute](https://youtu.be/V2Z1h_2gLNU)
- [Working remotely at GitLab](https://youtu.be/NoFLJLJ7abE)
- [This is GitLab](https://youtu.be/Mkw1-Uc7V1k)
- [What is GitLab?](https://youtu.be/MqL6BMOySIQ)

## How to contribute (working with us)

To contribute an idea or [proposal](/handbook/values/#make-a-proposal) to further GitLab's all-remote mission:

1. Please put each new idea/topic in a new issue within [Corporate Marketing](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/)
1. Put `Proposal: [IDEA]` as the subject
1. Add the labels `mktg-status::triage` and `All-Remote Team`
1. Assign to `@jessicareeder`
1. Please [set a due date](/handbook/values/#set-a-due-date) using GitLab's `Due Date` feature and provide context for the deadline(s).

### Requesting guidance 

To request guidance on a panel or speaking engagement: 

1. Provide an overview of the opportunity, and whether you or someone else is being requested, in the `#all-remote_action` Slack channel (for remote engagements) or `#marketing` Slack channel (for broader/more general engagements)
1. We will evaluate the opportunity and provide guidance; if we decide to proceed, the participating GitLab team member will be asked to create an issue using [this Corporate Marketing issue template](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/new?issuable_template=remote_consultation_request) to track progress.

### GitLab label overview

- The all-remote marketing team works primarily from the `All-Remote Team` [label](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues?label_name%5B%5D=All-Remote+Team). By applying this label, your issue or merge request will appear in our workflows. Please also tag `@dmurph` and `@jessicareeder` if input, action, or unblocking is requested.
- For work related to remote, but not needing action from the all-remote team, you may apply the `remote-work` [label](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues?label_name%5B%5D=remote-work).
- For issues or merge requests requiring the attention of the Head of Remote to unblock, please also apply the `HOR Attention` [label](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=HOR%20Attention) and tag `@dmurph`.

## Team

### Meet the all-remote team

[**Darren Murph**](/handbook/marketing/readmes/dmurph/)

* Title: Head of Remote
* GitLab handle: `dmurph`
* Slack handle: `dmurph`

[**Jessica Reeder**](/company/team/?department=marketing#jessicareeder)

* Title: All-Remote Integrated Campaign Manager
* GitLab handle: `jessicareeder`
* Slack handle: `jessicareeder`

[**Stefanie Haynes**](/company/team/?department=marketing#shaynes13)

* Title: All-Remote Coordinator and Sr. EBA
* GitLab handle: `shaynes13`
* Slack handle: `shaynes`

### Contact us

- [Slack](https://gitlab.slack.com/app_redirect?channel=all-remote_action)
- [Email the all-remote team](mailto:remote@gitlab.com)

----

Return to the [Corporate Marketing Handbook](/handbook/marketing/corporate-marketing/).
