---
layout: handbook-page-toc
title: "GitLab Ireland Ltd"
description: "Discover GitLab's benefits for team members in Ireland"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

----

## Medical

GitLab offers a private medical plan through VHI which gives you access to cutting-edge medical treatments, health screenings and protection against the high medical costs of private care. GitLab covers 100% of the premiums for team members and eligible dependents.

For a full breakdown of plan descriptions please review the following [Company Plan Plus Level 1.3 details](https://drive.google.com/file/d/1pnyWAp71MMYzdsUm1rk0o4gq7k8cdvJO/view?usp=sharing) and [Table of Benefits](https://drive.google.com/file/d/1-0cfnrKSTyQIyAEro3wR7i2aBLHsvXEv/view?usp=sharing).

To find out which hospitals and treatment centers are covered on your plan, please refer to the directories of approved facilities which are available at `Vhi.ie/downloads` or contact VHI directly via the Corporate Advice Line: `056 777 5800`.

**Does Lifetime Community Rating (LCR) Apply?**
From 1st of May 2015, if you are 35 or over when you first take out private health insurance, you will pay an additional amount of 2% for each year that you are aged 35 and over. Please contact VHI if you think this may apply.

### Enroll in the VHI Plan

GitLab's VHI scheme will begin November 1, 2020. You can choose to enroll from this date or from your hire date (if later than Nov 1st). Team members will need to enroll via VHI directly either through the online portal or by calling VHI. GitLab has selected Company Plan Plus Level 1.3 company wide. If you feel that this does not suit your needs (or family member needs), please call VHI on `056 777 5800` and they will be able to discuss your requirements.

As part of the initial enrollment, please use the following [enrollment guide](https://drive.google.com/file/d/1YwsQp7ybVZ7cYg5G4EBlPFBJskxmjfqG/view?usp=sharing) to review the plans and enroll either via the online platform or by calling VHI. Enrollment will close on October 26th for a November 1st effective date.

### Tax Implications of Medical Plans

The health insurance allowance is treated like any other income or allowance under Revenue Commissioners rules. Your normal tax rate including other statutory deductions will apply. You will be liable for Benefit in kind on your health insurance premium, for more information please read [here](https://www.revenue.ie/en/personal-tax-credits-reliefs-and-exemptions/health-and-age/medical-insurance-premiums/index.aspx). 

## Pension

GitLab offers a private pension plan via a defined contribution scheme. Orca Financial is an Irish owned Financial Services company to help with the administration of the pension on behalf of GitLab. The pension provider is **Aviva**. Aviva allows for transfers into the plan and can set up an AVC pension for any members that wish to contribute above the minimum required contribution.

Orca has put together the following [video](https://drive.google.com/file/d/1IkVkLVtKonn8JsJ2hNGZI_89V3FZyPIU/view?usp=sharing) with information about the plan as a resource.  

### Pension Match

GitLab will match up to 5% of annual base salary of the team member's contributions.

### Enrollment

To enroll in the pension plan or to make changes to an existing contribution, please email `total-rewards@gitlab.com` with the desired percent contribution. Total Rewards will add Orca Financial `info@orca.ie` to the email thread. Orca will gather all relevant information from the team member to add to the pension plan. On the 7th of every month, Orca will send Payroll and Total Rewards a summary of any changes to the pension plan which Total Rewards will update in the appropriate payroll changes spreadsheet. Total Rewards will also file the original election email in BambooHR under the "Benefits and ISO" folder for an audit trail.

After each pay cycle, Payroll will send Aviva (ccing Orca and Total Rewards) a spreadsheet with all contributions for Aviva to reconcile. 

If you have any questions about a pension plan, how pension benefits work in Ireland, or anything else, please reach out to Orca Financial who will be able to assist you directly. Email: `info@orca.ie` Phone: `+353 1 2103030`

You will be able to review the investment strategy once enrolled through the Fund centre on the Aviva website.

## Death in Service

All full-time GitLab team members in Ireland are eligible for death in service at 4x annual salary. This benefit is administered through Aviva.

## Disability

All full-time GitLab team members are eligible for disability cover in the amount of 66% of base salary less social welfare to age 65 (after 13 weeks deferred period). This benefit is administered through Aviva.

## Additional Benefits Through Aviva

* Best Doctors
* Up to 15% off Aviva general insurance products
* Free travel insurance if you take our Car & Home together

## Bike to Work/Tax Saver Commuter Scheme

GitLab uses TravelHub to assist with the administration of the bike to work/tax saver commuter scheme. The GitLab specific online portal can be found at (https://gitlab.travelhub.ie/). Team members will need to register and account to enter the GitLab specific online portal. Here is a link to TravelHub's [FAQs](https://support.hubex.ie/hc/en-ie/articles/360044040031) about these programs.  

**Bike Applications:**

1. Once your account is created, select a Bike to Work Ltd partner shop. 
1. Pick the new bike and accessories if required. 
1. Upload the quote received from the bike ship to the TravelHub portal to have the order approved. 
1. The Total Rewards team will receive an email with the request to login, review, and approve the application (credentials can be found in 1password). Please note that all payments should be made to TravelHub directly and not to the supplier chosen by staff. TravelHub will pay the supplier once the employee has received their Bike or Ticket. 
1. Total Rewards will notify payroll to reduce the team member's gross salary by the cost of the bike using a Salary Sacrifice Agreement. 

**Ticket Applications:**

1. Once your account is created, select a Travel Operator. 
1. Select either a monthly or yearly ticket. 
1. Submit the travel ticket to the TravelHub portal to have the order approved. 
1. The Total Rewards team will receive an email with the request to login, review, and approve the application (credentials can be found in 1password). 
1. Total Rewards will notify payroll to reduce the team member's gross salary by the cost of the ticket using a Salary Sacrifice Agreement. 
1. Note: There is a 4% (plus VAT) booking fee for annual tickets and a 10 EUR (VAT inclusive) booking fee for monthly tickets paid for by the team member by being added to the price of the ticket. 

Once the total rewards team has approved the bike or ticket application, an invoice is generated. Once payment as been received, TravelHub emails the team member a voucher to use in the bike shop or tickets are dispatched in to the team member. 

## GitLab Ireland LTD Leave Policy

* Sick Leave

  * Team members should refer to [GitLab's PTO Policy](https://about.gitlab.com/handbook/paid-time-off/#sick-time-procedures---all-team-members) as there is no statutory requirement to provide sick pay or unpaid sick leave in Ireland. 
  * Team members must designate all time off for illness as `Out Sick` to ensure that PTO is properly tracked. 

* Paternity Leave in Ireland:
  *  The Paternity Leave and Benefit Act 2016 provides two weeks' statutory leave and benefit for employees who are the relevant parents of children born or adopted.
  *  Paternity leave generally must be taken in one block of two weeks within the first six months following the birth or adoption placement. The term "relevant parent" is defined broadly to provide for fathers, adoptive fathers, civil partners, cohabitants and same-sex couples.
  *  Paternity benefit is payable by the state subject to an employee's PRSI contributions. Workers should apply for the payment 4 weeks before they intend to go on paternity leave.
  *  State paternity benefit is payable at a rate of 245 EUR per week for two weeks.
  *  Leave certification - All employees must have their paternity leave certified by their employer. If you're an employee, you must provide GitLab with proof of the expected date of confinement of your spouse or partner. In other words, you will be required to provide your employer with a certificate from your spouse or partner's doctor confirming when your baby is due, or confirmation of the baby’s actual date of birth if you apply for leave after the birth has occurred. GitLab must then complete a form [(PB2: Employer Certificate for Paternity Benefit (pdf))](https://www.gov.ie/en/service/apply-for-paternity-benefit/) to confirm that you are entitled to paternity leave.
  *  [If you have been at GitLab for six months, GitLab will supplement Paternity Pay up to the full salary amount](/handbook/total-rewards/benefits/general-and-entity-benefits/#parental-leave).
  * Process for applying for state paternity benefit:
    * Notify Total Rewards of your baby's due date.
    * Total Rewards will send you the completed PB2 form.
    * Submit your application for state paternity benefit via the [MyWelfare website](https://services.mywelfare.ie/en/topics/parents-children-family/).
