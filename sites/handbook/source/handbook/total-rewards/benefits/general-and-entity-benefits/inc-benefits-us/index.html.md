---
layout: handbook-page-toc
title: "GitLab Inc (US) Benefits"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

----

## Specific to US based team members
{: #us-specific-benefits}

US based benefits are arranged through [Lumity](https://lumity.helloflock.com/login). The benefits decision discussions are held by Total Rewards, the CFO, and the CEO to elect the next year's benefits by the carrier's deadlines. Total Rewards will notify the team of open enrollment as soon as details become available.  

**The 2020 Open Enrollment will begin October 19, 2020 and will end on October 30, 2020. Your plan will renew January 1, 2021. _This will be an active enrollment, you must elect benefits in Lumity during the Open Enrollment window to have benefits in 2021._** Please review the information added on the plans for 2021 below and the [digital benefits guide](https://guides.lumity.com/f8wiCGbj/GitLab) is also available to review. 

Please review the full [summary plan description](https://drive.google.com/file/d/1XGOniJ-lnUyPdSvE_ce1W5KZlTzjRyTK/view?usp=sharing) of all related health benefits.

If you have any questions regarding benefits please reach out to Total Rewards directly. If you have any questions with the Lumity platform, feel free to reach out to their support personnel at `support@lumity.com` or `1-844-258-6489`.

Carrier ID cards are normally received within weeks of submitting your benefit elections. If you or your medical providers are in need of immediate confirmation of your coverage, please contact the carrier directly. ID cards are also available within the Lumity mobile app: [IOS](https://apps.apple.com/us/app/lumity-employee-benefits/id1460901256) or [Android](https://play.google.com/store/apps/details?id=com.lumity.mobile) or available to be printed through the carrier website, [myCigna](https://my.cigna.com/web/public/guest), for Cigna participants..

If you have existing coverage when joining GitLab (e.g. coverage for an additional month from your prior employer), you have the option of enrolling in GitLab's coverage after your prior coverage terminates. If you wish to do this, you should register with Lumity during onboarding and waive all coverages. Once your previous coverage is terminated, you should sign up for coverage through Lumity on or within 30 days of the termination date by initiating a Qualifying Life Event and providing proof of coverage termination.

GitLab covers **100% of team member contributions and 66% for spouse, dependents, and/or domestic partner** of premiums for medical, dental, and vision coverage. Plan rates are locked through December 31, 2020.

More information on the processed deductions in payroll from Lumity can be found on the [Accounting and Reporting page](/handbook/finance/accounting/).

## Group Medical Coverage

GitLab offers plans from Cigna for all states within the US as well as additional Kaiser options for residents of California, Hawaii, and Colorado. Deductibles for a plan year of 2021-01-01 to 2021-12-31.

For additional information on how [Benefits](https://drive.google.com/file/d/0B4eFM43gu7VPVS1aOFdRdXRSVzZ5Z3B0dEVKUXJIWm1zZXRj/view?usp=sharing) or [HSAs](https://drive.google.com/file/d/0B4eFM43gu7VPbUo1VFNFVFlQNlUyV0xQZkE0YXQyZENSNU1j/view?usp=sharing) operate, please check out the documentation on the Google Drive or Lumity's [resources](https://employee-resources.lumity.com/help).

_If you already have current group medical coverage, you may choose to waive or opt out of group health benefits. If you choose to waive health coverage, you will receive a $300.00 monthly benefit allowance and will still be able to enroll in dental, vision, optional plans, and flexible spending accounts._

If you do not enroll in a plan within your benefits election period, you will automatically receive the medical waiver allowance.

GitLab has confirmed that our medical plans are CREDITABLE. Please see the attached [notice](https://drive.google.com/file/d/1-uX5Sp0se1Kq5aPZQPqByeCTgcDen9-b/view?usp=sharing). If you or your dependents are Medicare eligible or are approaching Medicare eligibility, you will need this notice to confirm your status when enrolling for Medicare Part D.

### Qualifying Life Events

Due to IRS guidelines, you cannot make changes to your health insurance benefits outside of GitLab's annual open enrollment period unless you experience a [Qualifying Life Event](https://employee-resources.lumity.com/help/qualifying-life-event).

A QLE is a change in your situation — like getting married, having a baby, etc that can make you eligible for a special enrollment period. You have ***30 days from the date of your qualifying event*** to submit your requested change to Lumity. You will be asked to include supporting documents (see [Required Documents](https://employee-resources.lumity.com/help/qualifying-life-event)), and list the date the change occurred.

You can start the process by logging into [Lumity](https://lumity.helloflock.com/login), selecting Benefits from the panel on the left, and selecting Update Benefits in the upper right. Lumity support can be contacted directly with any questions.

### Eligibility

Any active, regular, full-time team member working a minimum of 30 hours per week are eligible for all benefits. Benefits are effective on your date of hire. Others eligible for benefits include:
  * Your legal spouse or domestic partner,
  * Your dependent children up until age 26 (including legally adopted and stepchildren), and/or
  * Any dependent child who reaches the limiting age and is incapable of self-support because of a mental or physical disability

Note: If you and an eligible dependent (as defined above) are both employed by GitLab, you may only be covered by GitLab’s coverage once. This also applies to enrolling in either your own supplemental life insurance or supplemental spouse/dependent life insurance through your dependent who is employed by GitLab, but not both.

### Cigna Medical Plans (2021)

Effective January 1, 2021, GitLab will be transitioning from UHC to Cigna as the primary national carrier. 

#### Cigna 2021 Calendar Year Plans

**Coverages:**

In Network:

| Plan Details               | [Cigna - HSA](https://drive.google.com/file/d/1snzJv2JsItipBS12ueibzedjZMgLjs_e/view?usp=sharing)  | [Cigna EPO (PPO $0)**](https://drive.google.com/file/d/147z46SlhXLGUhoXVjQTxI7Qs5ROL6ZLr/view?usp=sharing) | [Cigna PPO 500***](https://drive.google.com/file/d/1utEEBXzbvIY4guFt0asM86waDWVMdLHQ/view?usp=sharing)       |
|:---------------------------|:----------------------:|:------------------:|:-----------------:|
| Deductible (Single/Family) | $2,000 / $2,800        | $0 / $0            | $500 / $1,000     |
| OOP Max (Single/Family)    | $4,000 / $8,000        | $2,500 / $5,000    | $3,000 / $6,000   |
| Primary Care Visit         | 20%                    | $20 per visit      | $20 per visit     |
| Specialist Visit           | 20%                    | $20 per visit      | $20 per visit     |
| Urgent Care                | 20%                    | $50 per visit      | $50 per visit     |
| Emergency Room             | 20%                    | $100 per visit     | $100 per visit    |
| Hospital Inpatient         | 20%                    | $250 per admission | 10%               |
| Hospital Outpatient        | 20%                    | 0%                 | 10%               |
| Generic                    | $10                    | $10                | $10               |
| Brand - Preferred          | $30                    | $30                | $30               |
| Brand - Non-Preferred      | $50                    | $50                | $50               |
| Rx Plan^                   | [Standard 3-tier](https://www.cigna.com/static/www-cigna-com/docs/individuals-families/member-resources/prescription/standard-3-tier.pdf) | [Standard 3-tier](https://www.cigna.com/static/www-cigna-com/docs/individuals-families/member-resources/prescription/standard-3-tier.pdf) | [Standard 3-tier](https://www.cigna.com/static/www-cigna-com/docs/individuals-families/member-resources/prescription/standard-3-tier.pdf) |

** In order for the EPO plan to be compliant in all states, it has been set up as a PPO plan with bad out-of-networks benefits including a deductible of $10k/$20k, an OOPM of $20k/$40k, and a coinsurance of 50%. Please do not enroll in this plan if you are intending to use the out-of-networks benefits and instead review the Cigna PPO 500 or Cigna HSA plans. 

*** Cigna will provide an extended network PPO Plan for Utah team members which will include the Intermountain Healthcare System.

^ The linked Prescription Drug List is subject to change. When making a change mid-year, Cigna states that they will send out the following communication to impacted members: centralized notification 60 days prior, 2 letters before the change, and 1 letter after the change.

**Team Member Costs:**

The following costs are monthly rates which would be deducted from your paycheck.

| Tier                     | HSA | EPO  | PPO  |
|--------------------------|:---:|:----:|:----:|
| Team Member Only         | $0  | $0   | $0   |
| Team Member + Spouse     | $0  | $234 | $222 |
| Team Member + Child(ren) | $0  | $180 | $168 |              
| Family                   | $0  | $402 | $378 |

#### Cigna Infertility Services

Infertility services will be included in both the PPO 500 plan and HSA plan. This includes a $15,000 lifetime infertility benefit for each covered member. More details will be included in the SBCs once available.

#### Cigna Transgender Benefit Coverage

Cigna has advised they will provide [WPATH](https://www.wpath.org/) compliant coverage for all plans. For more information on their coverage, please see the [coverage policy document](https://drive.google.com/file/d/1sdsiFcTFEWsIidOXwPBcNNOCqFVERSXg/view?usp=sharing). For Cigna to provide coverage, medical necessity must be proven. It is highly recommended to go through the prior authorization process when seeking treatment. 

Please reach out to the Total Rewards team and Lumity with any questions or if you need help while seeking authorization for a treatment. 

#### Cigna Provider Search 

1. Visit [myCigna](https://my.cigna.com/web/public/guest). If you haven't previously registered for an account, you will want to register.
1. Select "Find Care & Costs" at the top of the page.
1. Here you can select to search by Doctor Type, Name, Reason for Visit, Facility, or you can price a medication at local pharmacies.
1. When you scroll down, there are quick links for your nearest urgent care and telehealth.

IMPORTANT NOTE:

As an additional measure to confirm “Contracted Providers” (i.e. In-Network), it’s always a good idea to follow up with Providers directly (phone, email or otherwise) to additionally confirm if a Provider is contracted with Cigna or not.
Provider contracts change and sometimes the contracts change faster than the website maintenance teams for the insurance carriers. It's better to spend time researching and confirming rather than assuming and being stuck with *balance billing* from Out of Network providers.
*Balance billing* is if the Out of Network provider's charge is $100 and the carrier’s maximum allowed reimbursement (MAR) amount is $70, the provider is within rights to bill you for the remaining $30. A “Cigna Contracted Provider” cannot balance bill Cigna members.

#### Cigna Telehealth

Virtual visits for Cigna members can be accessed by visiting [myCigna](https://my.cigna.com/web/public/guest), selecting "Find Care & Costs" and scrolling down to the bottom of the page. 

### Kaiser Medical Plans

#### Kaiser 2021 Calendar Year Plans

**Coverages:**

| Plan Details               | [HMO 20 NorCal](https://drive.google.com/file/d/1kgJGxaVfAwdN5E2fD0gbzKFWW1eCyU56/view?usp=sharing)       | [HMO 20 SoCal](https://drive.google.com/file/d/1UtnmvtKmn4PKETOphAQ7e54lYu32NGb_/view?usp=sharing)    | [HMO 20 CO](https://drive.google.com/file/d/1MuG9MQumOq3OjAMtwjil33RKkWdNrAa7/view?usp=sharing)     | [HMO 20 HI](https://drive.google.com/file/d/1oLiQJkVcb2YLuvB-adEJPwf4knM1R9J3/view?usp=sharing)       |
|----------------------------|:--------------------:|:---------------:|:---------------:|:---------------:|
| Deductible (Single/Family) | $0 / $0              | $0 / $0         | $0 / $0         | $0 / $0         |
| OOP Max (Single/Family)    | $1,500 / $3,000      | $1,500 / $3,000 | $2,000 / $4,000 | $2,500 / $7,500 |
| PCP/Specialist Copay       | $20 / $35            | $20 / $35       | $20 / $35       | $15 / $15       |
| Emergency Room             | $100                 | $100            | $250            | $100            |
| Urgent Care                | $20                  | $20             | $50             | $15             |
| Hospital Inpatient         | $250/admit           | $250/admit      | $300/admit      | 10%             |
| Hospital Outpatient        | $35/procedure        | $35/procedure   | $100/procedure  | 10%             |
| **Rx - Deductible**        |                      |                 |                 |                 |
| Generic                    | $10                  | $10             | $10             | See SBC (tier 1), $3 (tier 1a), $15 (tier 1b)             |
| Brand - Preferred          | $35                  | $35             | $30             | $50             |
| Brand - Non-Preferred      | $35                  | $35             | $50             | $50             |
| Specialty Drugs            | 20% up to $150       | 20% up to $150  | 20% up to $250  | $200            |

**Team Member Costs:**

The following costs are monthly rates which would be deducted from your paycheck.

| Tier                     | HMO CA North | HMO CA South | HMO CO | HMO HI |
|--------------------------|:------------:|:------------:|:------:|:------:|
| Team Member Only         | $0           | $0           | $0     | $0     |
| Team Member + Spouse     | $240         | $240         | $276   | $174   |
| Team Member + Child(ren) | $192         | $192         | $228   | $138   |          
| Family                   | $366         | $366         | $456   | $342   |

#### Kaiser Telehealth

Virtual visits for Kaiser members can be accessed by logging into Kaiser's [online portal](https://healthy.kaiserpermanente.org/). Please consult the online portal and your plan details for your copay amount.

#### Kaiser Period to Submit Claims

For in-network services: N/A.

For out-of-network services: 365 days from Date of Service.

### Infertility Services

Infertility services are provided through the UHC HSA plan. Please consult the legal SBC for more information or reach out to Total Rewards.

### Pregnancy & Maternity Care

With medical plans, GitLab offers pregnancy and maternity care. Depending on the plan you selected, your coverages may differ for in-network vs out-of-network, visits, and inpatient care. Please contact Lumity or Total Rewards with any questions about your plan. Once your child has arrived, please follow the steps outlined above in regards to this [Qualifying Life Event](/handbook/total-rewards/benefits/general-and-entity-benefits/inc-benefits-us/#qualifying-life-events).

## Dental

Dental is provided by Cigna, plan: DPPO.

Dental does not come with individualized insurance cards from Cigna, although you can download them by setting up a Cigna account through the [Cigna website](https://my.cigna.com). Lumity's site will house individualized ID cards team members can access at any time. For the most part, dental providers do not request or require ID cards as they look up insurance through your social security number. If you need additional information for a claim please let People Ops know. Cigna'a mailing address is PO Box 188037 Chattanooga, TN, 37422 and the direct phone number is 800-244-6224.

When submitting a claim, you can mail it to Cigna Dental PO Box 188037 Chattanooga, TN, 37422 or fax it to 859-550-2662.

### Dental 2021 Calendar Year Plan

**Coverages:**

| Plan Details                         | [DPPO](https://drive.google.com/file/d/1s8x3BJ36hJ5gU-vn29dQSOFVkhvm1rH4/view?usp=sharing)       |  
|--------------------------------------|:----------:|
| Deductible                           | $50 / $150 |
| Maximum Benefit                      | $2,000     |
| Preventive Care CoInsurance (in/out) | 0% / 0%    |
| Basic Care Coinsurance (in/out)      | 20% / 20%  |
| Major Care Coinsurance (in/out)      | 50% / 50%  |
| Out of Network Reimbursement         | 90th R&C   |
| **Orthodontia**                      |            |
| Orthodontic Coinsurance (in/out)     | 50% / 50%  |
| Orthodontic Max Benefits             | $1,500     |

**Team Member Costs:**

The following costs are monthly rates which would be deducted from your paycheck.

| Tier                     | DPPO |
|--------------------------|:----:|
| Team Member Only         | $0   |
| Team Member + Spouse     | $12  |
| Team Member + Child(ren) | $18  |
| Family                   | $36  |

#### Cigna Dental Period to Submit Claims

For in-network services: N/A.

For out-of-network services: 365 days from Date of Service.

## Vision

Vision is provided by Cigna.

When submitting a claim, you can mail it to Cigna Vision PO Box 385018 Birmingham, AL 35238 or submit it online using the following instructions:

1. Log in or register an account at https://cigna.vsp.com/.
1. Navigate to "Claims & Reimbursement" on the left panel.
1. Choose yourself or dependent from the dropdown depending who the claim is for.
1. Expand the "Customer Reimbursement Form" section.
1. Click "Continue" to be taken to the online claim form. Make sure you attach an itemized receipt when prompted.

### Vision 2021 Calendar Year Plan

**Coverages:**

| Plan Details                      | [Vision](https://drive.google.com/file/d/1iqeSHND7WzTouvP-Rzc6kw8THxz2DlDL/view?usp=sharing)       |  
|-----------------------------------|:------------:|
| Frequency of Services             | 12 / 12 / 12 |
| Copay Exam                        | $20          |  
| Copay Materials                   | -            |
| Single Vision                     | $0           |  
| Bifocal                           | $0           |
| Trifocal                          | $0           |
| Frame Allowance                   | up to $130   |
| Elective Lenses Contact Allowance | up to $130   |

**Team Member Costs:**

The following costs are monthly rates which would be deducted from your paycheck.

| Tier                     | Vision |
|--------------------------|:------:|
| Team Member Only         | $0     |
| Team Member + Spouse     | $2.40  |
| Team Member + Child(ren) | $1.80  |
| Family                   | $4.80  |

#### Cigna Vision Period to Submit Claims

For in-network services: 365 days from Date of Service.

For out-of-network services: 365 days from Date of Service.

## Basic Life Insurance and AD&D

GitLab offers company paid [basic life](https://drive.google.com/open?id=1wl62igCepQ23kjF8QnyVLsvorkr7joxP) and [accidental death and dismemberment (AD&D)](https://drive.google.com/open?id=1wl62igCepQ23kjF8QnyVLsvorkr7joxP) plans through UHC. The Company pays for basic life insurance coverage valued at two times annual base salary with a maximum benefit of $250,000, which includes an equal amount of AD&D coverage.

## Group Long-Term and Short-Term Disability Insurance

GitLab provides a policy through UHC that may replace up to 66.7% of your base salary, for qualifying disabilities. For [short-term disability](https://drive.google.com/open?id=12_xDzUIuBgSJB0AJ6I0oE2VEgMdwymWx) there is a weekly maximum benefit of $2,500; for [long-term disability](https://drive.google.com/open?id=14uIt3CUabd4Yj95BDgNl9Rdfzi6SxrFW) there is a monthly benefit maximum of $12,500. More information can be found in the [UHC Long-Term Disability Welcome brochure](https://drive.google.com/file/d/195tzz2R7cqmmMnor9sWDXjeelhyx4CoE/view?usp=sharing) and [UHC Short-Term Disability Welcome brochure](https://drive.google.com/file/d/19oWAPE-V5n88WRHgXFuyiOL-elX2FiG3/view?usp=sharing) for these benefits.

**Process for Disability Claim**

1. If an team member will be unable to work due to disability for less than 25 calendar days, no action is needed and the absence will be categorized under [paid time off](/handbook/paid-time-off/).
1. Since the short-term disability insurance has a 7-day waiting period, the team member should decide on day 18 whether they will be able to return to work after 25 calendar days. If they will not be able to return, they should inform the Total Rewards team of their intent to go on short-term disability and apply for short-term disability at this time by sending the Total Rewards team a completed [Leave Request Form](https://drive.google.com/open?id=1BZKHKPHvmMoOS1ctjXG3q6RHk8RHYuv1). While the team member is on short-term disability (which covers 66.7%), GitLab will supplement the remaining 33.3% through payroll if the team member has been employed for more than six months. Benefit coverage will also continue for the time the team member is on short-term disability.
1. At the end of the maximum benefit period for short-term disability of 12 weeks, the team member will determine whether they are able to return back to work. If they are unable to, the team member will be moved to unpaid leave and will have the option to continue their benefits by electing [COBRA coverage](https://www.dol.gov/sites/dolgov/files/ebsa/about-ebsa/our-activities/resource-center/faqs/cobra-continuation-health-coverage-consumer.pdf). The team member will be eligible to apply for long-term disability at this time.

## Employee Assistance Program

GitLab team members in the United States are eligible for a complementary [Employee Assistance program](hhttps://www.cigna.com/individuals-families/member-resources/employee-assistance-program) as a result of enrollment in the long-term disability plan through Cigna, dependents who are enrolled in a Cigna coverage are also eligible. More information can be found online on [myCigna](https://my.cigna.com/web/public/guest) for the following topics: Emotional Health and Family Support, Home Life Referrals, Financial and Legal Assistance, Job and Career Support, and other topics.

## 401k Plan

The company offers a 401k plan in which you may make voluntary pre-tax contributions toward your retirement.

### Administrative Details of 401k Plan

1. You are eligible to participate in GitLab’s 401k as of your hire date. There is no auto-enrollment. You must actively elect your deductions.
1. You will receive an invitation from [Betterment](https://www.betterment.com) who is GitLab's plan fiduciary. For more information about Betterment please check out this [YouTube Video](https://www.youtube.com/watch?v=A-9II-zBq1k).
1. Any changes to your plan information will be effective on the next available payroll.
1. Once inside the platform you may elect your annual/pay-period contributions and investments.
1. If you have any questions about making changes to your elections, we recommend that you reach out to Betterment directly, by chat in the app, by phone at 855-906-5281, or by email at `support@betterment.com`. There is also a [Help section](https://www.betterment.com/resources/tags/help/) on the Betterment site.
1. Please review the [Summary Plan Document](https://drive.google.com/file/d/1k1xWqZ-2HjOWLv_oFCcyBkTjGnuv1l_B/view?usp=sharing) and [QDIA & Fee Disclosure](https://drive.google.com/file/d/10-nOQTAsYqj1S6xrRFzTa4-H4qQnOOa3/view?usp=sharing). If you have any questions about the plan or the documents, please reach out to Total Rewards at `total-rewards@domain`. Total Rewards is not able to advise you on your financial decisions.
1. ADP payroll system monitors and will stop the 401(k) contribution when you have reached the IRS limit for the year.
1. If your employment with GitLab terminates and you are unable to access your Betterment account due to this being connected to your GitLab email, please contact Betterment at 646-600-8263 to have the email address on file updated.

### 401(k) Match

GitLab offers matching 50% of contributions on the first 6% of allocated earnings with a yearly cap of 1,500 USD. As you are eligible to participate in GitLab's 401k as of your hire date, you are *also* eligible for GitLab matching contributions as of your hire date.

All employer contributions are pre-tax contributions. Team members can still make Roth team member contributions and receive pre-tax employer contributions.

**Vesting:**

Employer contributions vest according to the following schedule:

| Years of Vesting Service             | Vesting Percentage |
|--------------------------------------|:------------------:|
| Less than One Year                   | 0%                 |
| One Year but less than Two Years     | 25%                |
| Two Years but less than Three Years  | 50%                |
| Three Years but less than Four Years | 75%                |
| Four or More Years                   | 100%               |

*Employee* contributions are the assets of those team members and are not applicable to this vesting schedule.

**Vesting example**

To help you understand the math, here is a hypothetical vesting chart showing how much the employeed would get if they left the company.

In this example the employee's salary is $50,000 USD and they max out their match every year. 50,000 * 6% = 3000. 3000 * 50% = 1500 match.

Year 1: Put in 3K, GitLab matches 1500. Leave the company get 3K (none vested)
Year 2: Put in 3K, GitLab matches 1500. Leave the company get 6K own money + $750 USD vested contribution.   (3K * 0.25)
Year 3: Put in 3K, GitLab matches 1500. Leave the company get 9K own money + $2250 USD vested contribution. (4500 * 0.50)
Year 4: Put in 3K, GitLab matches 1500. Leave the company get 12K own money + $4500 USD vested contribution. (6000 * 0.75)
Year 5: Put in 3K, GitLab matches 1500. Leave the company get 15K own money + $7500 USD (fully vested)

**Administration of the 401(k) Match:**
* The employer will use the calculation on each check date effective as of January 1, 2019.
* The team member must have a contribution for a check date to be eligible for the employer match.
* Employer matching will be released into participant accounts three business days after the check date.
* For team members who defer more than 6% on each check date, Payroll will conduct a true up quarterly.

### 401(k) Rollover

If you leave GitLab and would like to rollover your 401(k) account, contact Betterment directly to get more information about this process. You can reach Betterment, by Chat in the app, by phone at 855-906-5281, and by email at `support@betterment.com`. They also have a Rollovers section on their site going into detail.

### 401(k) Committee

The 401(k) Committee will meet quarterly with Betterment to review how the plan is doing as well as updates from the Betterment investment team.

**Committee Members:**
Chair: Principal Accounting Officer (PAO)
**Other Members:**
* Principal Accounting Officer
* Chief Legal Officer
* Manager, Total Rewards
* Senior Director, People Success
* Senior Manager, Payroll and Payments

**Gitlab's 401(k) Committee Responsibilities:**
* Maintain and enforce the plan document and features
* Comply with all reporting, testing and disclosure requirements
* Timely data & deposit transmission
* Evaluating plan services and fees
* Review and approval of prepared forms and distributions
* Employee engagement and eligibility
* Adding new eligible employees to the plan

**Betterment's Responsibilities (co-fiduciary):**
* Investment selection and monitoring
* Audit support
* Employee education
* Administrative support on distributions, loans and more
* Employee and Plan Sponsor customer support via email and phone
* Statement and tax form generation

## Optional Plans Available at Team Member Expense

### Discovery Benefits

If you are enrolled in an HSA, FSA, or commuter benefits, the funds are held through Discovery Benefits. After your benefit enrollment effective start date, [create an account with Discovery](https://www.discoverybenefits.com/) to manage your funds. You will only receive one debit card upon enrollment. To obtain a second card (for a dependent, etc.) you will need to login to your account on Discovery or call and they will send one to your home of record.

If you would like to transfer your HSA from a previous account, please contact Discovery Benefits and request a HSA Transfer funds form. On the form you will put your old HSA provider’s account number and any other required personal information. You will then submit the form to Discovery, and they will get in contact with your old HSA provider and process the transfer of funds. You can reach Discovery Benefits at 866.451.3399. If you would like to adjust your HSA contributions please log into [Lumity](https://lumity.helloflock.com/login). HSAs roll over completely year-to-year, and FSAs have a $550 rollover each calendar year.

FSAs are employer-owned accounts. If you leave GitLab, you cannot take FSA accounts with you. You are able to use your full FSA amounts up to and on your last day, but not afterwards. Your FSA benefits stop on your termination date. Discovery Benefits asks that all claims be submitted up to 90 days after termination date. For additional information, please reach out to Discovery Benefits at 866.451.3399.

#### Flexible Spending Account (FSA) Plans

FSAs help you pay for eligible out-of-pocket health care and dependent day care expenses on a pretax basis. You determine your projected expenses for the Plan Year and then elect to set aside a portion of each paycheck into your FSA.

##### FSA Period to Submit Claims

Up to 90 days after the plan year has concluded (also known as the runout period).

#### Commuter Benefits

GitLab offers [commuter benefits](https://drive.google.com/file/d/0B4eFM43gu7VPek1Ia0ZqYjhuT25zYjdYTUpiS1NFSXFXc0Vn/view?usp=sharing) which are administered through Discovery Benefits. The contribution limits from the IRS for 2020 are $270/month for parking and $270/month for transit. These contributions rollover month to month.

##### Commuter Benefits Period to Submit Claims

For active employees: Up to 180 days after plan year has concluded (also known as the runout period).

For terminated employees: Up to 90 days after the termination date.

### Cigna Supplemental Life/AD&D 

Team members who wish to elect additional life insurance above what is provided by GitLab through the Basic Life Insurance or elect life insurance for their dependents, can elect [Voluntary Life Insurance](https://drive.google.com/file/d/1yY1tFJTuj1wCMlorUvnfcgWwZUuP279r/view?usp=sharing) through Cigna.
   * $10,000 Increments up to the lesser of 6x annual salary or $750,000 for team members
   * $5,000 Increments up to the lesser of $250,000 or 100% of team member election for spouses and domestic partners
   * $10,000 of coverage available for children

##### Evidence of Insurability

If you elect greater than the guaranteed issue of voluntary life insurance for yourself or dependent, you or your dependent may be required to complete an evidence of insurability (EOI). If required, you will be prompted and provided with the form during the enrollment process. 

Please complete this form to the best of your ability, but if you're unsure for any field, please leave it blank. No information needs to be filled out for the ID # field and for security, you may also leave the Social Security Number field blank. 

Once complete, please send the form to `total-rewards@ domain` and `support@lumity.com`. Lumity and the Total Rewards team will then help fill in any missing information, if applicable, and Lumity will forward to the carrier for review. Lumity will confirm receipt of the EOI with the carrier, track its status, and reach out to the team member with any issues that need to be addressed in order for the EOI to be approved.


## Team Member Discount Platforms

US team members have access to two discount platforms offered through ADP and Lumity. These platforms provide discounts for national and local brands and services.

To access LifeMart through ADP:
1. Login to ADP using the following link: [(https://workforcenow.adp.com.)]
1. Click on the "MYSELF" tab in the navigation bar, hover your mouse over "Benefits" in the dropdown menu, and click "Employee Discounts - Life Mart".
1. Confirm the email you use to access ADP and click "View my discounts" to enter the website.

To access PerkSpot through Lumity:
1. Navigate to https://lumity.perkspot.com/.
1. Click "Create an Account" and fill out the form in order to register.

## Monthly Health Bill Payments

The Total Rewards Analyst will review and initiate payment for all monthly health bills in the United States.

* All bills are available on the first of the month and should be paid by the 13th.
* Lumity will send a reconciliation report breaking down the bills by department. People Ops will transfer the department breakdown and Group Invoice to the "Lumity Bill Reconciliations" google sheet.
* TODO Build in audit procedure to verify bills against current elections to ensure accuracy.
* Total Rewards will login to each admin platform and pay the bills using the banking information and links found in the 1password note: "Monthly Heath Bills"
* Total Rewards will then email a detailed breakdown of the amount/department totals to `ap@gitlab.com` for accounting purposes.

## GitLab Inc. United States Leave Policy:

Based on the Family and Medical Leave Act, or [FMLA](https://www.dol.gov/agencies/whd/fmla), US team members are "eligible to take job-protected leave for specified family and medical reasons with continuation of group health insurance coverage under the same terms and conditions as if the team member had not taken leave." For more information on what defines an eligible team member and medical reason please visit the [Electronic Code of Federal Regulation](http://www.ecfr.gov/cgi-bin/text-idx?c=ecfr&sid=d178a2522c85f1f401ed3f3740984fed&rgn=div5&view=text&node=29:3.1.1.3.54&idno=29#sp29.3.825.b) for the most up to date data.

### Apply For Parental Leave in the US

1. Notify the Total Rewards team of intention to take parental leave at least 30 days in advance, or as soon as reasonable.
2. For a birthing parent (maternity leave), the team member will fill out the [Leave Request Form](https://drive.google.com/open?id=1BZKHKPHvmMoOS1ctjXG3q6RHk8RHYuv1) per the instructions below and email the form to total-rewards@ domain.
      - Pages 3-5 and 8 have to be completed before Total Rewards can email the form to UHC. 
3. Once the form is complete, the Total Rewards team will email the form the UHC using the email address provided on the form through the Secure UHC upload.
      - Once the team member has filed a claim for STD, UHC will confirm the start and end dates of claim via a determination letter. 
      - The typical pay for STD is six weeks for a vaginal birth and eight weeks for a c-section. STD can be extended if the team member has a pregnancy related complication (bed rest, complications from delivery, etc).
1. The Total Rewards team will send the team member an email with how payments will be processed and advise any differences in pay.
      - Use PTO time for the Elimination Period
1. The Total Rewards team will confirm [payroll details](#payroll-processing-during-parental-leave) with the Payroll team via the Payroll Changes google sheet.
1. The team member will notify the Total Rewards team on their first day back to work.
1. The Total Rewards team will get a notification once the claim is closed. 
1. TODO Outline process to return the team member to work

#### Maternity Leave STD Form - Team Member to fill out:
    - General Demographics (page 3): Name, SSN, address, phone number, date of birth, gender and marital status. 
    - Employment and Claim Information (page 4):
      - Date of hire
      - Date you first noticed symptoms of illness/injury: When the symptoms of the pregnancy developed
      - Date last worked: Due date of birth
      - Hours worked that day: 8
      - What date do you expect to return to work: Anticipated date of return
      - When were you first treated for your injury or illness? First Prenatal appointment 
    - Have you ever had the same similar condition in the past? Y if you have been pregnant before, N if it is your first pregnancy
    - Have you returned to work: n/a
    - Your occupation: Your job title
    - What parts of your job are you unable to do? n/a
    - Please describe the onset and nature of your illness or injury: Pregnancy
    - Is your claim a result of illness or accident: N
    - Was your injury or illness due to an auto accident? N
    - Were you injured at work? N
    - Fill out your physician's details and dates of appointments
    - Benefits and Earnings Information (page 5): Leave blank
    - Tax Information (page 5): Fill out based on your individual tax preferences 
    - Final Signature and Certification (page 5)
    - Review and sign the Disclosure Authorization (page 6)
    - Complete the Authorization of Personal Representative section (page 7) if there is someone involved in your care that you would like to be able to contact UHC in relation to your disability claim
    - Have your physician complete the Attending Physician's Disability Statement (page 8)

#### Maternity Leave STD Form - Total Rewards to fill out:
    - General Demographics (page 1):
      - Employee's Name, SSN, address, phone number, date of birth, gender and marital status
      - Employer's name, phone number, employer's address
    - Employment and Claim Information (page 2): 
       - Date of hire
       - Last day worked: Estimated last day of work
       - Insurance/division: Insurance Class
       - Effective date of STD coverage: 2020-01-01 (until policy is renewed)
       - Was coverage effective date within the last 12 months? Y
       - Occupation: Team member's job title
       - List employee's job duties: Add the link to the team member's job responsibilities
       - Has employment been terminated? N
       - Has employee returned to work? Leave blank
       - If yes, return to work date? TBD
       - Employee has returned to work in what capacity? Leave blank
       - Are you willing to make return-to-work accommodations for the employee if needed? Y
       - Was employee injured at work? N
    - Benefits and Earning Information
       - Does the employee contribute to the STD premium? N
       - Is the employee also covered under a LTD or Life Insurance Policy provided by us? Check both LTD and Life
       - If yes, do they contribute to the LTD premium? N
       - If yes, do they contribute on a PRE or POST tax basis? Leave blank
       - How is the employee paid? Add hourly rate if team member is getting paid per hour. If not, add the annual salary next to "Salaried"
       - Does the employee eceive other work related income? Add the annual commission amount if team member receives variable pay.
       - Is the employee currently receiving or eligible for any other income benefits? Leave blank
    - Final Signature and Certification: Stage for signatures via HelloSign


### Payroll Processing During Parental Leave

**Paternity Leave**
Paternity Leave is not covered under Short Term Disability, so if the team member is eligible for 100% of pay, payroll would enter 100% of the pay period hours under "Leave with Pay."

**Maternity Leave**
For maternity leave, GitLab will verify 100% of the wages are paid for eligible team members through payroll and Short-term Disability (STD).

1. While the team member is on short-term disability (which covers 66.7%), GitLab will supplement the remaining 33.3% through payroll.
  * The team member will inform Total Rewards when their short term disability is set to begin and end.
  * There is a seven day elimination period for short term disability that is not paid through Cigna, where GitLab will need to supplement the entire wage through payroll.
    * For instance, if Cigna has approved STD for January 1 - February 12, January 1 - January 7 would not be paid through STD.
    * If any adjustments need to be made to a payroll that has already passed, People Ops will coordinate with the Payroll Lead to ensure retroactive payments are made.
  * For the days the team member is paid through STD, payroll will adjust leave with pay hours to equal 33.3% and leave without pay hours to equal 66.7%.
    * For example, if there are 80 hours in the pay period you would input 26.66 hours Leave with Pay / 53.34 Leave without Pay.
1. When short-term disability ends, payroll will need to have 100% of the hours fall under leave with pay.

Note: Also, ensure the disability is not is not capped out at the current maximum or that will also need to be supplemented in each pay cycle.

## COBRA

If you are enrolled in medical, dental, and/or vision when you terminate from GitLab (either voluntarily or involuntarily), you may be eligible to continue your coverage through [COBRA](https://www.dol.gov/sites/dolgov/files/ebsa/about-ebsa/our-activities/resource-center/faqs/cobra-continuation-health-coverage-consumer.pdf).

### Timeline
1. Typically terminations are updated in BambooHR on the date of the termination and once updated, will sync with [Lumity](https://lumity.helloflock.com/login) by the end of the next business day.
1. Once the termination has synced with Lumity, the information will then be sent over to [Discovery Benefits](https://www.discoverybenefits.com/), our COBRA administrator. Government guidelines give 30 days for Discovery to be notified of the COBRA eligibility, but typically this will take about 1-2 weeks.
1. Once notified, Discovery has 14 days to generate and send the COBRA enrollment packet. Allow normal mailing timelines (5-10 business days) to receive the packet once sent.
1. You will have 60 days from the time you receive the COBRA packet to enroll either through the mail or online. Instructions for how to enroll will be included in your COBRA packet. Coverage will be retro-effective to the date coverage was lost.
1. From the day you enroll, you have 45 days to bring your payments to current.
1. You may remain on COBRA for up to 18 months. Please see the COBRA enrollment packet for information on extending COBRA an additional 18 months, if applicable. The state you reside in may allow for additional time on COBRA, but may be more expensive and include only Medical. Please consult the laws for your state for more information.

If you are currently employed and have any general COBRA questions, feel free to contact the Compensation & Benefits team. If you have terminated already or have specific questions on the administration of COBRA, feel free to contact Discovery Benefits directly: (866) 451-3399.

### Costs per Month

**Medical**

| Tier                           | UHC HSA   | UHC EPO (A/B) |  UHC PPO   | Kaiser HMO NorCal | Kaiser HMO SoCal | Kaiser HMO CO | Kaiser HMO HI |
|--------------------------------|:---------:|:-------------:|:----------:|:-----------------:|:----------------:|:--------------:|:------------:|
| Team Member Only               | $371.52   |   $497.25     | $496.12    |     $502.20       |     $502.20      |    $625.78     |   $481.83    |
| Team Member + Domestic Partner | $813.61   |   $1,088.96   | $1,086.49  |     $1,159.72     |     $1,159.72    |    $1,376.71   |   $963.66    |
| Team Member + Spouse           | $813.61   |   $1,088.96   | $1,086.49  |     $1,159.72     |     $1,159.72    |    $1,376.71   |   $963.66    |            
| Team Member + Child(ren)       | $705.88   |   $944.76     | $942.61    |     $1,024.48     |     $1,024.48    |    $1,251.56   |   $867.29    |
| Family                         | $1,129.41 |   $1,511.63   | $1,508.19  |     $1,521.31     |     $1,521.31    |    $1,877.34   |   $1,445.49  |
| Spouse Only                    | $371.52   |   $497.25     | $496.12    |     $502.20       |     $502.20      |    $625.78     |   $481.83    |
| Spouse + Child(ren)            | $705.88   |   $944.76     | $942.61    |     $1,024.48     |     $1,024.48    |    $1,251.56   |   $867.29    |
| Child Only                     | $371.52   |   $497.25     | $496.12    |     $502.20       |     $502.20      |    $625.78     |   $481.83    |

**Dental**

| Tier                           | Cigna DPPO |
|--------------------------------|:----------:|
| Team Member Only               |   $38.99   |
| Team Member + Domestic Partner |   $77.48   |
| Team Member + Spouse           |   $77.48   |
| Team Member + Child(ren)       |   $89.62   |
| Family                         |   $137.49  |
| Spouse Only                    |   $38.99   |
| Spouse + Child(ren)            |   $89.62   |
| Child Only                     |   $38.99   |

**Vision**

| Tier                           | Cigna VPPO |
|--------------------------------|:----------:|
| Team Member Only               |   $7.51    |
| Team Member + Domestic Partner |   $15.05   |
| Team Member + Spouse           |   $15.05   |          
| Team Member + Child(ren)       |   $12.74   |
| Family                         |   $21.01   |
| Spouse Only                    |   $7.51    |
| Spouse + Child(ren)            |   $12.74   |
| Child Only                     |   $7.51    |

## Audit Processes

### Discovery Funding Account Audit

This quarterly audit is conducted to ensure the funding of our account used for FSA, DCFSA, and commuter benefit plans according to Accounts Payable matches the amount of claims incurred in Discovery's system.

1. Reach out to Accounts Payable to provide an updated payment history report for payments made to Discovery.
1. In the `Ongoing Discovery Audit` spreadsheet, add new entries in the report provided by Accounts Payable to the bottom of the table in the "Discovery Payments History" tab.
1. Navigate to [Discovery's platform](https://www.discoverybenefits.com/) and log into the employer portal.
   * Select "Benefits Administration" in the left toolbar.
   * Navigate to the "Reports" tab and select the "Employer Funding Report".
   * Download all reports for the months that have elapsed since the last audit was conducted.
1. Add the new monthly report(s) to the `Ongoing Discovery Audit` spreadsheet as new tabs.
1. Reconcile all funding sent by Accounts Payable against the Employer Funding Report details.
   * AP funding will be denoted as "MANUAL EMPLOYER TRANSACTION AND ADJUSTMENT" in these reports.
1. On the "Funding Summary" tab, add the newly downloaded month(s) to the bottom of the summary table:
   * Add the year of the report(s) in column A.
   * Add the month of the report(s) in column B.
   * Copy the formula down for columns C, D, and E.
   * For columns D and E, replace the year and month in the formula with the year and month inputted in a column A and B.
     * For example, if the formula current has `=sumif('March 2020 Funding Detail'!A:A,"Manual Employer Transaction and Adjustment",'March 2020 Funding Detail'!H:H)` and you are working on the row for April 2020, change the formula to say `=sumif('April 2020 Funding Detail'!A:A,"Manual Employer Transaction and Adjustment",'April 2020 Funding Detail'!H:H)`.
1. In the same "Funding Summary" tab, review the difference calculated in cell L3. This difference should be positive and roughly equivalent to the amount we currently have available in our Funding Account for Discovery, typically in the range of $5,000 to $50,000.
1. Any discrepancies or problems should be escalated to the Manager, Total Rewards.

### Discovery Payroll Audit

TODO

### Lumity/Carrier Enrollment Audit

This quarterly audit is conducted to identify any differences in enrollment between the carrier records and what a team member has elected in Lumity.

#### UHC Medical

TODO

#### Kaiser Medical

TODO

#### Cigna Dental/Vision

TODO

#### UHC Life/Disability

TODO
