---
layout: handbook-page-toc
title: "Making Decisions"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Intro to making decisions

On this page, we have outlined how we make decisions at GitLab.  

## Making decisions

1. We use our [values](/handbook/values/), and particularly our [values hierarchy](/handbook/values/#hierarchy), to guide the decisions we make.
1. We combine the [best of both hierarchical and consensus organizations](/company/culture/all-remote/management/#separating-decision-gathering-from-decision-making). Hierarchical organizations have good speed but are bad at gathering data, leading to people saying yes but not doing it. Consensus organizations are good at gathering data but lack speed, leading to projects happening under the radar. We split decisions into two phases. The data gathering phase has the best of consensus organizations, where everyone can contribute. The decision phase has the best of a hierarchical organization, the person that does the work or their manager decides what to do.
1. If you apply consensus in both the data gathering phase and the decision phase you lose speed and you get decisions that try to stay under the radar so there are fewer people to convince.
1. If you apply hierarchy in both the data gathering phase and the decision phase you lose valuable input.
1. Providing input but then not being part of the decision making phase is counterintuitive, you feel ignored. We'll have to accept that people listened to us but don't owe us an explanation to have fast decisions based on everyone's input.
1. At GitLab, decision making is based on an informed and knowledgeable hierarchy, not on consensus or democracy. Voting on material decisions shows a lack of informed leadership.
1. Make data driven decisions but consider environments that do not allow reliable data collection. According to [research by the Harvard Business Review](https://hbr.org/2016/02/the-rise-of-data-driven-decision-making-is-real-but-uneven), "experience and knowledge of leaders in the subject matter still outperforms purely data-driven approaches."
1. When analyzing trends, never show cumulative graphs because they always look up and to the right even if business is bad.
1. Be aware of your unconscious biases and emotional triggers.
1. We don't have project managers. Individual contributors need to manage themselves. Not everyone will be able to do this effectively and fit our organization. Making someone responsible for managing others will cause negative effects to the results of the people that can manage themselves. If you manage yourself you have a much greater freedom to make decisions, and those decisions are based on deep knowledge of the situation. We want to retain the people that can handle that responsibility and therefore we can't retain the ones that struggle. Assigning a project manager/coordinator/case manager/etc. to something is an indicator that something is wrong and we are picking the wrong solution. The notable exception to this is in the [Professional Services](/services/) organization. While most functions at GitLab are serving the product or the company, ProServe is a services company which collaborates closely with customers and is sometimes contractually obligated to have project managers.
1. The person that does the work makes the decisions, they are the [Directly Responsible Individual (DRI)](/handbook/people-group/directly-responsible-individuals/). They should listen to the data and informed opinions of others to arrive at their decision. Part of making good decisions is knowing who has good information and/or experience that informs that decision. Once a DRI has made a decision, [Disagree, commit, and disagree](/handbook/values/#disagree-commit-and-disagree)
1. A DRI may make a decision that results in (and is hence the cause of) negative feelings, but it is important to remember [Collaboration is not consensus](/handbook/values/#collaboration-is-not-consensus) and [People are not their work](/handbook/values/#people-are-not-their-work). While others are invited to contribute data and informed opinions during the decision making process, the DRI is not responsible for how they feel. When contributing supporting information to making a decision, it is the responsibility of the contributors to do so with data, use cases, historic examples, etc and not personal opinions or attacks.
1. Short way to phrase this: We can allow others into our kitchen because we can always send them out (inviting people to give input is much easier if you retain the ability to make a decision by yourself).

> If good decision-making appears complicated, that’s because it is and has been for a long time. Let me quote from Alfred Sloan, who spent a lifetime preoccupied with decision-making: “Group decisions do not always come easily. There is a strong temptation for the leading officers to make decisions themselves without the sometimes onerous process of discussion.”
> 
> 
> - _Chapter 5: Decisions, Decisions of High Output Management by Andy Grove_


