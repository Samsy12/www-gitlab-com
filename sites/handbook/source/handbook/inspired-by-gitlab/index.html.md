---
layout: handbook-page-toc
title: "Inspired by GitLab"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Which companies can be listed here?

Any company that has been inspired by GitLab's culture and/or by any part of our handbook can be listed here.

[Transparency](/handbook/values/#transparency) is one of [GitLab's values](/handbook/values/), and so we encourage you and your company to read our handbook and use the parts that make sense to your business, or even adapt it to your context.

With that, we want to know what on GitLab's culture or on GitLab's handbook inspired you, and how are you and your company using it to the benefit your business?

To be listed in this page, [open an issue](https://gitlab.com/gitlab-com/www-gitlab-com/issues) with the following information:

**Title:** Add company XYZ on Inspired by GitLab handbook page / Update company XYX on Inspired by GitLab handbook page

**Description:** Tells us what exactly was the inspiration, with a link to the handbook page so that others can have a look, and maybe get inspired as well. Optionally, send us your company's website URL so that we can link to it.

**Label:** handbook

Or, if you feel comfortable with making the change yourself, submit a [merge request](https://gitlab.com/gitlab-com/www-gitlab-com/merge_requests), and we will be happy to review and have it merged as soon as possible. Don't forget to add the label 'handbook'.

### Companies that have been inspired by GitLab

In this section we list the companies that have been inspired by GitLab with the links to the handbook pages that inspired them.

1. [Doist](https://doist.com/)
	 - This all-remote company launched [Twist](https://doist.com/blog/twist-mindful-team-communication/), a team communications app. In a series of [Remote Work Guides](https://twist.com/remote-work-guides), Doist cites GitLab in sharing practices around all-remote [hiring](https://twist.com/remote-work-guides/remote-company-setup) and [management](https://twist.com/remote-work-guides/remote-management), as well as mastering [asynchronous communication](https://twist.com/remote-work-guides/remote-team-communication). 
1. [Joy Labs](https://joylabs.com/)
	-  This all-remote company is developing communication software, including [Memo](https://memo.com/). GitLab's intentional approach to [informal communication](/company/culture/all-remote/informal-communication/) was used in [building Joy Labs' company culture](https://www.linkedin.com/posts/joylabs_informal-communication-activity-6599648936762445824-Jd-6/).
1. [Veamly](https://veamly.com/)
	- Veamly is a remote-first software firm. CEO Emna G. was inspired GitLab's documented approach to [building company culture](/company/culture/all-remote/building-culture/), and shared elements that she is implementing in a [GitLab Unfiltered](https://www.youtube.com/channel/UCMtZ0sc1HHNtGGWZFDRTh5A) [Pick Your Brain](/company/culture/all-remote/pick-your-brain/) video. 
1. [ProxyCrawl](https://proxycrawl.com/)
	- At ProxyCrawl we are a completely remote company with employees all around the world working from home, co-working spaces, cofee shops, parks, etc. All our products have been created by our remote team. We have faced many of the issues of a worldwide remote team and GitLab handbook has helped us and inspired us to be a greater team. Special mentions to [communication](/handbook/communication/) and [all-remote](https://about.gitlab.com/company/culture/all-remote/guide/) pages. 

## Do you know of other companies that were inspired by GitLab but are not listed here?

Let us know by [opening an issue](https://gitlab.com/gitlab-com/www-gitlab-com/issues) to include them in this page, with the company's name and their website so that we can contact them and list their experience here as well, to inspire others. Don't forget to add the label 'handbook'.
