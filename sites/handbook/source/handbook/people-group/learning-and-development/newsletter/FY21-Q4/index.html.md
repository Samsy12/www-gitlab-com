---
layout: handbook-page-toc
title: FY21-Q4 Learning & Development Newsletter
---

Welcome to the first GitLab Learning & Development (L&D) newsletter! The purpose of the L&D newsletter is to enable a culture of curiosity and continuous learning that *prioritizes learning* with a [growth mindset](/handbook/values/#growth-mindset) for team members. The quarterly newsletter will raise awareness of what learning initiatives took place in the past quarter, insight into what's coming next, learning tips, and encourage participation. We will also feature leadership and learner profiles that highlight what our community has done to learn new skills. Consider this a forum to hear from others across GitLab on what learning has done for them.  

For more information on the structure and process for the L&D newsletter, see [this handbook page](/handbook/people-group/learning-and-development/newsletter/).

## Learn from Leadership 

This quarter we are learning from [Sid Sijbrandij, CEO](/company/team/#sytses). 

<figure class="video_container">
  <iframe width="560" height="315" src="https://www.youtube.com/embed/vIyz8RMc1Ts" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

### Five Fast Facts from our Interview with Sid

1. Favorite Book: [High Output Management](https://www.amazon.com/High-Output-Management-Andrew-Grove/dp/0679762884/ref=sr_1_3?dchild=1&hvadid=78065379167018&hvbmt=be&hvdev=c&hvqmt=e&keywords=high+output+management+book&qid=1603985339&sr=8-3&tag=mh0b-20) 
1. Preferred Learning Style: Read/Write
1. Role Model(s): Amazon (AWS), SpaceX 
1. What are you doing to grow?: has coaches, utilizes his board performance review, reads books, asks a lot of questions 
1. Handbook page to check out: The [Leadership Page](/handbook/leadership/) 

Is there a leader at GitLab that you want to learn more about? To nominate someone for our Learn from Leadership section in our next newsletter, use [this nomination form](https://docs.google.com/forms/d/e/1FAIpQLSeuOIH2r_gaQlv6woW96_8BfjBUbzWxLuxoZA7TW-MXz7cT0g/viewform). 

## Learner Spotlight   

Our Learner Spotlight for this quarter is [Kris Reynolds](/company/team/#kreykrey). Kris is a Manager of Field Enablement Programs. She also recently utilized [GitLab's Tuition Reimbursement program](/handbook/total-rewards/benefits/general-and-entity-benefits/#tuition-reimbursement). 

<figure class="video_container">
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vTHzCdtdjH5W7AG2pa3yilT0spTYG6UDwelBCvgvLifFZGvQTGInWVBq17X78Cj4X1Hjy5ChmlhK9TQ/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
</figure>

* **Which university/program were you a student in?**
   * _Kris_: I completed the [Performance Leadership Certificate from Cornell University](https://www.ecornell.com/certificates/leadership-and-strategic-management/performance-leadership/).  
* **What made you decide to do that program?**
   * _Kris_: I chose this program because it aligned with skills I listed in my Individual Growth Plan. I really liked this program because it was flexible and you could choose different courses from the catalog to create a certificate that made sense for you.  You can check out the [courses I chose](https://docs.google.com/document/d/1h_nSe9FV7wTPtJMOXy_qPvLpITrf10P0LH1PQxnEQco/edit?usp=sharing).
* **How will completing this program help you in your role at GitLab?**
   * _Kris_: We don’t have enough time to talk about all of the benefits! At a high level, this program provided me a framework to help me organize my thoughts and to lead learning initiatives and a high-performing team to tackle the enablement challenges that come our way! 
* **What is one thing you have learned so far in your program that has been beneficial?**
   * _Kris_: The Change Management course was something that I was able to lean into right away. So many of our learning programs require some behavioral change and thinking through a change management plan from the beginning helps the field understand why we are doing this and why we are doing this right now! 
* **When will your program be completed?**
   * _Kris_: I completed the program in July of this year, and it took about 12 weeks.
* **What advice would you give someone who is thinking about participating in GitLab's tuition reimbursement program?**
   * _Kris_: GitLab offers [$20K for your education](/handbook/total-rewards/benefits/general-and-entity-benefits/#tuition-reimbursement), and it’s like free money! More people should take advantage of the program. It worked with my manager and the process was pretty easy. 
* **Anything else you would like to share on the tuition reimbursement program?**
   * _Kris_: There are so many amazing programs out there! Take a look and talk to your manager today, why wait? Every day is a great day for learning. I understand everyone is busy, but it really is worth the time investment in yourself.  

Do you know a team member that places an emphasis on learning? To nominate someone for our Learner Spotlight for our next newsletter, use [this nomination form](https://docs.google.com/forms/d/e/1FAIpQLSfi72ONbp8UcUXDCL__TPAoCEEGH4K_9i1-ZQN7yh_YzlVx0w/viewform). 

## Department Spotlight 

The L&D Team has three team members that focus on enabling a culture of learning by making it a priority for team member growth and development. 

We would like to introduce you to our L&D Team! 

<figure class="video_container">
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vT7oZf3KL_AAAblfZEs1WkivFVupxF5HtkUDgANQbsi4OzkHyM1cbaVgEGQ6WugvhloX9JwfS0slxYr/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
</figure>

If you would like to learn more about L&D at GitLab, visit our [handbook page](/handbook/people-group/learning-and-development/). 

Each quarter we will feature a different team and what they do here at GitLab. 

## Recap of FY21-Q3

* 26 managers participated in our [Manager Challenge Pilot](/handbook/people-group/learning-and-development/manager-challenge/#pilot-program). This was the first program of its kind at GitLab and it was developed in an effort to reinforce our values and to enable our people leader to manage remotely. We are currently making iterations and plan to roll out in early 2021! 
   * If you would like to learn about what the participants thought of the program, check out [the retrospectives](/handbook/people-group/learning-and-development/manager-challenge/#retrospective-sessions). 
* The [How to Manage a Remote Team](https://www.coursera.org/learn/remote-team-management) Course on Coursera Launched in September! To date, 157 people have completed the course and 8,814 have enrolled! 
* The first Monthly Continuous Learning Call took place on 2020-10-21. If you missed it or wanted to watch again, you can do that [here](/handbook/people-group/learning-and-development/learning-initiatives/#past-monthly-continuous-learning-call). 

## Upcoming in FY21-Q4 

Our L&D team will have a variety of learning initiatives throughout the quarter to help reinforce our culture of learning. 

**Live Learning Sessions**

[Live learning](/handbook/people-group/learning-and-development/#live-learning) sessions are one of the ways we provide learning opportunities to team members. GitLab team members [can sign up here](https://www.signupgenius.com/go/10c0d4faeae2ca7f4c70-fy21q4) for any of our Q4 live learning sessions. If we add more courses throughout the quarter, they will be added there as well. 

* 2020-11-10: Live Learning - [Belonging](/company/culture/inclusion/#gitlabs-definition-of-diversity-inclusion--belonging) 
* 2020-12-03: Live Learning - [Introduction to Coaching](/handbook/leadership/coaching/) 

**Challenges**

[Challenges](/handbook/people-group/learning-and-development/#gitlab-mini-and-extended-challenges) can cover a range of topics and they are intended to introduce, reinforce, and build new skills and habits through behavior change. In Q4, L&D is launching a one-week challenge that all team members can attend and a comprehensive manager challenge focused on people leaders. 

* 2020-11-16 - 2020-11-20: One-Week Challenge - [Psychological Safety](/handbook/leadership/emotional-intelligence/psychological-safety/#one-week-challenge) 
   * [Sign Up Here](https://www.signupgenius.com/go/10c0d4faeae2ca7f4c70-oneweek) 
* 2021-01 - [Manager Challenge](/handbook/people-group/learning-and-development/manager-challenge/) 
   * If you are interested in joining the January launch, [sign up here](https://gitlab.com/gitlab-com/people-group/learning-development/manager-challenge/-/issues/21)! 

**Monthly Continuous Learning Call**

Every month, the L&D team hosts a [Monthly Continuous Learning Call](handbook/people-group/learning-and-development/#monthly-continuous-learning-call). During these calls, we highlight all of the initatives taking place within the learning space at GitLab. Our upcoming calls for Q4 are outlined below: 

* 2020-11-18 @ 1:00 PM PST 
* 2020-12-16 @ 7:00 AM PST
* 2020-01-20 @ 1:00 PM PST 

**Other Events**

* 2020-11-12: [Learning Speaker Series](/handbook/people-group/learning-and-development/learning-initiatives/#learning-speaker-series-overview) - Building Trust
   * Learn more about the speaker: [Dr. Jeb Hurley](https://docs.google.com/presentation/d/1zyRgYcq3s1esbZ4gTNnqvoEjjFdjOp1EI8-YRBlC2_0/edit#slide=id.ga637fcc0e9_0_145) 
   * An invite was sent to all GitLab Team Members from the GitLab Team Meetings calendar. 
* 2020-12-01: DIB Certification
* 2020-12: Learning Speaker Series - Managing Mental Health & Burnout
   * An invite will be sent from the GitLab Team Meetings calendar when the date is finalized. 

**Note:** More learning activities may be added throughout the quarter. To stay up to date, please join our [#learninganddevelopment](https://app.slack.com/client/T02592416/CMRAWQ97W) Slack Channel. 

## Learning Spotlight 

More ways you can continue to learn throughout the quarter: 

* LinkedIn Learning [Diversity, Inclusion, & Belonging for All](https://www.linkedin.com/learning/paths/diversity-inclusion-and-belonging-for-all) learning path is free for anyone to take through the end of the year!  

## Learning Tips 

We want to remind you to [take time out to learn](/handbook/people-group/learning-and-development/#take-time-out-to-learn-campaign)! Focus Fridays are a great time to focus on learning. Consider blocking off a few hours each week to learn new skills for your role at GitLab

Are you having a hard time finding or making time for learning? The Potentiality article outlines [seven ways to make time for learning](https://thepotentiality.com/seven-ways-to-make-time-for-learning/). 

### Seven Ways to Make Time for Learning

- **Find motivation** - Start making time for learning by beginning with a topic – any topic – that “lights you up” and ignites your purpose.
- **Start small** - Maybe you can’t imagine fitting a Master's program into your life right now, but you can imagine reading a book or some articles about co-operatives and exploring a few relevant massive open online courses (MOOCS) about leading social impact.
- **Create buy-in** - HBR’s Rachael O’Meara recommends that you create a vision statement that can answer this question: “who will I become as a result of this investment of my time and resources?” 
- **Make time** - Time blocks, argues Fast Company’s Gwen Moran, are more effective than to-do lists because they force you to put what needs to be done in the proper place for the right amount of time.
- **Find space** - Changing your environment can shift your perspective and open your mind to formal and informal learning. 
- **Be disciplined** - Most employees don’t follow through with learning experiences because they aren’t fun, aren’t relevant and no one is holding them accountable to finish. You will require unbreakable self-discipline (or something approaching it) to follow-through with your commitment to learning
- **Share the value** - An easy way to do this is through journaling about what you learned from the experience.

## Other Enablement Initiatives

* Check out the monthly [Field Flash Newsletter](/handbook/sales/field-communications/field-flash-newsletter/#past-newsletters)
* The Diversity, Inclusion, & Belonging Team launched a newsletter last week. Search your inbox for the subject line `DIB - Diversity, Inclusion and Belonging Quarterly Newsletter`. 
* The [#mental_health_aware](https://app.slack.com/client/T02592416/C834CM4HW/thread/C834CM4HW-1604325012.091700) Slack channel is organizing a bi-monthly call to connect about mental health awareness. Check out their Slack channel for announcements about the next call.
* [Walk and Talk Calls](/handbook/communication/#walk-and-talk-calls) are a great way for teams to stay active during the day and build trust during calls. Consider applying this call format with your team!

## Discussion 

If you would like to discuss items related to this newsletter, please see the related [issue](https://gitlab.com/gitlab-com/people-group/learning-development/newsletter/-/issues/6).
