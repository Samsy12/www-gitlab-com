---
layout: security
title: Security - Frequently Asked Questions
description: "We designed this Frequently Asked Questions page to serve as a starting point for those interested in GitLab's Security."
canonical_path: "/security/faq/"
---

{::options parse_block_html="true" /}

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# GitLab Security Frequently Asked Questions

At GitLab, security is a priority. But so is transparency. And that's why it is so important that our Customers and Prospects have the most accurate information regarding our security. We designed this Frequently Asked Questions page to serve as a starting point for those interested in GitLab's Security. For more in depth resources, please visit our [Customer Assurance Package](https://about.gitlab.com/handbook/engineering/security/security-assurance/field-security/customer-assurance-package.html). 

<details>
  <summary markdown="span">
  Cloud Questions
  </summary>
  
<details>
  <summary markdown="span">
  **Q - What is the difference between SaaS and self-hosted?**
 </summary>

 A - GitLab has two deployment models. The first is a multi-tenant public cloud Software as a Service (SaaS). For customers who utilize the SaaS model, GitLab.com, GitLab deploys, operates, monitors, and maintains the instance continuously. The second is Self-Hosted. With this method, the customer is responsible for deploying, operating, monitoring and maintaining GitLab software on their own infrastructure. 
	    
</details>
 <details>
   <summary markdown="span">
 **Q - What does multi-tenant, public cloud, and Software as a Service mean?**
   </summary>
	            
 A - These are cloud computing terms defined in the [National Institute of Standards and Technology Special Publication 800-145: The NIST Definition of Cloud Computing](https://csrc.nist.gov/publications/detail/sp/800-145/final). To summarize, **Multi-Tenant** refers to the fact that multiple entities are deployed on the same underlying Infrastructure. **Public Cloud** refers to the fact that GitLab.com is open to most users with an internet connection with the few exceptions noted in our [Terms of Service](https://about.gitlab.com/terms/#subscription)
	    
</details>
 <details>
   <summary markdown="span">
**Q - Does GitLab.com depend on any other cloud providers?**
 </summary>

A - Yes, GitLab.com is deployed on [Google Cloud Platform (GCP)](https://cloud.google.com/security) Infrastructure as a Service (IaaS). For detailed architecture please see our [Production Architecture](https://about.gitlab.com/handbook/engineering/infrastructure/production/architecture/)
	    
</details>
<details>
  <summary markdown="span">
 **Q - What deployment model does GitLab recommend?**
 </summary>

 A - There is no one-size fits all solution, however for most entities GitLab.com is the most efficient and secure way to make use of all the great features GitLab offers. GitLab.com comes preconfigured and running and it takes minimal internal resources to support. Some entities with highly specific needs may choose to deploy GitLab as a self-hosted software to tailor GitLab to their needs. If you need help determining which is right for your organization [our Sales team would be happy to help](https://about.gitlab.com/sales/) evaluate your needs.
	  
</details>
 <details>
<summary markdown="span">
 **Q - What is Shared Responsibility?**
</summary>

A - In a cloud computing environment it is important to understand the roles and responsibilities all parties have in keeping data secure. To learn more about [Shared Responsibilities at GitLab](#)<!--todo: link whitepaper on shared responsibility--> see our whitepaper.
	    
</details>

</details>

<details>
  <summary markdown="span">
	    Security Questions
	    </summary>

<details>
  <summary markdown="span">
 **Q - Does GitLab have a Security Team?**
   </summary>

A - Yes; GitLab has a dedicated [Security Department](https://about.gitlab.com/handbook/engineering/security/). The current individual makeup of our security team can be seen on our [About the Team Page](https://about.gitlab.com/company/team/?department=security-department)
	 
 </details>
 <details>
  <summary markdown="span">
 **Q - Does GitLab have an Information Security Program?**
  </summary>

A - Yes; GitLab has a robust information security program and [many of our policies and procedures](https://about.gitlab.com/handbook/engineering/security/#resources) are publicly accessible.
	   
</details>
<details>
  <summary markdown="span">
 **Q - Is GitLab's Security Program aligned with industry standards?**
 </summary>

 A - Yes. GitLab's [Security Compliance Controls](https://about.gitlab.com/handbook/engineering/security/security-assurance/security-compliance/sec-controls.html) are publicly viewable. The GitLab Control Framework forms the building blocks of our Security Compliance Program.
	   
</details>
<details>
  <summary markdown="span">
**Q - Does GitLab hold any 3rd Party Compliance Attestations?**
  </summary>

A - Yes; GitLab currently has a SOC2 Type 1 Report that [can be provided under NDA](https://about.gitlab.com/handbook/engineering/security/security-assurance/field-security/procedures/SOC2.html). GitLab is actively engaged in attainment of a [SOC2 Type 2](https://about.gitlab.com/handbook/engineering/security/security-assurance/security-compliance/soc2.html).
	    
</details>
<details>
  <summary markdown="span">
 **Q - Does GitLab encrypt my data? At Rest? In Transit?**
  </summary>

A - Yes, Yes, and Yes. GitLab takes the security of data our customers entrust us with very seriously. Data is Encrypted at Rest using Google Cloud Platform's Encryption at Rest with the AES-256 Algorithm. All data in-transit is encrypted using TLS 1.2.
	   
</details>
<details>
  <summary markdown="span">
 **Q - Does GitLab have an incident response plan?**
 </summary>

A - Yes; You can view our [Incident Response Guide](https://about.gitlab.com/handbook/engineering/security/sec-incident-response.html) in the Handbook.
	    
 </details>
 <details>
   <summary markdown="span">
**Q - Does GitLab regularly undergo penetration testing by a 3rd party firm?**
  </summary>

A - Yes. Our 3rd Party Penetration Test Summary can be provided under NDA.
	   
</details>

<details>
  <summary markdown="span">
**Q - Does Gitlab have a business continuity plan/disaster recovery plan?** 
  </summary>

A - Yes. GitLab, Inc's [Business Continuity and Redundancy plans](https://about.gitlab.com/handbook/business-ops/gitlab-business-continuity-plan.html) is available in the Handbook for our SaaS customers. 

</details>

<details>
  <summary markdown="span">
 **Q - What data does GitLab have access to (personal and business)?**
  </summary>

A - GitLab is the Data Processor and our Customers are Data Controllers. GitLab collects information required to set up your GitLab.com account. Please see our DPA on the Privacy Compliance page. 
<br>
Personal Details (including but not limited to):
<br>
- First Name
- Last Name
- Email
- City
- Postal Code
- Country
- State or Territory
- Company/Group Details (Including but not limited to)
- Company Name
- Total # of employees

</details>

</details>

## Getting More information
- The [Customer Assurance Package](https://about.gitlab.com/handbook/engineering/security/security-assurance/field-security/customer-assurance-package.html) was designed to allow the anyone to review and evaluate GitLab's security posture. 

This FAQ applies solely to our Software as a Service (SaaS); GitLab.com.

