---
layout: job_family_page
title: "UX Researcher"
---

At GitLab, UX Researchers collaborate with our Product Designers, Product Managers and the rest of the community to assist in determining what features should be built, how they should behave, and what the priorities should be. UX Researchers report to the UX Research Manager.

## Responsibilities

Unless otherwise specified, all UX Research roles at GitLab share the following requirements and responsibilities:

#### Requirements

* [Self-motivated and self-managing](https://about.gitlab.com/handbook/values/#efficiency), with strong organizational skills.

* Share our [values](https://about.gitlab.com/handbook/values/), and work in accordance with those values.

* Passionate about mentoring others.

* Evangelize research. Share user insights within the broader organization and externally in creative ways to increase empathy.

* Empathetic, curious and open-minded.

* Able to thrive in a fully remote organization.

* Able to use GitLab.

#### Nice to haves

* Enterprise software company experience.

* Developer platform/tool industry experience.

* A degree in psychology, human factors, human-computer interaction, or a related field.

### UX Researcher

#### Job Grade

The UX Researcher is a [grade 6](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Responsibilities
* Deeply understand the technology and features of the stage groups to which you are assigned.

* Maintain a thorough knowledge of the direction and vision for your assigned stage groups. Consistently consider how your research can support the goals of your assigned stage groups, the broader product vision, and company objectives.

* Conduct problem validation user research projects within your stage groups, and engage Product Managers for collaboration.

* Respond to solution validation user research inquiries that are escalated by Product Design Managers.

* Proactively identify strategic user research initiatives that span multiple stage groups, and engage Senior UX Researchers/Staff UX Researchers for guidance and/or collaboration.

* Actively contribute to UX Research processes and documentation.

### Senior UX Researcher

#### Job Grade

The Senior UX Researcher is a [grade 7](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Responsibilities
* Demonstrate and articulate understanding of the technology and features of immediate and adjacent stage groups.

* Maintain a thorough knowledge of the direction and vision for immediate and adjacent stage groups. Consistently consider how your research can support the goals of immediate and adjacent stage groups, the broader product vision, and company objectives.

* Conduct problem validation user research projects within your stage groups, and engage Product Managers for collaboration.

* Respond to solution validation user research inquiries are escalated by Product Design Managers.

* Lead strategic user research initiatives that span multiple stage groups.

* Demonstrate, explain, and teach others how to apply a range of research methods, understanding the balance and trade-offs between smart, scrappy research and rigor.

* Actively contribute to UX Research processes and documentation.

* Work in collaboration with Staff UX Researchers to design and deliver training content and workshops.

* Interview potential UX candidates.

### Staff UX Researcher

#### Job Grade

The Staff UX Researcher is a [grade 8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Responsibilities
* Demonstrate and articulate understanding of the technology and features of immediate and adjacent stage groups, and have a working knowledge of the end-to-end GitLab product.

* Maintain a thorough knowledge of the direction and vision for immediate and adjacent stage groups. Consistently consider the holistic user experience and how your research can support the goals of immediate and adjacent stage groups, the broader product vision, and company objectives.

* Conduct problem validation user research projects within your stage groups, and engage Product Managers for collaboration.

* Respond to solution validation user research inquiries that are escalated by Product Design Managers.

* Lead strategic user research initiatives that span multiple stage groups (and possibly the entire product).

* Demonstrate, explain, and teach others how to apply a range of research methods, understanding the balance and trade-offs between smart, scrappy research and rigor.

* Actively seek out difficult impediments to our efficiency as a team (process, tooling, etc), and propose and implement solutions that will enable the entire team to work more efficiently.

* Exert significant influence on the overall objectives and long-range goals of the UX Research team.

* Identify research training needs for the UX and Product Department. Work with Senior UX Researchers to design and deliver training content and workshops.

* Interview potential UX candidates.

## Career Ladder

For more details on the engineering career ladders, please review the [engineering career development](/handbook/engineering/career-development/#roles) handbook page.

## Performance indicators

* [Perception of System Usability](/handbook/engineering/ux/performance-indicators/#perception-of-system-usability)

* [Proactive vs Reactive UX Work](/handbook/engineering/ux/performance-indicators/#ratio-of-proactive-vs-reactive-ux-work)

## Relevant links

- [UX Research Handbook](/handbook/engineering/ux/ux-research/)

- [UX Department Handbook](/handbook/engineering/ux/)

- [Product Handbook](/handbook/product)

## Hiring Process

Candidates for this position can expect the hiring process to follow the order below. Please keep in mind that candidates can be declined from the position at any stage of the process. To learn more about someone who may be conducting the interview, find their job title on our [team page](/company/team).

* Selected candidates will be invited to schedule a 30 minute [screening call](https://about.gitlab.com/handbook/hiring/interviewing/#screening-call) with one of our Global Recruiters. In this call we will discuss your experience, understand what you are looking for in a UX Research role, discuss your compensation expectations, reasons why you want to join GitLab and answer any questions you have.

* Next, if you successfully pass the screening call, we'll invite you to record a case study walkthrough video. The recording is expected to be **20- minutes in duration** and returned **within ten days** of the screening call. In addition to the details below, [this discussion on our GitLab Unfiltered YouTube playlist](https://youtu.be/VU2BKjGoer4) provides an overview of what we're looking for.  

### Why we ask for a pre-recorded presentation

1. It helps establish a shared understanding of your work between you and our interviewers ahead of the first interview. 
1. It allows us to ask more informed questions about your experience and work during the interview process. 
1. Generally, we [avoid using meetings as a means to present](https://about.gitlab.com/handbook/communication/#common-meeting-problems). Instead, we record presentations, upload to our Unfiltered Youtube channel, and use a meeting as a Q&A. This interview exercise provides you with an opportunity to experience this style of work.  

### Presentation Topics

1. An introduction; who you are, where you're based, you're background, and why you're a UX Researcher.
1. An overview of one *or* two research studies. We want to understand; the size, discipline make-up, and structure of the team you were a part of; the goals of the project; how you approached research;  what you personally did on the project, how you synthesized research data to inform product and design decisions; the final output of your research; the challenges you faced throughout the project; and the meaningful business impact that the research resulted in. 

### What we're looking for 

1. A recording that is 20 mintues in duration and returned to us within ten days of your screening call being completed. 
1. Ideally, one study you present should be relevant to the work you'd expect do at GitLab. This could be because the study involved research into the [persona(s)](/handbook/marketing/strategic-marketing/roles-personas/#user-personas) you'd study at GitLab, the study related to the DevOps or Developer tooling industry, the study relates to the [Stage](/handbook/product/categories/#devops-stages) you're interviewing for, is a study that went across multiple methods, or was a study that would be considered complex in nature. 
1. You share the recording using either a [private Youtube link](https://support.google.com/youtube/answer/157177?co=GENIE.Platform%3DDesktop&hl=en), a [Google Drive link](https://support.google.com/drive/answer/2494822?co=GENIE.Platform%3DDesktop&hl=en), or a [Loom link](https://www.loom.com/). 
1. Your presentation to address each of the topics listed above.  

## Some useful resources

1. Unsure on where to get started? Check out our [handbook page on recording a presentation in Zoom](https://about.gitlab.com/handbook/tools-and-tips/zoom/#how-to-share-a-presentation-in-zoom). A free Zoom account is all you'd need to complete this presentation. 
1. Interested in the type of work our UX team does? Check out our [UX Showcases on GitLab Unfiltered](https://www.youtube.com/playlist?list=PL05JrBw4t0Kq89nFXtkVviaIfYQPptwJz), our [UX Research handbook](https://about.gitlab.com/handbook/engineering/ux/ux-research/), and our [UX Department's handbook](https://about.gitlab.com/handbook/engineering/ux/).
1. Other questions? Would like the Recruiter to review your presentation before the review from the Hiring Manager? Don't hesitate to get in touch with the Recruiter who conducted your screening call. 

* We will schedule the next stages if the recording indicates your past experiences will lead to your success in a role at GitLab.

* The first interview is 45 minutes with a UX Researcher. Overall, this interview aims to understand how you choose your research methods, how you present results, how you work with a wider team of counterparts, and how you determine whether your research resulted in a meaningful business impact. This interview will also look to understand the softer skills you have as a researcher and how you apply those in the real world. 

* Next, candidates will be invited to a 1-hour interview with the Director of UX Research. 

* Candidates will then be invited to a third 50-minute interview with our VP of UX. In this final stage interview, we''ll focus on the end result of a research study you've outlined in your case study presentation. We want to understand the question(s) you were addressing (and how you determined these questions), your methodology, and how you shared that information with the wider team.

* The final interview is 50 minutes with a Product Manager. The scheduling of this interview is contingent on positive feedback in the previous two interviews. This interview be an opportunity for you to understand the stage you will a part of in more detail, hear a different perspective on the stable counterpart relationship, and ask any final questions you have. 

* Successful candidates will subsequently be made an offer via Zoom.

Additional details about our process can be found on our [hiring page](/handbook/hiring).

[groups]: /company/team/structure/#groups
